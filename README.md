# Minds Shared Infrastructure

## Local Setup

First, install the following dependencies:

- [`terraform`](https://www.terraform.io)
- [`tfenv`](https://github.com/tfutils/tfenv) (Optional)
- [`terragrunt`](https://terragrunt.gruntwork.io)
- [`terraform-docs`](https://github.com/terraform-docs/terraform-docs)
- [`pre-commit`](https://pre-commit.com)

If you're using Homebrew, you can install all of the above dependencies with:

```bash
brew install terraform terragrunt terraform-docs pre-commit
```

After installing the dependencies, run `pre-commit install` at the root of this repo. If you miss this, your commits may fail the CI.

Before commiting, you can test manually against your staged changes:

```bash
git add .
pre-commit run
```
