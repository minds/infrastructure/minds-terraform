resource "kubernetes_namespace" "elasticsearch" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "elasticsearch"
    }

    labels = {
      name = "elasticsearch"
    }

    name = "elasticsearch"
  }
}

locals {
  es_values = {
    elasticsearch = {
      master = {
        nodeSelector = { stateful = "true" }
        tolerations = [
          {
            key      = "stateful"
            operator = "Equal"
            value    = "true"
            effect   = "NoSchedule"
          }
        ]
      }
      data = {
        nodeSelector = { stateful = "true" }
        tolerations = [
          {
            key      = "stateful"
            operator = "Equal"
            value    = "true"
            effect   = "NoSchedule"
          }
        ]
      }
      client = {
        readinessProbe = {
          tcpSocket           = { port = "transport" }
          initialDelaySeconds = 60
          periodSeconds       = 10
        }
      }
    }
  }
}

module "eks-elasticsearch" {
  source = "../../modules/elasticsearch"

  providers = {
    helm = helm.eks
  }

  eks_cluster_name     = var.eks_cluster_name
  kubernetes_namespace = kubernetes_namespace.elasticsearch.metadata[0].name

  security_nodes_dn = "CN=transport.${var.certs_common_name}"
  security_admin_dn = "CN=admin.${var.certs_common_name}"

  data_replicas   = var.data_replicas
  master_replicas = var.master_replicas
  client_replicas = var.client_replicas

  data_jvm_memory_gb   = var.data_jvm_memory_gb
  master_jvm_memory_gb = var.master_jvm_memory_gb

  reindex_host = var.reindex_host

  values = [yamlencode(local.es_values)]
}

##
# Secrets and Certificates
# !! We have a separate helm chart, es-certs, in this directory that makes use of
# cert-manager. This is ONLY because terraform kubernetes does not yet support
# CRD's !!
#
# We use our ClusterIssuer provided by the Vault terraform environment
# Ensure that any domains are provided in the 'eks-cluster' pki role
#


resource "helm_release" "es_certs" {
  provider = helm.eks

  name      = "es-certs"
  namespace = kubernetes_namespace.elasticsearch.metadata[0].name

  repository = "./"
  chart      = "es-certs"

  set {
    name  = "vault.issuer.name"
    value = "vault-certs-issuer"
  }

  set {
    name  = "vault.issuer.kind"
    value = "ClusterIssuer"
  }

  set {
    name  = "certificates.secretNamePrefix"
    value = "elasticsearch"
  }

  set {
    name  = "certificates.commonName"
    value = var.certs_common_name
  }

  set {
    name  = "certificates.commonName"
    value = var.certs_common_name
  }

}
