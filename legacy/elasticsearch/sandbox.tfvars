eks_cluster_name = "sandbox"

certs_common_name = "elasticsearch.infra.minds.io"

data_replicas   = 1
client_replicas = 1
master_replicas = 1

data_jvm_memory_gb   = 1
master_jvm_memory_gb = 1

reindex_host = "elasticsearch-master.default.svc.cluster.local:9200"