# Minds OpenSearch (ElasticSearch)

## Configuring certificates

This environment makes use of the `vault-certs-issuer` ClusterIssuer, using certs-manager. Ensure that `elasticsearch-opendistro-es-client-service.elasticsearch.svc.cluster.local` is provided in the *Allowed domains** list.

## Accessing elasticsearch locally

```
kubectl -n elasticsearch port-forward elasticsearch-opendistro-es-master-0 9200:9200
```

## Issues

### Security index did not automatically bootstrap

```
sh ./plugins/opendistro_security/tools/securityadmin.sh \
    -cd ./plugins/opendistro_security/securityconfig \
    -cacert ./config/admin-root-ca.pem \
    -cert ./config/admin-crt.pem \
    -key ./config/admin-key.pem \
    -icl
```