eks_cluster_name = "minds-eks-jkqjL8xS"

certs_common_name = "elasticsearch.infra.minds.com"

data_replicas   = 3
client_replicas = 6
master_replicas = 2

data_jvm_memory_gb   = 20
master_jvm_memory_gb = 2

reindex_host = "10.0.5.20:9200"