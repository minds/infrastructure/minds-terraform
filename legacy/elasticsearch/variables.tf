##

variable "eks_cluster_name" {
  type        = string
  description = "The name of eks cluster to deploy to"
}

variable "eks_assume_role_name" {
  type    = string
  default = "minds_eks_k8s_devops"
}

variable "certs_common_name" {
  type        = string
  description = "The common name for the tls certificates"
}

variable "data_replicas" {
  type        = number
  description = "The number of data nodes to run"
}

variable "master_replicas" {
  type        = number
  description = "The number of master nodes to run"
}

variable "client_replicas" {
  type        = number
  description = "The number of client nodes to run"
}

variable "data_jvm_memory_gb" {
  type        = number
  description = "The amount of memory to allocate to elasticsearch. (note this pods need 2x this)"
}

variable "master_jvm_memory_gb" {
  type        = number
  description = "The amount of memory to allocate to elasticsearch. (note this pods need 2x this)"
}

variable "reindex_host" {
  type        = string
  default     = ""
  description = "The host to allow reindexing from (this adds to the whitelist). Include the port (ie. 9200)."
}