##

variable "eks_cluster_name" {
  type        = string
  description = "The name of eks cluster to deploy to"
}
