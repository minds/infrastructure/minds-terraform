##

variable "eks_cluster_name" {
  type        = string
  description = "The name of eks cluster to deploy to"
}


variable "vault_domain" {
  type        = string
  description = "The domain for the vault"
}

variable "eks_assume_role_name" {
  type    = string
  default = "minds_eks_k8s_devops"
}
