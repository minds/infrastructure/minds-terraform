resource "kubernetes_namespace" "vault" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "vault"
    }

    labels = {
      name = "vault"
    }

    name = "vault"
  }
}

locals {
  consul_values = {
    server = {
      nodeSelector = <<EOT
stateful: "true"
      EOT
      tolerations  = <<EOT
- key: "stateful"
  operator: "Equal"
  value: "true"
  effect: "NoSchedule"
      EOT
    }
  }
}

resource "helm_release" "consul" {
  provider  = helm.eks
  name      = "consul"
  namespace = kubernetes_namespace.vault.metadata[0].name

  repository = "https://helm.releases.hashicorp.com"
  chart      = "consul"
  version    = "0.31.1"

  set {
    name  = "global.image"
    value = "public.ecr.aws/hashicorp/consul:1.10.1"
  }
  set {
    name  = "global.foo"
    value = "bar"
  }
  set {
    name  = "server.replicas"
    value = 3
  }
  set {
    name  = "server.bootstrapExpect"
    value = 3
  }
  set {
    name  = "server.resources.requests.memory"
    value = "512Mi"
  }
  set {
    name  = "server.resources.limits.memory"
    value = "726Mi"
  }

  values = [yamlencode(local.consul_values)]
}

locals {
  vault_values = {
    server = {
      nodeSelector = { stateful = "true" }
      tolerations = [
        {
          key      = "stateful"
          operator = "Equal"
          value    = "true"
          effect   = "NoSchedule"
        }
      ]
    }
  }
}

resource "helm_release" "vault" {
  provider  = helm.eks
  name      = "vault"
  namespace = kubernetes_namespace.vault.metadata[0].name

  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  version    = "0.14.0"

  ##
  # Image override to avoid using dockerhub and hitting rate limits
  ##

  set {
    name  = "injector.image.repository"
    value = "public.ecr.aws/t7r8r4d6/hashicorp/vault-k8s"
  }

  set {
    name  = "injector.agentImage.repository"
    value = "public.ecr.aws/t7r8r4d6/hashicorp/vault"
  }

  set {
    name  = "server.image.repository"
    value = "public.ecr.aws/t7r8r4d6/hashicorp/vault"
  }

  #

  set {
    name  = "namespace"
    value = kubernetes_namespace.vault.metadata[0].name
  }

  set {
    name  = "server.ha.enabled"
    value = true
  }

  set {
    name  = "server.ha.config"
    value = <<EOT
ui = true

listener "tcp" {
  tls_disable = 1
  address = "[::]:8200"
  cluster_address = "[::]:8201"
}

seal "awskms" {
  region     = "us-east-1"
  kms_key_id = "${aws_kms_key.vault_auto_unseal.key_id}"
}

storage "consul" {
  path = "vault"
  # address = "HOST_IP:8500"
  address = "http://consul-consul-server.vault.svc:8500"
}
EOT
  }

  set {
    name  = "server.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.vault_aws_role.arn
  }

  set {
    name  = "server.ingress.enabled"
    value = false
  }

  set {
    name  = "server.ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "traefik"
  }

  set {
    name  = "server.ingress.hosts[0].host"
    value = var.vault_domain
  }

  set {
    name  = "csi.enabled"
    value = "true"
  }

  values = [yamlencode(local.vault_values)]

}

##
# Secrets and Certificates
# !! We have a separate helm chart, es-certs, in this directory that makes use of
# cert-manager. This is ONLY because terraform kubernetes does not yet support
# CRD's !!
#

data "kubernetes_namespace" "cert_manager" {
  provider = kubernetes.eks
  metadata {
    name = "cert-manager"
  }
}

resource "kubernetes_service_account" "vault_certs_issuer" {
  provider = kubernetes.eks
  metadata {
    name      = "vault-certs-issuer"
    namespace = data.kubernetes_namespace.cert_manager.metadata[0].name
  }
}

resource "helm_release" "vault_cluster_certs" {
  provider = helm.eks

  name      = "vault-cluster-certs"
  namespace = data.kubernetes_namespace.cert_manager.metadata[0].name

  repository = "./"
  chart      = "vault-certs"

  set {
    name  = "vault.server"
    value = "http://vault.vault:8200" # Note: namespace is vault
  }

  set {
    name  = "vault.path"
    value = "pki/sign/eks-cluster"
  }

  set {
    name  = "vault.issuerName"
    value = "vault-certs-issuer"
  }

  set {
    name  = "vault.role"
    value = "eks-cluster"
  }

  set {
    name  = "serviceAccount.secretRef"
    value = kubernetes_service_account.vault_certs_issuer.default_secret_name
  }

}
