resource "kubernetes_cluster_role_binding" "tokenreview_binding" {
  provider = kubernetes.eks
  metadata {
    name = "role-tokenreview-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "system:auth-delegator"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "minds-engine"
    namespace = "default"
  }

}