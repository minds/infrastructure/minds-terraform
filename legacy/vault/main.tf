terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "environments/vault/terraform.tfstate"
  }
}

# AWS providers

provider "aws" {
  region = "us-east-1"
}

provider "aws" {
  alias  = "eks"
  region = "us-east-1"

  assume_role {
    role_arn     = "arn:aws:iam::324044571751:role/${var.eks_assume_role_name}"
    session_name = "vault-terraform"
  }
}

data "aws_eks_cluster" "cluster" {
  name = var.eks_cluster_name
}

data "aws_eks_cluster_auth" "cluster" {
  provider = aws.eks
  name     = var.eks_cluster_name
}

data "aws_caller_identity" "current" {}


# EKS Kubernetes

provider "kubernetes" {
  alias                  = "eks"
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

provider "helm" {
  alias = "eks"
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    token                  = data.aws_eks_cluster_auth.cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  }
}
