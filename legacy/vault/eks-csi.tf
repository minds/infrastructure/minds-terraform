##################
### CSI Driver ###
##################

resource "helm_release" "csi_driver" {
  provider   = helm.eks
  name       = "csi"
  namespace  = kubernetes_namespace.vault.metadata[0].name
  chart      = "secrets-store-csi-driver"
  repository = "https://kubernetes-sigs.github.io/secrets-store-csi-driver/charts"

  wait = false

  set {
    name  = "syncSecret.enabled"
    value = "true"
  }
}
