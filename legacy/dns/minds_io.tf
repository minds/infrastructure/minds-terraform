resource "aws_route53_zone" "minds_io" {
  comment       = "Minds IO"
  force_destroy = "false"
  name          = "minds.io."
}

resource "aws_route53_record" "ns_minds_io" {
  name    = "minds.io"
  records = ["ns-1055.awsdns-03.org.", "ns-1977.awsdns-55.co.uk.", "ns-226.awsdns-28.com.", "ns-861.awsdns-43.net."]
  ttl     = "172800"
  type    = "NS"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "soa_minds_io" {
  name    = "minds.io"
  records = ["ns-226.awsdns-28.com. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
  ttl     = "900"
  type    = "SOA"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "a_wildcard_minds_io" {
  alias {
    evaluate_target_health = "false"
    name                   = "dualstack.k8s-traefik-traefik-3ab5d10366-153525964.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "*.minds.io"
  type    = "A"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "a_minds_io" {
  name    = "minds.io"
  records = ["68.195.244.134"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "a_ostk_minds_io" {
  alias {
    evaluate_target_health = "false"
    name                   = "dualstack.a5889448624b711e9ba680a5267c5537-1561559637.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "ostk.minds.io"
  type    = "A"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "cname_mark_minds_io" {
  name    = "mark.minds.io"
  records = ["pro-ingress-test.minds.com"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "cname__5b018e70eef8b98b47b5a52167c62a74_minds_io" {
  name    = "_5b018e70eef8b98b47b5a52167c62a74.minds.io"
  records = ["_80bfcebbf1fa07b26d03041625874d55.acm-validations.aws."]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "cname_iglu_minds_io" {
  name    = "iglu.minds.io"
  records = ["minds.gitlab.io"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "txt_minds_io" {
  name    = "minds.io"
  records = ["HfKF1LD"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "txt_www_minds_io" {
  name    = "www.minds.io"
  records = ["HfKF1LD"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_io.zone_id
}

resource "aws_route53_record" "txt_dzc_minds_io" {
  name    = "dzc.minds.io"
  records = ["HfKF1LD"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_io.zone_id
}


resource "aws_route53_record" "txt_gitlab-pages-verification-code_iglu_minds_io" {
  name    = "_gitlab-pages-verification-code.iglu.minds.io"
  records = ["gitlab-pages-verification-code=fa426dda177da9d73739da4e8742bbc8"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_io.zone_id
}
