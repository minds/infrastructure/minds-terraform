## Cloudflare has a list of IP address that can change
## To allow our security groups to stay in sync, we create a lambda process that
## will synchronise the ips

data "aws_vpc" "cf_vpc" {
  id = var.vpc_id
}

# The security group to apply to resources

resource "aws_security_group" "cf_security_group" {
  name        = "allow-cloudflare"
  description = "Allow inbound traffic from Cloudflare"
  vpc_id      = data.aws_vpc.cf_vpc.id

  tags = {
    Name        = "allow-cloudflare"
    environment = "CloudflareDNS"
  }
}

resource "aws_security_group_rule" "cf_security_group_ingress_http" {
  type              = "ingress"
  description       = "TLS from VPC"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = [data.aws_vpc.cf_vpc.cidr_block]
  security_group_id = aws_security_group.cf_security_group.id
}

resource "aws_security_group_rule" "cf_security_group_ingress_https" {
  type              = "ingress"
  description       = "TLS from VPC"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = [data.aws_vpc.cf_vpc.cidr_block]
  security_group_id = aws_security_group.cf_security_group.id
}

resource "aws_security_group_rule" "cf_security_group_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.cf_security_group.id
}

# Create an archive (zip) from the python script

data "archive_file" "cf_lambda_script" {
  type        = "zip"
  source_file = "${path.module}/lambda/cf-security-group-update.py"
  output_path = "${path.module}/lambda/cf-security-group-update.zip"
}

# The lambda function, with the environment variables

# resource "aws_lambda_function" "cf_lambda_function" {
#   description   = "This keeps our security group in sync with cloudflare"
#   function_name = "cf-security-group-update"
#   handler       = "cf-security-group-update.lambda_handler"
#   role          = aws_iam_role.cf_lambda_role.arn
#   runtime       = "python3.6"

#   filename         = data.archive_file.cf_lambda_script.output_path
#   source_code_hash = filebase64sha256(data.archive_file.cf_lambda_script.output_path)
#   timeout          = "60"

#   environment {
#     variables = {
#       SECURITY_GROUP_ID = aws_security_group.cf_security_group.id
#       PORTS_LIST        = "80,443"
#     }
#   }

#   tags = {
#     environment = "CloudflareDNS"
#   }
# }

# Triggers the lambda job on a schedule

resource "aws_cloudwatch_event_rule" "cf_lambda_schedule" {
  name                = "cf-security-group-update"
  description         = "Sync cloudflare ips with security group"
  schedule_expression = "rate(1 day)"
  role_arn            = aws_iam_role.cf_lambda_role.arn
}

# Permissions / IAM

resource "aws_iam_role" "cf_lambda_role" {
  assume_role_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Principal":{
        "Service": [
                "lambda.amazonaws.com",
                "events.amazonaws.com"
            ]
        },
      "Action":"sts:AssumeRole"
    }
  ]
}
EOF
  path               = "/service-role/"
  tags = {
    environment = "CloudflareDNS"
  }
}

resource "aws_iam_policy" "cf_lambda_policy" {
  policy = <<EOF
{
    "Version":"2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:DescribeSecurityGroups"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cf_lambda_role_policy_attachment" {
  role       = aws_iam_role.cf_lambda_role.name
  policy_arn = aws_iam_policy.cf_lambda_policy.arn
}
