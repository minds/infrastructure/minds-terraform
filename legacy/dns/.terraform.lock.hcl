# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.22.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:QF87L0J98NgpNMhOfOtFykadyzJ0FfkV2RPF6sf/FrA=",
    "h1:z5PrIPAF3ia9YcIVZ/vJEjBE29+qeP1JKsW3Lffjn+c=",
    "zh:12c7d968b14e54990a1c137f1b0b525eacefbcc857baf00c107c7a32ebb3f476",
    "zh:2202f5debee417cc367e12183803ecb373c4a7571b79dbb39e826ce88276184c",
    "zh:283b16ee3cb1bac96d9a1db588e0bb0272e7a89d5ac101243f8758c81f70ec7c",
    "zh:40f635c1c455213cb510adb043c385686077ebeda1d3038b9c1f252a7a662991",
    "zh:5c05c8e81d125abf5db9a337de8dbb48ac44eb59bcfa927f1f1addce49449346",
    "zh:7f893d384ee4e798391ef0532046e593b95a5ab2b328e6fff1a81ef27cf86133",
    "zh:a39c728c083fa695fc6b7134dd3cf57996aeb1098faca2cbc40fb775e01e5d7a",
    "zh:b6078ff29c6fcb30217df242c668cf7db0108f57a6711347aa5bbd365a09f4ca",
    "zh:c8cb07aad4fc0e4d082f931888e6ac6c0ac093e84a3f8821163d751d605641af",
    "zh:d4998ca9a69ac0f4f42b8c02bfbfd572cd16acd9aa1c76e0e28ccc983c09611e",
    "zh:d514ada82cfe18e85c203bffd3435deb01d2b59e276261701c802be2f1296f5b",
    "zh:e3e58fc11651db0db4fa221a384d78032da6cf603cd74228a57b3b47c8711c30",
    "zh:ee2c2290f78c694116bb05955ed5c56165eacf0e3fe28c43eda55e14882b3f7f",
    "zh:f4f2dc474dbbee45257fed660782c6967d365bc63520ff5439e7bc4d6ad91acf",
  ]
}

provider "registry.terraform.io/hashicorp/archive" {
  version = "2.2.0"
  hashes = [
    "h1:2K5LQkuWRS2YN1/YoNaHn9MAzjuTX8Gaqy6i8Mbfv8Y=",
    "h1:CIWi5G6ob7p2wWoThRQbOB8AbmFlCzp7Ka81hR3cVp0=",
    "zh:06bd875932288f235c16e2237142b493c2c2b6aba0e82e8c85068332a8d2a29e",
    "zh:0c681b481372afcaefddacc7ccdf1d3bb3a0c0d4678a526bc8b02d0c331479bc",
    "zh:100fc5b3fc01ea463533d7bbfb01cb7113947a969a4ec12e27f5b2be49884d6c",
    "zh:55c0d7ddddbd0a46d57c51fcfa9b91f14eed081a45101dbfc7fd9d2278aa1403",
    "zh:73a5dd68379119167934c48afa1101b09abad2deb436cd5c446733e705869d6b",
    "zh:841fc4ac6dc3479981330974d44ad2341deada8a5ff9e3b1b4510702dfbdbed9",
    "zh:91be62c9b41edb137f7f835491183628d484e9d6efa82fcb75cfa538c92791c5",
    "zh:acd5f442bd88d67eb948b18dc2ed421c6c3faee62d3a12200e442bfff0aa7d8b",
    "zh:ad5720da5524641ad718a565694821be5f61f68f1c3c5d2cfa24426b8e774bef",
    "zh:e63f12ea938520b3f83634fc29da28d92eed5cfbc5cc8ca08281a6a9c36cca65",
    "zh:f6542918faa115df46474a36aabb4c3899650bea036b5f8a5e296be6f8f25767",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version = "4.28.0"
  hashes = [
    "h1:TXCUuuaf2q54C43bxSNiF9g+cxTr8zqEZem0pW15cjE=",
    "h1:Z7sun0kcsON43rhvjmlv0nzkL2XMoBUTGLdCIFTmJ4I=",
    "zh:1d4806e50971d2cd565273cedf3206e38931677a6f546cf2b9fb140b52b80604",
    "zh:3f076791002b8afa5ba2d2038f1e1db5956022327eb5242152723ed410ae4571",
    "zh:40e5944a9df0d083dbd316bcc6ac9ceada5c00dab70c21897e62b68c4c936bc9",
    "zh:68b78d0c1866aa0bcbbadb1cf51349c9af697f8789f5778b7e7e2912a9c4845d",
    "zh:72d6e66136841c0e5ae264e03555cf59751ddae1b9784eafcb877c624332c70a",
    "zh:902c8f89dc10d321b87c09270c27a31a42d4e74e4da1608e55b7f241cd010a62",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:bf54c9f55d420b4e1fe68db81a759c40b9f9747159dea3061212a1c9768dcdfd",
    "zh:bfbe7e745c420a4ebd27ca35dfe5c2acc7cdd05092e1daf60f5ae29a1130d752",
    "zh:d271a30b16f0861f020e423d120d1458cf1757e740e016ace22084c39dc13550",
    "zh:f1e4672d1625fd1f1268d4b807cb90e28150d46fb2d0dd0836de65db29c8d5e6",
    "zh:f5cee910b4db2da3c2a28dae9055cbca4273eb774c362bb7bb5bde04deff4557",
  ]
}
