# Minds DNS

DNS configurations for Minds.

## Naming conventions

### Zones

The name of the 'zone' ("aws_route53_zone") resource should be the root level domain following the snake case pattern. For example, `minds.com` should be called `minds_com`.

```tf
resource "aws_route53_zone" "minds_com" {
    ...
}
```

### Records

The name of the 'record' ("aws_route53_record") resources should also follow the snake case pattern and be prefixed with the record type. For example, an **A** record for `minds.com` should be called `a_minds_com`.

```tf
resource "aws_route53_record" "a_minds_com" {
  name    = "minds.com"
  records = [ "127.0.0.1" ]
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}
```

As the `*` wildcard character is not supported with terraform, we can replace it with the word `wildcard`. For example `*.minds.com` should be called `wildcard_minds_com`.
