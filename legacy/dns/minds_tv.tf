resource "aws_route53_zone" "minds_tv" {
  comment       = "Minds TV"
  force_destroy = "false"
  name          = "minds.tv."
}

resource "aws_route53_record" "ns_minds_tv" {
  name    = "minds.tv"
  records = ["ns-746.awsdns-29.net.", "ns-1651.awsdns-14.co.uk.", "ns-283.awsdns-35.com.", "ns-1412.awsdns-48.org."]
  ttl     = "172800"
  type    = "NS"
  zone_id = aws_route53_zone.minds_tv.zone_id
}

resource "aws_route53_record" "soa_minds_tv" {
  name    = "minds.tv"
  records = ["ns-946.awsdns-54.net. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
  ttl     = "900"
  type    = "SOA"
  zone_id = aws_route53_zone.minds_tv.zone_id
}

resource "aws_route53_record" "cname__f5e42f836fb9307c5abdf2b3e15ebd7d_minds_tv" {
  name    = "_f5e42f836fb9307c5abdf2b3e15ebd7d.minds.tv"
  records = ["_056594851a045d5adedba881ff65fb13.kirrbxfjtw.acm-validations.aws."]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_tv.zone_id
}

resource "aws_route53_record" "a_wildcard_minds_tv" {
  alias {
    evaluate_target_health = "false"
    name                   = "a2fe45a25a36c4bd999c4db7cf1c5d2a-959553126.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "*.minds.tv"
  type    = "A"
  zone_id = aws_route53_zone.minds_tv.zone_id
}

resource "aws_route53_record" "a_minds_tv" {
  alias {
    evaluate_target_health = "false"
    name                   = "a2fe45a25a36c4bd999c4db7cf1c5d2a-959553126.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "minds.tv"
  type    = "A"
  zone_id = aws_route53_zone.minds_tv.zone_id
}

resource "aws_route53_record" "txt_www_minds_tv" {
  name    = "www.minds.tv"
  records = ["HfKF1LD"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_tv.zone_id
}

resource "aws_route53_record" "txt_dzc_minds_tv" {
  name    = "dzc.minds.tv"
  records = ["HfKF1LD"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_tv.zone_id
}

resource "aws_route53_record" "txt_minds_tv" {
  name    = "minds.tv"
  records = ["HfKF1LD", "google-site-verification=iAJ1VWEaOX6UPi_9gdXD-CVRME6V7E5_iHBgFXc9YpI"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_tv.zone_id
}
