variable "cloudflare_zone_id" {
  type = string
}

variable "cloudflare_api_token" {
  type      = string
  sensitive = true
}

# We use vpc_id for the security group
variable "vpc_id" {
  type = string
}
