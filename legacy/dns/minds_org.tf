resource "aws_route53_zone" "minds_org" {
  comment       = "Minds.ORG"
  force_destroy = "false"
  name          = "minds.org."
}

resource "aws_route53_record" "ns_minds_org" {
  name    = "minds.org"
  records = ["ns-48.awsdns-06.com.", "ns-667.awsdns-19.net.", "ns-1472.awsdns-56.org.", "ns-1830.awsdns-36.co.uk."]
  ttl     = "172800"
  type    = "NS"
  zone_id = aws_route53_zone.minds_org.zone_id
}

resource "aws_route53_record" "soa_minds_org" {
  name    = "minds.org"
  records = ["ns-1830.awsdns-36.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
  ttl     = "900"
  type    = "SOA"
  zone_id = aws_route53_zone.minds_org.zone_id
}

resource "aws_route53_record" "a_wildcard_minds_org" {
  alias {
    evaluate_target_health = "false"
    name                   = "a2fe45a25a36c4bd999c4db7cf1c5d2a-959553126.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "*.minds.org"
  type    = "A"
  zone_id = aws_route53_zone.minds_org.zone_id
}

resource "aws_route53_record" "a_minds_org" {
  alias {
    evaluate_target_health = "false"
    name                   = "a2fe45a25a36c4bd999c4db7cf1c5d2a-959553126.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "minds.org"
  type    = "A"
  zone_id = aws_route53_zone.minds_org.zone_id
}

resource "aws_route53_record" "txt_minds_org" {
  name    = "minds.org"
  records = ["B58MAFZX", "google-site-verification=fe837bqcsaPufvmOV_4RPYvHJQfLsPH4Fis1KtHulQU"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_org.zone_id
}
