resource "cloudflare_record" "root_com" {
  name    = "minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "a2fe45a25a36c4bd999c4db7cf1c5d2a-959553126.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "wildcard_com" {
  name    = "*.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "a2fe45a25a36c4bd999c4db7cf1c5d2a-959553126.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "www_com" {
  name    = "www.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "k8s-traefik-traefik-41f2e6f36e-88732639.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "cdn_com" {
  name    = "cdn.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "d3ae0shxev0cb7.cloudfront.net"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "cdnassets_com" {
  name    = "cdn-assets.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "d15u56mvtglc6v.cloudfront.net"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "cdncinemr_com" {
  name    = "cdn-cinemr.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "d2isvgrdif6ua5.cloudfront.net"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "status_com" {
  name    = "status.minds.com"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "1h7b1b31wy99.stspg-customer.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "sockets_com" {
  name    = "ha-socket-alb-io-us-east-1.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "k8s-traefik-traefik-41f2e6f36e-88732639.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "developers_com" {
  name    = "developers.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "minds.gitlab.io"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "dashboards_com" {
  name    = "dashboards.minds.com"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = "34.107.233.174"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "translate_com" {
  name    = "translate.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "cname.crowdin.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "checkout_com" {
  name    = "checkout.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "dualstack.checkout-396317781.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "protraefik_com" {
  name    = "pro-traefik.minds.com"
  proxied = false
  ttl     = 120
  type    = "A"
  value   = "52.86.159.246"
  zone_id = var.cloudflare_zone_id
}

## Chat

resource "cloudflare_record" "wildcard_matrix_com" {
  name    = "*.matrix.minds.com"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "34.117.128.220"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "hat_com" {
  name    = "chat.minds.com"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "34.117.128.220"
  zone_id = var.cloudflare_zone_id
}

## Analytics

resource "cloudflare_record" "sp_com" {
  name    = "sp.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "dualstack.aadaa6daaae6444f3a0b758c8a952bd7-1673993051.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "kibana_com" {
  name    = "kibana.minds.com"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "dualstack.minds-1604594883.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "analytics_com" {
  name    = "analytics.minds.com"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "dualstack.a926fb84a890d419fafeac7ddd48644c-1814374511.us-east-1.elb.amazonaws.com"
  zone_id = var.cloudflare_zone_id
}


## Gitlab verification

resource "cloudflare_record" "txt_developers_gitlab_com" {
  name    = "_gitlab-pages-verification-code.developers.minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "gitlab-pages-verification-code=f4d13f8be66226421b1a06ab252e47a2"
  zone_id = var.cloudflare_zone_id
}

## Protonmail

resource "cloudflare_record" "mx20_com" {
  name     = "minds.com"
  proxied  = false
  ttl      = 120
  type     = "MX"
  priority = 20
  value    = "mailsec.protonmail.ch"
  zone_id  = var.cloudflare_zone_id
}

resource "cloudflare_record" "mx10_com" {
  name     = "minds.com"
  proxied  = false
  ttl      = 120
  type     = "MX"
  priority = 10
  value    = "mail.protonmail.ch"
  zone_id  = var.cloudflare_zone_id
}

resource "cloudflare_record" "protonmail_txt_domainkey" {
  name    = "protonmail._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDB3u+QYUkqaTBtYaO5tIRx5t/rLCCunwfsvaETKNWpplwyX9H09OeKkWLVOVAvMUUdTYA/vXTzidhY2vpvyVyyKD74bTzfz4uJZiEEo7v0o9j7baowDrUa5JU8NmmMRIGUTXi9/DilA1VuO7z6oYSnmW4K99ucIMZK+PkBJWihCwIDAQAB"
  zone_id = var.cloudflare_zone_id
}


## Sendgrid verification

resource "cloudflare_record" "sendgrid_cname_1" {
  name    = "s1._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "s1.domainkey.u81420.wl171.sendgrid.net"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "sendgrid_cname_2" {
  name    = "s2._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "s2.domainkey.u81420.wl171.sendgrid.net"
  zone_id = var.cloudflare_zone_id
}


## Webmaster tools verification

resource "cloudflare_record" "txt1_com" {
  name    = "minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "protonmail-verification=2664832620177c791e0feb4bd661a589ebd38b49"
  zone_id = var.cloudflare_zone_id
}
resource "cloudflare_record" "txt2_com" {
  name    = "minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "google-site-verification=IlgABuR33fbWH-81hJH4uGR08QhUGwX2RbgaOS0oLuw"
  zone_id = var.cloudflare_zone_id
}
resource "cloudflare_record" "txt3_com" {
  name    = "minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "v=spf1 include:amazonses.com include:_spf.protonmail.ch -all"
  zone_id = var.cloudflare_zone_id
}
resource "cloudflare_record" "txt4_com" {
  name    = "minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "brave-ledger-verification=76e3cc1fc7bfd06cddac92ef36d76ca392b801683a506c550fe54169d155ede6"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "wildcard_tx" {
  name    = "*.minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "knym7S8T"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "www_tx" {
  name    = "www.minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "yandex-verification: ccdc537adbfac9a8"
  zone_id = var.cloudflare_zone_id
}

# Stripe verification


resource "cloudflare_record" "stripe_txt" {
  name    = "minds.com"
  proxied = false
  ttl     = 120
  type    = "TXT"
  value   = "stripe-verification=60b8e87d9c8b2f84127412b82e7fc3a7bfdfc731aa40a1852ff73a521b6aaf16"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "stripe_cname_domainkey1" {
  name    = "nlq33bwln7hikedqwc7r44quq25zyal2._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "nlq33bwln7hikedqwc7r44quq25zyal2.dkim.custom-email-domain.stripe.com."
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "stripe_cname_domainkey2" {
  name    = "iui7z5priy7tlwv4p4vjdr5o5u5j3baf._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "iui7z5priy7tlwv4p4vjdr5o5u5j3baf.dkim.custom-email-domain.stripe.com."
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "stripe_cname_domainkey3" {
  name    = "5aimlmopcdv6sqjbr2f4xtimxzutelvk._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "5aimlmopcdv6sqjbr2f4xtimxzutelvk.dkim.custom-email-domain.stripe.com."
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "stripe_cname_domainkey4" {
  name    = "siiaxz5alb5fxhmacqpdbxunefpcblcm._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "siiaxz5alb5fxhmacqpdbxunefpcblcm.dkim.custom-email-domain.stripe.com."
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "stripe_cname_domainkey5" {
  name    = "bwryhrzptydi7zdmlkfjlww4xdmweasj._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "bwryhrzptydi7zdmlkfjlww4xdmweasj.dkim.custom-email-domain.stripe.com."
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "stripe_cname_domainkey6" {
  name    = "bs3waiurcp2p6ar4ouastimjoseiwwbx._domainkey.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "bs3waiurcp2p6ar4ouastimjoseiwwbx.dkim.custom-email-domain.stripe.com."
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "stripe_cname_bounce" {
  name    = "bounce.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "custom-email-domain.stripe.com."
  zone_id = var.cloudflare_zone_id
}

# 

resource "cloudflare_record" "zeroheight_cname" {
  name    = "design.minds.com"
  proxied = false
  ttl     = 120
  type    = "CNAME"
  value   = "zeroheight.com."
  zone_id = var.cloudflare_zone_id
}
