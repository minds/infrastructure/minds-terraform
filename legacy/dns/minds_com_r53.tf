resource "aws_route53_zone" "minds_com" {
  comment       = "Minds.COM (terraform wip)"
  force_destroy = "false"
  name          = "minds.com."
}

resource "aws_route53_record" "ns_minds_com" {
  name = "minds.com"
  records = [
    "ns-1011.awsdns-62.net.",
    "ns-1331.awsdns-38.org.",
    "ns-1873.awsdns-42.co.uk.",
    "ns-84.awsdns-10.com."
  ]
  ttl     = "172800"
  type    = "NS"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "soa_minds_com" {
  name    = "minds.com"
  records = ["ns-1011.awsdns-62.net. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
  ttl     = "900"
  type    = "SOA"
  zone_id = aws_route53_zone.minds_com.zone_id
}

## TODO ##

resource "aws_route53_record" "a_wildcard_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-app-1793160088.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "*.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt_wildcard_minds_com" {
  name    = "*.minds.com"
  records = ["knym7S8T"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_4ytyzny5hzkczb632mr2_irl_minds_com" {
  name    = "4ytyzny5hzkczb632mr2.irl.minds.com"
  records = ["verify.squarespace.com"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_5t7rlm3w63tax6qdgt356bbdf45rvduf__domainkey_minds_com" {
  name    = "5t7rlm3w63tax6qdgt356bbdf45rvduf._domainkey.minds.com"
  records = ["5t7rlm3w63tax6qdgt356bbdf45rvduf.dkim.amazonses.com"]
  ttl     = "1800"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_81420_emails_minds_com" {
  name    = "81420.emails.minds.com"
  records = ["sendgrid.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_81420_minds_com" {
  name    = "81420.minds.com"
  records = ["sendgrid.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname__438807f6e37da0b38d103914df202e65_oss_minds_com" {
  name    = "_438807f6e37da0b38d103914df202e65.oss.minds.com"
  records = ["_a598b60fdd659d602724f745a13a5428.duyqrilejt.acm-validations.aws."]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt__amazonses_minds_com" {
  name    = "_amazonses.minds.com"
  records = ["f+GBg6na7/tF1/q9XdF4prjt+4WuBkrOknx/fkTb3N8="]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt__dmarc_minds_com" {
  name    = "_dmarc.minds.com"
  records = ["v=DMARC1; p=none; sp=none; rua=mailto:dmarc@mailinblue.com!10m; ruf=mailto:dmarc@mailinblue.com!10m; rf=afrf; pct=100; ri=86400"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt__gitlab-pages-verification-code_design_minds_com" {
  name    = "_gitlab-pages-verification-code.design.minds.com"
  records = ["gitlab-pages-verification-code=478719fad1451f454aa1d9b0a14e5e18"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt__gitlab-pages-verification-code_developers_minds_com" {
  name    = "_gitlab-pages-verification-code.developers.minds.com"
  records = ["gitlab-pages-verification-code=f4d13f8be66226421b1a06ab252e47a2"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt__gitlab-pages-verification-code_oss_minds_com" {
  name    = "_gitlab-pages-verification-code.oss.minds.com"
  records = ["gitlab-pages-verification-code=c0f7d603f303c56a255581f8ccbea3c2"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt__gitlab-pages-verification-code_status_minds_com" {
  name    = "_gitlab-pages-verification-code.status.minds.com"
  records = ["gitlab-pages-verification-code=3619ccb1d1ea810cd108d7091cfa1379"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_abbeyroad_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-1604594883.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "abbeyroad.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_bastion_vpc_us-east_aws_minds_com" {
  name    = "bastion.vpc.us-east.aws.minds.com"
  records = ["54.85.183.19"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_cassandra_minds_com" {
  name    = "cassandra.minds.com"
  records = ["54.208.192.105"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_cdn-assets_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "d15u56mvtglc6v.cloudfront.net"
    zone_id                = "Z2FDTNDATAQYW2"
  }

  name    = "cdn-assets.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_cdn-cinemr_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "d2isvgrdif6ua5.cloudfront.net"
    zone_id                = "Z2FDTNDATAQYW2"
  }

  name    = "cdn-cinemr.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_cdn-proxy_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "multisite2loadbalancer2-1442974952.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "cdn-proxy.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_cdn_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "d3ae0shxev0cb7.cloudfront.net"
    zone_id                = "Z2FDTNDATAQYW2"
  }

  name    = "cdn.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_chat-002D-ssl_minds_com" {
  name    = "chat-ssl.minds.com"
  records = ["107.21.42.113"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_checkout_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "checkout-396317781.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "checkout.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_cinemr_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "dualstack.elggload-1650695692.us-east-1.elb.amazonaws.com."
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "cinemr.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_dashboards_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "grafana-749103589.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "dashboards.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_design_minds_com" {
  name    = "design.minds.com"
  records = ["minds.gitlab.io"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_developers_minds_com" {
  name    = "developers.minds.com"
  records = ["minds.gitlab.io"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_eggman_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-1604594883.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "eggman.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_em2244_minds_com" {
  name    = "em2244.minds.com"
  records = ["u81420.wl171.sendgrid.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_fs_minds_com" {
  name    = "fs.minds.com"
  records = ["d77b44eaj2gpn.cloudfront.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_ha-socket-alb-io-us-east-1_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "sockets-alb-383466297.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "ha-socket-alb-io-us-east-1.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_ha-socket-io-us-east-1_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "sockets-alb-383466297.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "ha-socket-io-us-east-1.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_hosting_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-1604594883.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "hosting.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_irl_minds_com" {
  name    = "irl.minds.com"
  records = ["198.185.159.145", "198.185.159.144", "198.49.23.144", "198.49.23.145"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_john_minds_com" {
  name    = "john.minds.com"
  records = ["52.86.159.246"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_k1__domainkey_minds_com" {
  name    = "k1._domainkey.minds.com"
  records = ["dkim.mcsv.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_kibana_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-1604594883.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "kibana.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_local_loop_minds_com" {
  name    = "local.loop.minds.com"
  records = ["127.0.0.1"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_m_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-app-1793160088.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "m.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt_mandrill__domainkey_minds_com" {
  name    = "mandrill._domainkey.minds.com"
  records = ["facebook-domain-verification=9kgdv9kvsqo4gns8mi9qcm65cfu4oq", "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrLHiExVd55zd/IQ/J/mRwSRMAocV/hMB3jXwaHH36d9NaVynQFYV8NaWi69c1veUtRzGt7yAioXqLj7Z4TeEUoOLgrKsn8YnckGs9i3B3tVFB+Ch/4mPhXWiNfNdynHWBcPcbJ8kjEQ2U8y78dHZj1YeRXXVvWob2OaKynO8/lQIDAQAB;"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_mark_minds_com" {
  name    = "mark.minds.com"
  records = ["www.minds.com"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "aaaa_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-app-1793160088.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "minds.com"
  type    = "AAAA"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-app-1793160088.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "mx_minds_com" {
  name    = "minds.com"
  records = ["10 mail.protonmail.ch", "20 mailsec.protonmail.ch"]
  ttl     = "300"
  type    = "MX"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt_minds_com" {
  name    = "minds.com"
  records = ["google-site-verification=IlgABuR33fbWH-81hJH4uGR08QhUGwX2RbgaOS0oLuw", "v=spf1 include:amazonses.com include:_spf.protonmail.ch -all", "protonmail-verification=2664832620177c791e0feb4bd661a589ebd38b49", "brave-ledger-verification=76e3cc1fc7bfd06cddac92ef36d76ca392b801683a506c550fe54169d155ede6", "yandex-verification: ccdc537adbfac9a8"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_mobile_minds_com" {
  name    = "mobile.minds.com"
  records = ["www.minds.com"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt_nomad_minds_com" {
  name    = "nomad.minds.com"
  records = ["datkey=0a69de666238867a69f54acc8ec5dc90422f85772dc64bcb4645042886fc1a5a"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_oss_minds_com" {
  name    = "oss.minds.com"
  records = ["minds.gitlab.io"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_pro-ingress-test_minds_com" {
  name    = "pro-ingress-test.minds.com"
  records = ["52.86.159.246"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_pro-ingress_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-app-1793160088.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "pro-ingress.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_pro-traefik_minds_com" {
  name    = "pro-traefik.minds.com"
  records = ["52.86.159.246"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt_protonmail__domainkey_minds_com" {
  name    = "protonmail._domainkey.minds.com"
  records = ["v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDB3u+QYUkqaTBtYaO5tIRx5t/rLCCunwfsvaETKNWpplwyX9H09OeKkWLVOVAvMUUdTYA/vXTzidhY2vpvyVyyKD74bTzfz4uJZiEEo7v0o9j7baowDrUa5JU8NmmMRIGUTXi9/DilA1VuO7z6oYSnmW4K99ucIMZK+PkBJWihCwIDAQAB"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_rjwkg7dzzsnmdkk6eoirhcamycujpbcc__domainkey_minds_com" {
  name    = "rjwkg7dzzsnmdkk6eoirhcamycujpbcc._domainkey.minds.com"
  records = ["rjwkg7dzzsnmdkk6eoirhcamycujpbcc.dkim.amazonses.com"]
  ttl     = "1800"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_rubbersoul_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-1604594883.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "rubbersoul.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "s1__domainkey_minds_com" {
  name    = "s1._domainkey.minds.com"
  records = ["s1.domainkey.u81420.wl171.sendgrid.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_s2__domainkey_minds_com" {
  name    = "s2._domainkey.minds.com"
  records = ["s2.domainkey.u81420.wl171.sendgrid.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_solix_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "solix-1874339154.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "solix.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_status_minds_com" {
  name    = "status.minds.com"
  records = ["minds.gitlab.io"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_thumnails_minds_com" {
  name    = "thumnails.minds.com"
  records = ["dladfude8tdj2.cloudfront.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_url3554_minds_com" {
  name    = "url3554.minds.com"
  records = ["sendgrid.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_url9906_emails_minds_com" {
  name    = "url9906.emails.minds.com"
  records = ["sendgrid.net"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_walrus_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-1604594883.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "walrus.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_www_irl_minds_com" {
  name    = "www.irl.minds.com"
  records = ["ext-cust.squarespace.com"]
  ttl     = "300"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_www_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-app-1793160088.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "www.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "txt_www_minds_com" {
  name    = "www.minds.com"
  records = ["yandex-verification: ccdc537adbfac9a8"]
  ttl     = "300"
  type    = "TXT"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_xmpp_minds_com" {
  name    = "xmpp.minds.com"
  records = ["68.195.220.45"]
  ttl     = "300"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "a_yellowsubmarine_minds_com" {
  alias {
    evaluate_target_health = "false"
    name                   = "minds-1604594883.us-east-1.elb.amazonaws.com"
    zone_id                = "Z35SXDOTRQ7X7K"
  }

  name    = "yellowsubmarine.minds.com"
  type    = "A"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_zflghtbnuvoafy3n4nustfc6nhhhn6qa__domainkey_minds_com" {
  name    = "zflghtbnuvoafy3n4nustfc6nhhhn6qa._domainkey.minds.com"
  records = ["zflghtbnuvoafy3n4nustfc6nhhhn6qa.dkim.amazonses.com"]
  ttl     = "1800"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}

resource "aws_route53_record" "cname_translate_minds_com" {
  name    = "translate.minds.com"
  records = ["cname.crowdin.com"]
  ttl     = "1800"
  type    = "CNAME"
  zone_id = aws_route53_zone.minds_com.zone_id
}
