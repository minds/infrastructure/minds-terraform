##################
### CloudWatch ###
##################

resource "aws_cloudwatch_event_rule" "this" {
  name                = "${var.name_prefix}-sync"
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "this" {
  target_id = "${var.name_prefix}-sync"
  rule      = aws_cloudwatch_event_rule.this.name
  arn       = aws_lambda_function.this.arn
}
