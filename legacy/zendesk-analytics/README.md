# Zendesk Analytics

## Package Lambda

***Note*** This Lambda depends upon an AWS secret to be created in Secrets Manager with `username` and `password` fields for the Zendesk credentials to use. 

Before applying, you need to package the Lambda by running these commands:

```bash
cd lambda/src
npm install
npm run package
```

Which will produce a `function.zip` archive in the `lambda` directory. You can continually run the `package` script to update the zip archive.

## Deploy Terraform

After packaging the Lambda, you can deploy this module with:

```bash
terraform init
terraform apply
```
