###############
### Secrets ###
###############

data "aws_secretsmanager_secret" "this" { name = var.aws_secret_name }

###########
### IAM ###
###########

// IAM Role
resource "aws_iam_role" "lambda" {
  name               = "${var.name_prefix}-LambdaExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume_role.json
}

data "aws_iam_policy_document" "lambda_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

// Inline Policy
resource "aws_iam_role_policy" "lambda" {
  role   = aws_iam_role.lambda.id
  policy = data.aws_iam_policy_document.lambda_execution.json
}

data "aws_iam_policy_document" "lambda_execution" {

  // Allow
  statement {
    actions = ["s3:ListBucket", "s3:PutObject"]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*"
    ]
  }

  // Allow reading secret value for API credentials
  statement {
    actions = ["secretsmanager:GetSecretValue"]

    resources = [data.aws_secretsmanager_secret.this.arn]
  }
}

##############
### Lambda ###
##############

resource "aws_lambda_function" "this" {
  function_name = var.name_prefix
  role          = aws_iam_role.lambda.arn

  // src
  filename         = "./lambda/function.zip"
  source_code_hash = filebase64sha256("./lambda/function.zip")

  // runtime
  runtime = "nodejs16.x"
  handler = "index.handler"

  timeout = 900

  environment {
    variables = {
      BUCKET_NAME      = aws_s3_bucket.this.bucket
      ZENDESK_BASE_URI = var.zendesk_base_uri
      SECRET_NAME      = var.aws_secret_name
      FILE_NAME        = "tickets/tickets.json"
    }
  }
}

resource "aws_lambda_permission" "cloudwatch" {
  statement_id  = "CloudWatchInvokeLambda"
  principal     = "events.amazonaws.com"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  source_arn    = aws_cloudwatch_event_rule.this.arn
}
