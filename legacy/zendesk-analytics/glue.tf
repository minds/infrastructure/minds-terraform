############
### Glue ###
############

// Database

resource "aws_glue_catalog_database" "this" { name = var.name_prefix }

// Table

resource "aws_glue_catalog_table" "tickets" {
  database_name = aws_glue_catalog_database.this.name
  name          = "tickets"
  table_type    = "EXTERNAL_TABLE"

  parameters = {
    EXTERNAL           = "TRUE"
    has_encrypted_data = "true"
  }

  storage_descriptor {
    compressed        = "true"
    input_format      = "org.apache.hadoop.mapred.TextInputFormat"
    location          = "s3://${aws_s3_bucket.this.bucket}/tickets/"
    number_of_buckets = "-1"
    output_format     = "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"

    dynamic "columns" {
      for_each = var.glue_catalog_table_columns
      content {
        name = columns.key
        type = columns.value
      }
    }

    ser_de_info {
      parameters            = { "serialization.format" = "1" }
      serialization_library = "org.openx.data.jsonserde.JsonSerDe"
    }
  }
}
