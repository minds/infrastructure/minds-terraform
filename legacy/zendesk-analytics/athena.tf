##############
### Athena ###
##############

resource "aws_athena_workgroup" "this" {
  name = "${var.name_prefix}-tickets"

  configuration {
    result_configuration {
      output_location = "s3://${aws_s3_bucket.this.bucket}/output/"
      encryption_configuration {
        encryption_option = "SSE_S3"
      }
    }
  }
}
