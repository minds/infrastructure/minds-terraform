const axios = require('axios');
const AWS = require('aws-sdk');


// ENV variables
const BUCKET_NAME = process.env.BUCKET_NAME;
const ZENDESK_BASE_URI = process.env.ZENDESK_BASE_URI;
const FILE_NAME = process.env.FILE_NAME;
const SECRET_NAME = process.env.SECRET_NAME;
const TICKETS_ENDPOINT = `${ZENDESK_BASE_URI}/api/v2/tickets`;

// AWS clients
const s3Client = new AWS.S3();
const secretsClient = new AWS.SecretsManager({ region: 'us-east-1' });

// Retrieve all Zendesk tickets
const getTickets = async () => {

  // Retrieve Zendesk credentials from Secrets Manager
  const value = await secretsClient.getSecretValue({ SecretId: SECRET_NAME }).promise();
  const { username, password } = JSON.parse(value.SecretString);
  const auth = { username, password };

  try {
    let response = await axios.get(TICKETS_ENDPOINT, { auth });

    let tickets = response.data?.tickets || [];
    let next_page = response.data.next_page;

    // Loop through pages
    let page = 1;
    while (next_page) {
      response = await axios.get(next_page, { auth });
      tickets = (response.data?.tickets || []).concat(tickets);

      next_page = response.data.next_page;

      page++;
      console.log(`Page: ${page}`);
    }

    console.log(`Found ${page} pages.`)

    return tickets;
  } catch (error) {
    console.error(error);
  }
};

const uploadBucket = (filename, file) => {
  const bucketOptions = {
    Bucket: BUCKET_NAME,
    Key: filename,
    Body: file
  };

  s3Client.upload(
    bucketOptions,
    (err, data) => {
      if (err) {
        console.error(err);
      } else {
        console.log(data);
      }
    }
  );
};

exports.handler = async () => {
  // Retrieve tickets from Zendesk API
  const tickets = await getTickets();

  // Format tickets as JSON separated by newline
  let formatted = '';
  tickets.forEach(
    ticket => { formatted += `${JSON.stringify(ticket)}\n`; }
  );

  // Upload JSON file to S3 containing formatted tickets
  uploadBucket(FILE_NAME, Buffer.from(formatted));
};
