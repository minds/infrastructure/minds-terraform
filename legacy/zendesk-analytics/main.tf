provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Name   = var.name_prefix
      Module = "environments/zendesk-analytics"
    }
  }
}

terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "zendesk-analytics/terraform.tfstate"
  }
}
