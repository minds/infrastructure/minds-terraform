variable "name_prefix" {
  description = "Name prefix to apply on all created resources."
  type        = string

  default = "zendesk-analytics"
}

##############
### Lambda ###
##############

variable "zendesk_base_uri" {
  description = "Zendesk base URI to supply to the Lambda function."
  type        = string

  default = "https://support.minds.com"
}

variable "aws_secret_name" {
  description = "AWS secrets manager secret containing Zendesk credentials."
  type        = string

  default = "zendesk-analytics"
}

############
### Glue ###
############

variable "glue_catalog_table_columns" {
  type = map(string)

  // Commented out fields are objects, need to handle these if they are used in analytics
  default = {
    url         = "string",
    id          = "int",
    external_id = "string",
    // via 
    created_at  = "timestamp",
    updated_at  = "timestamp",
    type        = "string",
    subject     = "string",
    raw_subject = "string",
    // description      = "string",
    priority         = "string",
    status           = "string",
    recipient        = "string",
    requester_id     = "string",
    submitter_id     = "string",
    assignee_id      = "string",
    organization_id  = "string",
    group_id         = "string",
    collaborator_ids = "array<string>",
    follower_ids     = "array<string>",
    email_cc_ids     = "array<string>",
    forum_topic_id   = "string",
    problem_id       = "string",
    has_incidents    = "boolean",
    is_public        = "boolean",
    due_at           = "timestamp",
    tags             = "array<string>"
    // custom_fields
    // satisfaction_rating
    sharing_agreement_ids = "array<string>",
    followup_ids          = "array<string>",
    ticket_form_id        = "string",
    brand_id              = "string",
    allow_channelback     = "boolean",
    allow_attachments     = "boolean"
  }
}