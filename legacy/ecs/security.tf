
resource "aws_security_group" "ecs_security_group" {
  name        = "minds-ecs-instance"
  description = "Minds ECS Instances"
  vpc_id      = module.vpc.id

  tags = {
    Name = "minds-ecs-instance"
  }
}

resource "aws_security_group_rule" "ecs_security_group_ingress_from_loadbalancer" {
  type                     = "ingress"
  description              = "All ports from the load balancer"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.ecs_load_balancer_security_group.id
  security_group_id        = aws_security_group.ecs_security_group.id
}

##
# NOTE: This security group should not have ANY rules
#
resource "aws_security_group" "ecs_load_balancer_security_group" {
  name        = "allow-load-balance-access"
  description = "Allow the load balancers to access the ECS instances"
  vpc_id      = module.vpc.id

  tags = {
    Name = "allow-load-balance-access"
  }
}