
resource "aws_ecs_service" "production_ratelimits" {
  name            = "ratelimits"
  cluster         = aws_ecs_cluster.minds.id
  task_definition = aws_ecs_task_definition.production_ratelimits.arn

  deployment_maximum_percent         = 100
  deployment_minimum_healthy_percent = 0
  desired_count                      = 1

  deployment_controller {
    type = "ECS"
  }

  ordered_placement_strategy {
    field = "attribute:ecs.availability-zone"
    type  = "spread"
  }
  ordered_placement_strategy {
    field = "instanceId"
    type  = "spread"
  }
}

resource "aws_ecs_task_definition" "production_ratelimits" {
  family                = "ratelimits"
  container_definitions = file("task-definitions/production-ratelimits.json")

  requires_compatibilities = [
    "EC2",
  ]
}
