# Minds ECS

Elastic Container Service for Minds. NB: This will soon be deprecated as we begin to migrate to kubernetes.

## Scaling a service

For temporary changes run `terraform apply -var 'production_app={count:6}'`

For permanent changes, modify `minds.auto.tfvars`.
