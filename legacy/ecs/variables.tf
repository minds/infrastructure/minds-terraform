variable "production_app" {
  type = object({
    count = number
  })
}

variable "production_runners" {
  type = object({
    count = number
  })
}

variable "production_sockets" {
  type = object({
    count = number
  })
}

variable "canary_app" {
  type = object({
    count = number
  })
}


variable "staging_app" {
  type = object({
    count = number
  })
}
