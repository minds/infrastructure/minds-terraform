production_app = {
  count = 6
}

production_runners = {
  count = 2
}

production_sockets = {
  count = 2
}

canary_app = {
  count = 2
}

staging_app = {
  count = 1
}
