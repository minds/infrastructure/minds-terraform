
resource "aws_ecs_service" "staging_app" {
  name            = "staging"
  cluster         = aws_ecs_cluster.minds.id
  task_definition = aws_ecs_task_definition.staging_app.arn

  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 100
  desired_count                      = var.staging_app.count

  deployment_controller {
    type = "ECS"
  }

  load_balancer {
    container_name   = "nginx"
    container_port   = 80
    target_group_arn = aws_lb_target_group.staging_app.arn
  }

  ordered_placement_strategy {
    field = "attribute:ecs.availability-zone"
    type  = "spread"
  }
  ordered_placement_strategy {
    field = "instanceId"
    type  = "spread"
  }
}

resource "aws_ecs_task_definition" "staging_app" {
  family                = "staging-app"
  container_definitions = file("task-definitions/staging-app.json")

  requires_compatibilities = [
    "EC2",
  ]

}

resource "aws_lb_target_group" "staging_app" {
  name                 = "ecs-minds-staging"
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = module.vpc.id
  deregistration_delay = 30
}
