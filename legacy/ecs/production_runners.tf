
resource "aws_ecs_service" "production_runners" {
  name            = "runners"
  cluster         = aws_ecs_cluster.minds.id
  task_definition = aws_ecs_task_definition.production_runners.arn

  deployment_maximum_percent         = 150
  deployment_minimum_healthy_percent = 50
  desired_count                      = var.production_runners.count

  deployment_controller {
    type = "ECS"
  }

  ordered_placement_strategy {
    field = "attribute:ecs.availability-zone"
    type  = "spread"
  }
  ordered_placement_strategy {
    field = "instanceId"
    type  = "spread"
  }
}

resource "aws_ecs_task_definition" "production_runners" {
  family                = "production-runners"
  container_definitions = file("task-definitions/production-runners.json")
}
