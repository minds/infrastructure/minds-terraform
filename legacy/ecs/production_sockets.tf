
resource "aws_ecs_service" "production_sockets" {
  name            = "sockets"
  cluster         = aws_ecs_cluster.minds.id
  task_definition = aws_ecs_task_definition.production_sockets.arn

  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 50
  desired_count                      = var.production_sockets.count

  deployment_controller {
    type = "ECS"
  }

  load_balancer {
    container_name   = "sockets"
    container_port   = 3030
    target_group_arn = aws_lb_target_group.production_sockets.arn
  }

  ordered_placement_strategy {
    field = "attribute:ecs.availability-zone"
    type  = "spread"
  }
  ordered_placement_strategy {
    field = "instanceId"
    type  = "spread"
  }
}

resource "aws_ecs_task_definition" "production_sockets" {
  family                = "sockets"
  container_definitions = file("task-definitions/production-sockets.json")

  requires_compatibilities = [
    "EC2",
  ]
}

resource "aws_lb_target_group" "production_sockets" {
  name                 = "sockets-alb"
  port                 = 3030
  protocol             = "HTTP"
  vpc_id               = module.vpc.id
  deregistration_delay = 10
}
