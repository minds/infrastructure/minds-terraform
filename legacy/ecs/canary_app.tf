
resource "aws_ecs_service" "canary_app" {
  name            = "canary"
  cluster         = aws_ecs_cluster.minds.id
  task_definition = aws_ecs_task_definition.canary_app.arn

  deployment_maximum_percent         = 150
  deployment_minimum_healthy_percent = 100
  desired_count                      = var.canary_app.count

  deployment_controller {
    type = "ECS"
  }

  load_balancer {
    container_name   = "nginx"
    container_port   = 80
    target_group_arn = aws_lb_target_group.canary_app.arn
  }

  ordered_placement_strategy {
    field = "attribute:ecs.availability-zone"
    type  = "spread"
  }
  ordered_placement_strategy {
    field = "instanceId"
    type  = "spread"
  }
}

resource "aws_ecs_task_definition" "canary_app" {
  family                = "canary-app"
  container_definitions = file("task-definitions/canary-app.json")

  requires_compatibilities = [
    "EC2",
  ]
}

resource "aws_lb_target_group" "canary_app" {
  name                 = "ecs-minds-canary"
  port                 = 80
  protocol             = "HTTP"
  vpc_id               = module.vpc.id
  deregistration_delay = 60
}
