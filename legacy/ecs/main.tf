provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "minds-terraform"
    key    = "ecs/terraform.tfstate"
  }
}

module "vpc" {
  source = "../vpc"
}

resource "aws_ecs_cluster" "minds" {
  name = "minds"
}
