resource "kubernetes_namespace" "superhero" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "superhero"
    }

    labels = {
      name = "superhero"
    }

    name = "superhero"
  }
}


resource "helm_release" "superhero_school" {
  provider  = helm.eks
  name      = "matrix"
  namespace = kubernetes_namespace.superhero.metadata[0].name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx"
}