##

variable "gcp_project_id" {
  type        = string
  description = "The name of gcp project"
}

variable "gcp_region" {
  type        = string
  description = "The region of the gcp project"
}

variable "gke_cluster_name" {
  type        = string
  description = "The name of the GKE cluster"
}

##

variable "matrix_postgresql_password" {
  type        = string
  sensitive   = true
  description = "The password for the matrix user"
}

variable "pgpool_admin_password" {
  type        = string
  sensitive   = true
  description = "The password for the pgpool upgrades"
}