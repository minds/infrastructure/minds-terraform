resource "kubernetes_namespace" "postgresql" {
  metadata {
    annotations = {
      name = "postgresql"
    }

    labels = {
      name = "postgresql"
    }

    name = "postgresql"
  }
}

##
# Todo: move this to ../../modules directory
##
# resource "helm_release" "postgresql" {
#   name      = "postgresql"
#   namespace = kubernetes_namespace.postgresql.metadata[0].name

#   repository = "https://charts.bitnami.com/bitnami"
#   chart      = "postgresql-ha"
#   version    = "7.8.2"

#   set {
#     name  = "postgresql.podAntiAffinityPreset"
#     value = "hard"
#   }

#   set {
#     name  = "persistence.storageClass"
#     value = kubernetes_storage_class.postgresql_ssd.metadata[0].name
#   }

#   set {
#     name  = "persistence.size"
#     value = "128Gi"
#   }

#   set {
#     name = "postgresql.repmgrPassword"
#     value = random_password.postgresql_repmgr_password.result
#   }

#   set {
#     name = "postgresql.password"
#     value = random_password.postgresql_password.result
#   }

#   set {
#     name = "postgresql.resources.requests.cpu"
#     value = 4
#   }

#   set {
#     name = "postgresql.resources.limits.cpu"
#     value = 6
#   }

#   set {
#     name = "postgresql.resources.requests.memory"
#     value = "6Gi"
#   }

#   set {
#     name = "postgresql.resources.limits.memory"
#     value = "8Gi"
#   }

#   set {
#     name = "postgresql.readinessProbe.initialDelaySeconds"
#     value = 60
#   }

#   set {
#     name = "postgresql.livenessProbe.initialDelaySeconds"
#     value = 60
#   }

#   set {
#     name = "pgpool.resources.requests.cpu"
#     value = 0.25
#   }

#   set {
#     name = "pgpool.resources.limits.cpu"
#     value = 1
#   }

#   set {
#     name = "pgpool.resources.requests.memory"
#     value = "1Gi"
#   }

#   set {
#     name = "pgpool.resources.limits.memory"
#     value = "1.2Gi"
#   }

#   set {
#     name = "pgpool.replicaCount"
#     value = 2
#   }

#   set {
#     name = "pgpool.numInitChildren"
#     value = 320
#   }

#   set {
#     name = "pgpool.livenessProbe.enabled"
#     value = true
#   }

#   set {
#     name = "pgpool.adminPassword"
#     value = var.pgpool_admin_password
#   }

#   set {
#     name = "pgpool.srCheckDatabase"
#     value = "matrix"
#   }

#   # Custom users listed below

#   set {
#     name  = "pgpool.customUsers.usernames"
#     value = "matrix"
#   }

#   set {
#     name  = "pgpool.customUsers.passwords"
#     value = var.matrix_postgresql_password
#   }
# }

##
# Storage class for postgresql pods
##
resource "kubernetes_storage_class" "postgresql_ssd" {
  metadata {
    name = "storage-ssd-postgresql"
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  reclaim_policy      = "Retain"
  parameters = {
    type = "pd-ssd"
  }
  volume_binding_mode = "WaitForFirstConsumer"
}

resource "random_password" "postgresql_repmgr_password" {
  length           = 16
  special          = true
  override_special = "/@£$"

  lifecycle {
    ignore_changes = all
  }
}

resource "random_password" "postgresql_password" {
  length           = 16
  special          = true
  override_special = "/@£$"

  lifecycle {
    ignore_changes = all
  }
}