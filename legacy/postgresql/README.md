# Minds Postgresql Deployments

This is the production deployment of postgresql (ha) at Minds.com. This is used by Matrix.

## Workspace

The `default` workspace is the production environment.

## Deploying

```
terraform apply
```
