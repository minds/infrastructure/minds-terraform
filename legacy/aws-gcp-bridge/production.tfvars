gcp_region     = "us-east1"
gcp_project_id = "minds-production"
gcp_network_id = "projects/minds-production/global/networks/minds-production-vpc"

aws_vpc_id         = "vpc-e8ce7583"
aws_route_table_id = "rtb-ce4269a5"