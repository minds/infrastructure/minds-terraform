gcp_region     = "us-east1"
gcp_project_id = "minds-sandbox"
gcp_network_id = "projects/minds-sandbox/global/networks/minds-sandbox-vpc"

aws_vpc_id         = "vpc-e8ce7583"
aws_route_table_id = "rtb-ce4269a5"