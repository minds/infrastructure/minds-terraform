# GCP

variable "gcp_region" {
  type = string
}

variable "gcp_project_id" {
  type = string
}

variable "gcp_network_id" {
  type = string
}

# AWS

variable "aws_vpc_id" {
  type = string
}

variable "aws_route_table_id" {
  type = string
}