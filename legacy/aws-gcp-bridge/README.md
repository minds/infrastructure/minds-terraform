# AWS <-> GCP Bridge

This module creates a bridge between an AWS VPC and a GCP VPC.

## Assumptions

- You have functionig VPC's on both AWS and GCP
- Your subnets have a route table configured on both AWS and GCP
- You are not using more than one route table
- 2 tunnels are appropriate for your setup

## How to use

`terraform workspace new production`
`terraform apply --tf-vars=production.tfvars`

## Troubleshooting

### What is my aws_route_table_id?

This is the route table that your subnets use, and that will propogate ovet the VPN.

### My VPN connection is up but I can not ssh/ping/http any servers

Firewall configurations are out of the scope of this module. Ensure that your Firewall/Security groups allow ingress/egress to the relevant sources/targets.