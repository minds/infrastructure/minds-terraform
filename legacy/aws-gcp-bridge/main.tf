provider "aws" {
  region = "us-east-1"
}

provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

terraform {
  backend "s3" {
    bucket = "minds-terraform"
    key    = "aws-gcp-bridge"
  }
}

# GCP Gateway

resource "google_compute_ha_vpn_gateway" "ha_gateway" {
  region  = var.gcp_region
  name    = "ha-vpn-${var.aws_vpc_id}"
  network = var.gcp_network_id
}


# AWS Gateway

resource "aws_customer_gateway" "customer_gateway" {
  bgp_asn    = google_compute_router.router.bgp[0].asn
  ip_address = google_compute_ha_vpn_gateway.ha_gateway.vpn_interfaces[0].ip_address
  type       = "ipsec.1"

  tags = {
    Name = "aws-gcp-customer-gateway"
  }
}

resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = var.aws_vpc_id

  tags = {
    Name = "aws-gcp-vpn-gateway"
  }
}

# AWS VPN Connection

resource "aws_vpn_connection" "vpn_connection" {
  vpn_gateway_id      = aws_vpn_gateway.vpn_gateway.id
  customer_gateway_id = aws_customer_gateway.customer_gateway.id
  type                = "ipsec.1"
  static_routes_only  = false
}

# GCP External Gateway

resource "google_compute_external_vpn_gateway" "external_gateway" {
  name            = "aws-vpn-gateway-${var.aws_vpc_id}"
  redundancy_type = "TWO_IPS_REDUNDANCY"
  description     = "An externally managed VPN gateway"
  interface {
    id         = 0
    ip_address = aws_vpn_connection.vpn_connection.tunnel1_address
  }
  interface {
    id         = 1
    ip_address = aws_vpn_connection.vpn_connection.tunnel2_address
  }
}

# GCP Tunnels

resource "google_compute_vpn_tunnel" "tunnel1" {
  name                            = "tunnel1-${var.aws_vpc_id}"
  peer_external_gateway           = google_compute_external_vpn_gateway.external_gateway.id
  peer_external_gateway_interface = 0
  shared_secret                   = aws_vpn_connection.vpn_connection.tunnel1_preshared_key

  region                = google_compute_ha_vpn_gateway.ha_gateway.region
  vpn_gateway           = google_compute_ha_vpn_gateway.ha_gateway.id
  vpn_gateway_interface = 0

  router      = google_compute_router.router.name
  ike_version = 2
}

resource "google_compute_vpn_tunnel" "tunnel2" {
  name                            = "tunnel2-${var.aws_vpc_id}"
  peer_external_gateway           = google_compute_external_vpn_gateway.external_gateway.id
  peer_external_gateway_interface = 1
  shared_secret                   = aws_vpn_connection.vpn_connection.tunnel2_preshared_key

  region                = google_compute_ha_vpn_gateway.ha_gateway.region
  vpn_gateway           = google_compute_ha_vpn_gateway.ha_gateway.id
  vpn_gateway_interface = 0

  router      = google_compute_router.router.name
  ike_version = 2
}

# GCP Cloud Router

resource "google_compute_router" "router" {
  name    = "gcp-router-${var.aws_vpc_id}"
  network = var.gcp_network_id
  region  = google_compute_ha_vpn_gateway.ha_gateway.region
  bgp {
    asn = 64514
  }
}

resource "google_compute_router_interface" "tunnel1" {
  name       = "interface-1"
  router     = google_compute_router.router.name
  ip_range   = "${aws_vpn_connection.vpn_connection.tunnel1_cgw_inside_address}/30"
  vpn_tunnel = google_compute_vpn_tunnel.tunnel1.name
}

resource "google_compute_router_peer" "peer-1" {
  name                      = "peer-1-${var.aws_vpc_id}"
  router                    = google_compute_router.router.name
  peer_ip_address           = aws_vpn_connection.vpn_connection.tunnel1_vgw_inside_address
  peer_asn                  = aws_vpn_connection.vpn_connection.tunnel1_bgp_asn
  advertised_route_priority = 100
  interface                 = google_compute_router_interface.tunnel1.name
}

resource "google_compute_router_interface" "tunnel2" {
  name       = "interface-2"
  router     = google_compute_router.router.name
  ip_range   = "${aws_vpn_connection.vpn_connection.tunnel2_cgw_inside_address}/30"
  vpn_tunnel = google_compute_vpn_tunnel.tunnel2.name
}

resource "google_compute_router_peer" "peer-2" {
  name                      = "peer-2-${var.aws_vpc_id}"
  router                    = google_compute_router.router.name
  peer_ip_address           = aws_vpn_connection.vpn_connection.tunnel2_vgw_inside_address
  peer_asn                  = aws_vpn_connection.vpn_connection.tunnel2_bgp_asn
  advertised_route_priority = 100
  interface                 = google_compute_router_interface.tunnel2.name
}

# AWS Route table

resource "aws_vpn_gateway_route_propagation" "example" {
  vpn_gateway_id = aws_vpn_gateway.vpn_gateway.id
  route_table_id = var.aws_route_table_id
}
