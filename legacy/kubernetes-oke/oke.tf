resource "oci_containerengine_cluster" "oke_cluster" {
  compartment_id     = var.oci_compartment_ocid
  kubernetes_version = var.oke_k8s_version
  name               = var.oke_cluster_name
  vcn_id             = oci_core_vcn.oke_vcn.id

  endpoint_config {
    is_public_ip_enabled = true
    subnet_id            = oci_core_subnet.k8s_api_subnet.id
    nsg_ids = [
      oci_core_network_security_group.oke_k8s_api.id
    ]
  }

}

# Our core worker nodes

resource "oci_containerengine_node_pool" "node_pool_1" {
  cluster_id         = oci_containerengine_cluster.oke_cluster.id
  compartment_id     = var.oci_compartment_ocid
  kubernetes_version = oci_containerengine_cluster.oke_cluster.kubernetes_version
  name               = "${var.oke_cluster_name}-node-pool-1"
  node_shape         = "VM.Standard.E3.Flex"

  node_config_details {
    placement_configs {
      availability_domain = "MMiR:US-ASHBURN-AD-1"
      subnet_id           = oci_core_subnet.workers_subnet_1.id
    }
    placement_configs {
      availability_domain = "MMiR:US-ASHBURN-AD-2"
      subnet_id           = oci_core_subnet.workers_subnet_1.id
    }
    placement_configs {
      availability_domain = "MMiR:US-ASHBURN-AD-3"
      subnet_id           = oci_core_subnet.workers_subnet_1.id
    }

    size = 3

    nsg_ids = [oci_core_network_security_group.oke_k8s_workers.id]
  }

  node_source_details {
    image_id    = "ocid1.image.oc1.iad.aaaaaaaaffh3tppq63kph77k3plaaktuxiu43vnz2y5oefkec37kwh7oomea"
    source_type = "IMAGE"
  }

  node_shape_config {
    memory_in_gbs = 8
    ocpus         = 8
  }
}