oci_tenancy_ocid     = "ocid1.tenancy.oc1..aaaaaaaa24wrydmie6u35gxqyeqnt2g4swp2kge3jomukit34tjy6kokqknq"     #mindsoci
oci_compartment_ocid = "ocid1.compartment.oc1..aaaaaaaaeveizn2oeenmh264tdv7hpx63r45lmzas77alwhibf3ryof2ujkq" # minds-sandbox
oci_region           = "us-ashburn-1"

#

vcn_cidr_blocks = [
  "10.8.0.0/16"
]
vcn_dns_label = "sandboxoke"

vcn_workers_subnet_1_cidr = "10.7.0.0/24"
vcn_k8s_api_subnet_cidr   = "10.7.1.0/24"

#

oke_k8s_version  = "v1.21.5"
oke_cluster_name = "sandbox"