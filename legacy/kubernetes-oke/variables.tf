variable "oci_tenancy_ocid" {
  type = string
}

variable "oci_region" {
  default = "us-ashburn-1"
  type    = string
}

variable "oci_compartment_ocid" {
  type = string
}

# VCN

variable "vcn_cidr_blocks" {
  type = list(string)

}

variable "vcn_dns_label" {
  type = string
}

variable "vcn_workers_subnet_1_cidr" {
  type = string
}

variable "vcn_k8s_api_subnet_cidr" {
  type = string
}

# OKE

variable "oke_k8s_version" {
  type        = string
  description = "The version of Kubernetes to use"
}

variable "oke_cluster_name" {
  type        = string
  description = "The name of the cluster"
}