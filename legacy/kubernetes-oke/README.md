# Oracle Kubernetes Engine

## Workspaces

- production
- sandbox

## What this does

- Deploys a VPN
- Deploys an OKE cluster
- Handles K8S roles and iam permissions
- Sets up Security Group permissions

## Assumptions

- You have already configured you `oci-cli`

## Deploying

`terraform apply -var-file "$(terraform workspace show).tfvars"`