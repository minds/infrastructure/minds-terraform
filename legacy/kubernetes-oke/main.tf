terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "environments/oke/terraform.tfstate"
  }
  required_providers {
    oci = {
      version = "4.58.0"
    }
  }
}

provider "oci" {
  tenancy_ocid = var.oci_tenancy_ocid
  region       = var.oci_region
}

