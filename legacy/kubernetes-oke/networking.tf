# https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengnetworkconfig.htm#

# OKE will have its own VPN
resource "oci_core_vcn" "oke_vcn" {
  compartment_id = var.oci_compartment_ocid

  display_name = "${var.oke_cluster_name}-oke-vcn"

  cidr_blocks    = var.vcn_cidr_blocks
  dns_label      = var.vcn_dns_label
  is_ipv6enabled = true
}

# Our private subnet
resource "oci_core_subnet" "workers_subnet_1" {
  cidr_block     = var.vcn_workers_subnet_1_cidr
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id
  display_name   = "${var.oke_cluster_name}-workers-subnet-1"

  #
  prohibit_internet_ingress  = true
  prohibit_public_ip_on_vnic = true

  route_table_id = oci_core_route_table.oke_workers_route_table.id
}

# Our public subnet
resource "oci_core_subnet" "k8s_api_subnet" {
  cidr_block     = var.vcn_k8s_api_subnet_cidr
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name   = "${var.oke_cluster_name}-k8s-api-subnet"
  route_table_id = oci_core_route_table.oke_public_route_table.id
}

# We require an Internet Gateway so our cluster can talk to the world

resource "oci_core_internet_gateway" "oke_internet_gateway" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-internet-gateway"
}

# Our route table must point to our internet gateway

resource "oci_core_route_table" "oke_public_route_table" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-route-table"

  route_rules {
    network_entity_id = oci_core_internet_gateway.oke_internet_gateway.id
    destination       = "0.0.0.0/0"
  }
}

# We require a NAT Gateway for a workers nodes to talk to the internet

resource "oci_core_nat_gateway" "oke_nat_gateway" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-nat-gateway"
}

# Our route table must point to our nat gateway

resource "oci_core_route_table" "oke_workers_route_table" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-worker-route-table"

  route_rules {
    network_entity_id = oci_core_nat_gateway.oke_nat_gateway.id
    destination       = "0.0.0.0/0"
  }

  route_rules {
    network_entity_id = oci_core_service_gateway.oke_service_gateway.id
    destination       = "all-iad-services-in-oracle-services-network"
    destination_type  = "SERVICE_CIDR_BLOCK"
  }

  # Let our cluster talk to cassandra
  # TODO: this is specific to production
  route_rules {
    destination       = "10.0.8.0/24"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = "ocid1.drg.oc1.iad.aaaaaaaan3yxzldh3fpdsfbtszugudhhwfxckrividnzbel7cu7wggtcmx3q"
  }
}

# We also need a service gateway as our worker nodes will be in a private subnet

resource "oci_core_service_gateway" "oke_service_gateway" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-service-gateway"

  services {
    service_id = "ocid1.service.oc1.iad.aaaaaaaam4zfmy2rjue6fmglumm3czgisxzrnvrwqeodtztg7hwa272mlfna" # All services
  }
}

# Security rules for Kubernetes API Endpoint (public)
resource "oci_core_network_security_group" "oke_k8s_api" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-oke-k8s-api"
}
resource "oci_core_network_security_group_security_rule" "oke_k8s_api_client_access" {
  network_security_group_id = oci_core_network_security_group.oke_k8s_api.id
  direction                 = "INGRESS"
  protocol                  = "6"

  description = "Client access to Kubernetes API endpoint"

  stateless = false
  source    = "0.0.0.0/0"

  tcp_options {
    destination_port_range {
      max = 6443
      min = 6443
    }
  }
}

# Security tules for Kubernetes API Endpoint (worker nodes / private)
resource "oci_core_network_security_group" "oke_k8s_api_workers" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-oke-k8s-api-workers"
}

resource "oci_core_network_security_group_security_rule" "oke_k8s_api_workers_access" {
  network_security_group_id = oci_core_network_security_group.oke_k8s_api.id
  direction                 = "INGRESS"
  protocol                  = "6"

  description = "Kubernetes worker to Kubernetes API endpoint communication."

  stateless = false
  source    = oci_core_subnet.workers_subnet_1.cidr_block

  tcp_options {
    destination_port_range {
      max = 6443
      min = 6443
    }
  }
}

resource "oci_core_network_security_group_security_rule" "oke_k8s_api_workers_control_plane" {
  network_security_group_id = oci_core_network_security_group.oke_k8s_api.id
  direction                 = "INGRESS"
  protocol                  = "6"

  description = "Kubernetes worker to control plane communication."

  stateless = false
  source    = oci_core_subnet.workers_subnet_1.cidr_block

  tcp_options {
    destination_port_range {
      max = 12250
      min = 12250
    }
  }
}

resource "oci_core_network_security_group_security_rule" "oke_k8s_api_workers_path_discovery" {
  network_security_group_id = oci_core_network_security_group.oke_k8s_api.id
  direction                 = "INGRESS"
  protocol                  = "1"

  description = "Kubernetes worker to control plane communication."

  stateless = false
  source    = oci_core_subnet.workers_subnet_1.cidr_block

  icmp_options {
    type = 3
    code = 4
  }
}

# Security rules for Worker Nodes

resource "oci_core_network_security_group" "oke_k8s_workers" {
  compartment_id = var.oci_compartment_ocid
  vcn_id         = oci_core_vcn.oke_vcn.id

  display_name = "${var.oke_cluster_name}-oke-workers"
}

resource "oci_core_network_security_group_security_rule" "oke_k8s_workers_pods" {
  network_security_group_id = oci_core_network_security_group.oke_k8s_workers.id
  direction                 = "INGRESS"
  protocol                  = "all"

  description = "Allow pods on one worker node to communicate with pods on other worker nodes."

  stateless = false
  source    = oci_core_subnet.workers_subnet_1.cidr_block
}

resource "oci_core_network_security_group_security_rule" "oke_k8s_workers_control_plane" {
  network_security_group_id = oci_core_network_security_group.oke_k8s_workers.id
  direction                 = "INGRESS"
  protocol                  = "6" # TCP

  description = "Allow Kubernetes control plane to communicate with worker nodes."

  stateless = false
  source    = oci_core_subnet.k8s_api_subnet.cidr_block

  tcp_options {
    # All
  }
}

resource "oci_core_network_security_group_security_rule" "oke_k8s_workers_path_discovery" {
  network_security_group_id = oci_core_network_security_group.oke_k8s_workers.id
  direction                 = "INGRESS"
  protocol                  = "1"

  description = "Kubernetes worker to control plane communication."

  stateless = false
  source    = "0.0.0.0/0"

  icmp_options {
    type = 3
    code = 4
  }
}