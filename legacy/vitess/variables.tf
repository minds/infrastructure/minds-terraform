##

variable "eks_assume_role_name" {
  type        = string
  description = "Name of IAM role to assume before authenticating with the EKS cluster."

  default = "minds_eks_k8s_devops"
}

variable "eks_cluster_name" {
  type        = map(string)
  description = "The name of eks cluster to deploy to"

  default = {
    default = "minds-eks-jkqjL8xS"
    sandbox = "sandbox"
  }
}
