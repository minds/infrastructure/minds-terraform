###########
### IAM ###
###########

# Polices

// Read Only

data "aws_iam_policy_document" "vttablet_s3_read_only" {
  statement {
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*",
    ]
  }
}

resource "aws_iam_policy" "vttablet_s3_read_only" {
  name   = "vitess-vttablet-${local.environment}-s3-read-only"
  policy = data.aws_iam_policy_document.vttablet_s3_read_only.json
}

// Full access

data "aws_iam_policy_document" "vttablet_s3" {
  statement {
    actions = ["s3:*"]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}",
      "arn:aws:s3:::${aws_s3_bucket.this.bucket}/*",
    ]
  }
}

resource "aws_iam_policy" "vttablet_s3" {
  name   = "vitess-vttablet-${local.environment}-s3"
  policy = data.aws_iam_policy_document.vttablet_s3.json
}

# Users

// S3

resource "aws_iam_user" "vttablet_s3" {
  name = "vitess-vttablet-${local.environment}-backups"
}

resource "aws_iam_user_policy_attachment" "vttablet_s3" {
  user       = aws_iam_user.vttablet_s3.name
  policy_arn = aws_iam_policy.vttablet_s3.arn
}

// Argo

resource "aws_iam_user" "vttablet_s3_argo" {
  count = local.environment != "sandbox" ? 1 : 0
  name  = "vitess-vttablet-${local.environment}-backups-argo"
}

resource "aws_iam_user_policy_attachment" "vttablet_s3_argo" {
  count      = local.environment != "sandbox" ? 1 : 0
  user       = aws_iam_user.vttablet_s3_argo[0].name
  policy_arn = aws_iam_policy.vttablet_s3_read_only.arn
}
