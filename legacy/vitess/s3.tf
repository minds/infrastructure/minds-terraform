##########
### S3 ###
##########

resource "aws_s3_bucket" "this" { bucket = "vitess-tablet-backups-${local.environment}" }

resource "aws_s3_bucket_acl" "this" {
  bucket = aws_s3_bucket.this.bucket
  acl    = "private"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  bucket = aws_s3_bucket.this.bucket
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  bucket = aws_s3_bucket.this.bucket

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  bucket = aws_s3_bucket.this.bucket

  rule {
    id     = "main"
    status = "Enabled"

    // Match all objects
    filter {}

    // Expire after 30 days
    expiration { days = 30 }

    // Transition to Glacier IR after 3 days
    transition {
      days          = 3
      storage_class = "GLACIER_IR"
    }
  }
}
