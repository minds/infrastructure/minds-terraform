# Minds Vitess Cluster

!! Note: this module assumes the vitess operator is already installed. You can install the operator by running `kubectl apply -f vitess-operator.yaml`


## Applying

This module consists of two workspaces: `sandbox` and `default` for deploying to Sandbox and Production respectively.

Variables that differ across environments use a map with the workspace as a key and a default value. For example:

```hcl
variable "environment" {
  type        = map(string)
  default = {
    default = "production"
    sandbox = "sandbox"
  }
}

output "environment" {
  value = var.environment[terraform.workspace]
}
```

## Setting up schemas

`kubectl -n vitess port-forward --address localhost "$(kubectl -n vitess get service --selector="planetscale.com/component=vtctld" -o name | head -n1)" 15999:15999`

```
alias vtctlclient="vtctlclient --server localhost:15999 --alsologtostderr"
vtctlclient ApplySchema --sql="$(cat schemas/minds.sql)" minds
vtctlclient ApplyVSchema --vschema="$(cat schemas/minds-vschema.json)" minds
```

## Accessing mysql cli

`kubectl -n vitess port-forward --address localhost "$(kubectl -n vitess get service --selector="planetscale.com/component=vtgate,!planetscale.com/cell" -o name | head -n1)" 3306:3306`

## Post Install Steps

When creating an IAM user for the first time, you need to create a K8s secret in the namespace of Vitess with the credentials for this user in the format of the `~/.aws/credentials` file:

```
kubectl create secret generic aws-auth --from-file=credentials -n vitess
```
