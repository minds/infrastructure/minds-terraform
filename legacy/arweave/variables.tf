variable "eks_cluster_name" {
  type = string
}

variable "arweave_xpriv" {
  description = "The secret key for the Arweave Wallet"
  type        = string
}
