# Minds - Arweave Node

!! This is an experimental feature !!

This installs a nodejs server on K8S, that communicates with Arweave.

## Setting up

`export ARWEAVE_XPRIV=`

`terraform apply --var arweave_xpriv=$ARWEAVE_XPRIV`

## Tearing down

`terraform destroy`
