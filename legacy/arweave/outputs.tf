output "service_uri" {
  value = data.kubernetes_service.arweave.load_balancer_ingress.0.hostname
}
