# OCI

variable "oci_tenancy_ocid" {
  type = string
}

variable "oci_region" {
  default = "us-ashburn-1"
  type    = string
}

variable "oci_compartment_ocid" {
  type = string
}

variable "oci_vcn_ocid" {
  type = string
}

# AWS

variable "aws_vpc_id" {
  type = string
}

variable "aws_route_table_id" {
  type = string
}

variable "vpn_inside_cidr_tunnel1" {
  type = string
}
variable "vpn_inside_cidr_tunnel2" {
  type = string
}