provider "aws" {
  region = "us-east-1"
}

provider "oci" {
  tenancy_ocid = var.oci_tenancy_ocid
  region       = var.oci_region
}

terraform {
  backend "s3" {
    bucket = "minds-terraform"
    key    = "aws-oci-bridge"
  }
}

# AWS temporary customer gateway
# "OCI requires the public IP of the remote VPN peer before creating a IPSec connection. 
# After this process has been completed, a new customer gateway is configured representing 
# the actual OCI VPN endpoint public IP."

resource "aws_customer_gateway" "tmp_customer_gateway" {
  bgp_asn    = "31898"   # This ASN is used in all OCI regions
  ip_address = "1.1.1.1" # Any valid ip address

  # We want it to be the below, but we get circle issue....
  #ip_address = oci_core_ipsec_connection_tunnel_management.tunnel_1.vpn_ip
  type = "ipsec.1"

  tags = {
    Name = "tmp-aws-oci-customer-gateway"
  }
}

# Create AWS Virtual Private Gateway (import if you already have one, can only have 1 VPG per VPC)
resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = var.aws_vpc_id

  tags = {
    Name = "${var.aws_vpc_id}-gateway"
  }
}

# Setup our route table

resource "aws_vpn_gateway_route_propagation" "vpg_to_vpc" {
  vpn_gateway_id = aws_vpn_gateway.vpn_gateway.id
  route_table_id = var.aws_route_table_id
}

# Create the VPN connection

resource "aws_vpn_connection" "vpn_connection" {
  vpn_gateway_id      = aws_vpn_gateway.vpn_gateway.id
  customer_gateway_id = aws_customer_gateway.tmp_customer_gateway.id
  type                = "ipsec.1"
  static_routes_only  = false

  tunnel1_inside_cidr = var.vpn_inside_cidr_tunnel1
  tunnel2_inside_cidr = var.vpn_inside_cidr_tunnel2

  tags = {
    Name = "${terraform.workspace}-${var.aws_vpc_id}-oci-vpn"
  }

  lifecycle {
    ignore_changes = [
      customer_gateway_id # must be changed to aws_customer_gateway.customer_gateway.id
    ]
  }
}


# Create OCI CPE Object

resource "oci_core_cpe" "oci_cpe" {
  compartment_id = var.oci_compartment_ocid
  ip_address     = aws_vpn_connection.vpn_connection.tunnel1_address # this could be wrong
  display_name   = "to_aws"
}

resource "oci_core_drg" "oci_drg" {
  compartment_id = var.oci_compartment_ocid
}

# Create OCI IPSec connection

resource "oci_core_ipsec" "oci_ip_sec_connection" {
  compartment_id = var.oci_compartment_ocid
  cpe_id         = oci_core_cpe.oci_cpe.id
  drg_id         = oci_core_drg.oci_drg.id
  static_routes  = ["0.0.0.0/0"]
  display_name   = "oci-to-aws-vpn"
}

# Setup BGP for tunnel

resource "oci_core_ipsec_connection_tunnel_management" "tunnel_1" {
  ipsec_id  = oci_core_ipsec.oci_ip_sec_connection.id
  tunnel_id = data.oci_core_ipsec_connection_tunnels.oci_tunnels.ip_sec_connection_tunnels[0].id
  routing   = "BGP"
  bgp_session_info {
    customer_bgp_asn      = aws_vpn_connection.vpn_connection.tunnel1_bgp_asn
    customer_interface_ip = "${aws_vpn_connection.vpn_connection.tunnel1_vgw_inside_address}/30"
    oracle_interface_ip   = "${aws_vpn_connection.vpn_connection.tunnel1_cgw_inside_address}/30"
  }
  shared_secret = aws_vpn_connection.vpn_connection.tunnel1_preshared_key
  display_name  = "tunnel-1"
}

resource "oci_core_ipsec_connection_tunnel_management" "tunnel_2" {
  ipsec_id  = oci_core_ipsec.oci_ip_sec_connection.id
  tunnel_id = data.oci_core_ipsec_connection_tunnels.oci_tunnels.ip_sec_connection_tunnels[1].id
  routing   = "BGP"
  bgp_session_info {
    customer_bgp_asn      = aws_vpn_connection.vpn_connection.tunnel2_bgp_asn
    customer_interface_ip = "${aws_vpn_connection.vpn_connection.tunnel2_vgw_inside_address}/30"
    oracle_interface_ip   = "${aws_vpn_connection.vpn_connection.tunnel2_cgw_inside_address}/30"
  }
  shared_secret = aws_vpn_connection.vpn_connection.tunnel2_preshared_key
  display_name  = "tunnel-2"
}


# Gather OCI tunnel information

data "oci_core_ipsec_connection_tunnels" "oci_tunnels" {
  ipsec_id = oci_core_ipsec.oci_ip_sec_connection.id
}

# Create new AWS customer gateway

resource "aws_customer_gateway" "customer_gateway" {
  bgp_asn    = "31898"
  ip_address = oci_core_ipsec_connection_tunnel_management.tunnel_1.vpn_ip
  type       = "ipsec.1"

  tags = {
    Name = "aws-oci-customer-gateway"
  }
}

output "aws_customer_gateway" {
  value = aws_customer_gateway.customer_gateway.id
}

# resource "aws_vpn_gateway" "vpn_gateway" {
#   vpc_id = var.aws_vpc_id

#   tags = {
#     Name = "aws-oci-vpn-gateway"
#   }
# }
