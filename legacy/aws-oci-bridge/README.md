# AWS <-> OCI Bridge

This module creates a bridge between an AWS VPC and a OCI VCN.

# Workspaces

- production
- sandbox

## Assumptions

- You have functionig VPC's on both AWS and OCI
- Your subnets have a route table configured on both AWS and OCI
- You are not using more than one route table
- 2 tunnels are appropriate for your setup
- You already have the `oci-cli` setup

## How to use

`terraform workspace new production`
`terraform apply -var-file "$(terraform workspace show).tfvars"`

Once the above is deployed you must change the AWS Virtual Customer Gateway to be `terraform output aws_customer_gateway`

## Troubleshooting

### What is my aws_route_table_id?

This is the route table that your subnets use, and that will propogate ovet the VPN.

### My VPN connection is up but I can not ssh/ping/http any servers

Firewall configurations are out of the scope of this module. Ensure that your Firewall/Security groups allow ingress/egress to the relevant sources/targets.