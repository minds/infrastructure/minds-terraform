# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "3.72.0"
  hashes = [
    "h1:6pleQtx6+jQE/Kekcr8Ou05yYrdvVSngnwHE0PkBELg=",
    "zh:0c4615ff3c6bc9700d8f16a5a644ddfcb666eaddbf2f77d71616008a28e4db75",
    "zh:29eb139a8fbb98391652fa1eb4668ad5a13a31d45a6c06fe2b1d66903c4e6509",
    "zh:3e73a9cf67d30c400456011cc8ed036bce68df8fd8131d591a929186e43ab80b",
    "zh:46090da59293464e1865190b2e67ae63103c9d87a16a5fcb982ce748369666d6",
    "zh:4fb25d9b139cb1856e519bff4fd49695285fa63a1d57e1c0efc1791bb36532a8",
    "zh:5acd99d2b22cd45f18c93905a6e5122712c48f432db3c3c3518af449c10ae7e6",
    "zh:95e53770503127e6de9f71d02e0bafdf0c7e7490f93401e05b6015bc7fa94b29",
    "zh:b31524932e804de5ef5613d3646892eb55656f062bcbb9d7c29cf6539f82397e",
    "zh:d977b9f8657c3026340295015930ef58caba5c2f59fd2e63e230c0b9ddba1ee7",
    "zh:fcb0202ad1b8de19f1cd58d0b60147cae5dd4f869a861f619e8e5d27f8a936a9",
    "zh:fe85cf3c44834230c2aaa2d0c622ddde1e33398bbe9f7213011eba68130b1588",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version = "4.6.0"
  hashes = [
    "h1:uGcZfOySgiqtYDw0eQXgmdzMDZrsuRXhmzNJYxJbgcc=",
    "zh:005a28a2c79f6b29680b0f57260c69c85d8a992688007b6e5645149bd379951f",
    "zh:2604d825de72cf99b4899d7880837adeb19d371f48e419666e32c4c3cf6a72e9",
    "zh:290da4eb18e44469480cf299bebce89f54e4d301f856cdffe2837b498878c7ec",
    "zh:3e5ba1a55d38fa17533a18fc14a612e781ded76c6309734d3dc0a937be27eec1",
    "zh:4a85de3cdb33c092d8ccfced3d7302934de0dd4f72bbcebd79d45afe0a0b6f85",
    "zh:5fb1a79800833ae922aaba594a8b2bc83be1d254052e12e0ce8330ca0d8933d9",
    "zh:679b9f50c6fe0476e74d37935f7598d46d6e9612f75b26a8ef1ca3c13144d06a",
    "zh:893216e32378839668c51ef135af1676cd887d63e2edb6625cf9adad7bfa346f",
    "zh:ad8f2fd19adbe4c10281ba9b3c8d5100877a9c541d3580bbbe9357714aa77619",
    "zh:bff5d6fd15e98c12ee9ed98b0338761dc4a9ba671a37834926daeabf73c71783",
    "zh:debdf15fbed8d63e397cd004bf65586bd2b93ce04e47ca51a7c70c1fe9168b87",
  ]
}

provider "registry.terraform.io/hashicorp/oci" {
  version = "4.59.0"
  hashes = [
    "h1:J0KYHJzS/brZnWDHlYPBwF42NgkiJFM1asAdYeH0HWw=",
    "zh:0a9e2e80d86e9a1d6f705e78f55479aa8b041e23557ebf84cb93e7ad0b341fb8",
    "zh:0b54f0ff3662ce20caaca5e7268aa3dbb5b969b64469b05218b9cfd16ae10ef4",
    "zh:2b755325a397a6b359d86fb5b0fa3a897813d8da33d889dbacafda5366dae831",
    "zh:51710cc3a39300fb5f227be545a2c0951a019c50a0ca7ffd20ebc60702c80396",
    "zh:551f689adab86139abfce6c450efe918e1fd6a47d29c7c46ff54e64e19c48f57",
    "zh:58b59933606c7c0737fe3fc27ad6f25ae8b8c67d11719b6751ca3bddf5d75e77",
    "zh:759cbb4f602ae05b94cb49aaba9faaed88da21260a6e128f52ab17bb1f6930dc",
    "zh:824755c4864d472bc2cf9973c69bebbe575c3bdaeb91c51b5f2a70fb3e5f5e30",
    "zh:cd2045b7a5ab7a1143b51881e5fa3777ff2b73603e073a4851351413b02f7881",
    "zh:ee1f11dfe06a0b26fbbdd13afe04058793c898fde7e43afdb03a4358fd6362cc",
  ]
}
