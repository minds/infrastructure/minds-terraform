resource "kubernetes_namespace" "logging" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "logging"
    }

    labels = {
      name = "logging"
    }

    name = "logging"
  }
}

resource "helm_release" "fluentd" {
  provider = helm.eks

  name      = "fluentd"
  namespace = kubernetes_namespace.logging.metadata[0].name

  repository = "https://fluent.github.io/helm-charts"
  chart      = "fluentd"

  set {
    name  = "fileConfigs.01_sources\\.conf"
    value = <<EOT
    <source>
      @type tail
      @id in_tail_container_logs
      @label @KUBERNETES
      path /var/log/containers/*.log
      pos_file /var/log/fluentd-containers.log.pos
      tag kubernetes.*
      read_from_head true
      <parse>
        @type multi_format
        <pattern>
          format json
          time_key time
          time_type string
          time_format "%Y-%m-%dT%H:%M:%S.%NZ"
          keep_time_key true
        </pattern>
        <pattern>
          format regexp
          expression /^(?<time>.+) (?<stream>stdout|stderr)( (.))? (?<log>.*)$/
          time_format '%Y-%m-%dT%H:%M:%S.%NZ'
          keep_time_key true
        </pattern>
      </parse>
      emit_unmatched_lines true
    </source>
EOT
  }

  set {
    name  = "fileConfigs.04_outputs\\.conf"
    value = <<EOT
    <label @OUTPUT>
      <match **>
        @type elasticsearch
        host "elasticsearch-opendistro-es-client-service.elasticsearch.svc.cluster.local"
        scheme https
        port 9200
        path ""
        user logstash
        password "${var.logstash_password}"
        ssl_verify false
        time_key time
        logstash_format true
      </match>
    </label>
  EOT
  }

  values = [yamlencode({
    tolerations = [
      {
        key      = "stateful"
        operator = "Equal"
        value    = "true"
        effect   = "NoSchedule"
      }
    ]
  })]

}
