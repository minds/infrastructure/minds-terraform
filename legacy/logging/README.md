# Logging

## Workspaces

- Default is production (terraform apply --var-file=production.tfvars)
- sandbox is sandbox (terraform apply --var-file=sandbox.tfvars)

## Deploying

`terraform apply --var-file=production.tfvars`
