##

variable "eks_cluster_name" {
  type        = string
  description = "The name of eks cluster to deploy to"
}

variable "eks_assume_role_name" {
  type        = string
  description = "Name of IAM role to assume before authenticating with the EKS cluster."
  default     = "minds_eks_k8s_admins"
}

variable "logstash_password" {
  type        = string
  description = "The logstash user password"
}
