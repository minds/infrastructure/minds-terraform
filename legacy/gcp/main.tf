provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

terraform {
  backend "s3" {
    bucket = "minds-terraform"
    key    = "gcp"
  }
}

# VPC
resource "google_compute_network" "vpc" {
  name                    = "${var.gcp_project_id}-vpc"
  auto_create_subnetworks = "false"
}

output "network_id" {
  value = google_compute_network.vpc.id
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.gcp_project_id}-subnet"
  region        = var.gcp_region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24"
}
