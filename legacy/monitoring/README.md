# Monitoring

- Grafana is run from GKE
- EKS runs only Prometheus

## Workspaces

- Default is production (terraform workspace select default && terraform plan --var-file=production.tfvars)
- sandbox is sandbox (terraform workspace select sandbox && terraform plan --var-file=sandbox.tfvars)

### Role Assumption

By default, both workspaces are configured to assume the `minds_eks_k8s_developers` IAM role for authenticating with EKS. This is to enable developers to run `terraform plan`, although this role does not allow creating, updating, or deleting resources in production. To deploy changes to production, you must assume the `minds_eks_k8s_admins` role:

```bash
terraform workspace select default
terraform apply -var-file production.tfvars -var eks_assume_role_name="minds_eks_k8s_admins"
```

## Accessing the dashboards

Go to https://dashboards.minds.com/