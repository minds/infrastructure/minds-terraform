/*
  Kubernetes Infrastructure:
  - Grafana
    - https://grafana.com/

  Resources:
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace
  - https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release
  - https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address
  - https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_managed_ssl_certificate

  Data Sources:
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/storage_class
*/


resource "kubernetes_namespace" "monitoring" {
  count = terraform.workspace != "sandbox" ? 1 : 0

  // TODO fix
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "monitoring"
    }

    labels = {
      name = "monitoring"
    }

    name = "monitoring"
  }
}

resource "helm_release" "gke-prometheus-stack" {
  count = terraform.workspace != "sandbox" ? 1 : 0

  provider  = helm.gke
  name      = "prometheus"
  namespace = kubernetes_namespace.monitoring[0].metadata[0].name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  version    = "34.10.0"

  set {
    name  = "grafana.service.type"
    value = "LoadBalancer"
  }
  set {
    name  = "grafana.ingress.enabled"
    value = true
  }
  set {
    name  = "grafana.ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "gce"
    type  = "string"
  }
  set {
    name  = "grafana.ingress.annotations.kubernetes\\.io/ingress\\.global-static-ip-name"
    value = google_compute_global_address.grafana_ingress_address[0].name
    type  = "string"
  }
  set {
    name  = "grafana.ingress.annotations.ingress\\.gcp\\.kubernetes\\.io/pre-shared-cert"
    value = google_compute_managed_ssl_certificate.grafana_ingress[0].name
  }
  set {
    name  = "grafana.ingress.path"
    value = "/*"
  }
  set {
    name  = "grafana.ingress.hosts.0"
    value = "dashboards.minds.com"
  }
  set {
    name  = "grafana.persistence.enabled"
    value = true
  }
  set {
    name  = "grafana.grafana\\.ini.auth\\.anonymous.enabled"
    value = true
  }
  set {
    name  = "grafana.grafana\\.ini.auth\\.anonymous.org_name"
    value = "Main Org."
  }
  set {
    name  = "grafana.grafana\\.ini.auth\\.anonymous.org_role"
    value = "Viewer"
  }
  set {
    name  = "grafana.plugins[0]"
    value = "grafana-piechart-panel"
  }
  set {
    name  = "grafana.plugins[1]"
    value = "grafana-polystat-panel"
  }
  set {
    name  = "grafana.grafana\\.ini.server.root_url"
    value = "https://dashboards.minds.com"
  }

  ##
  # OAuth
  ##
  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.enabled"
    value = true
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.scopes"
    value = "openid email profile"
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.name"
    value = "Oauth"
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.name"
    value = "Oauth"
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.allow_sign_up"
    value = true
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.client_id"
    value = "grafana"
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.client_secret"
    value = var.oauth_secret
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.auth_url"
    value = "https://keycloak.minds.com/auth/realms/minds-inc/protocol/openid-connect/auth"
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.token_url"
    value = "https://keycloak.minds.com/auth/realms/minds-inc/protocol/openid-connect/token"
  }

  set {
    name  = "grafana.grafana\\.ini.auth\\.generic_oauth.api_url"
    value = "https://keycloak.minds.com/auth/realms/minds-inc/protocol/openid-connect/userinfo"
  }

  ##
  # Prometheus
  ##
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.storageClassName"
    value = data.kubernetes_storage_class.gke-ssd[0].metadata[0].name
  }
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.accessModes[0]"
    value = "ReadWriteOnce"
  }
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.resources.requests.storage"
    value = "50Gi"
  }
}

resource "google_compute_global_address" "grafana_ingress_address" {
  count = terraform.workspace != "sandbox" ? 1 : 0

  name = "grafana-static-ingress"
}

resource "google_compute_managed_ssl_certificate" "grafana_ingress" {
  count = terraform.workspace != "sandbox" ? 1 : 0

  provider = google-beta

  name = "graphana-ingress-ssl"

  managed {
    domains = ["dashboards.minds.com"]
  }
}

data "kubernetes_storage_class" "gke-ssd" {
  count = terraform.workspace != "sandbox" ? 1 : 0

  provider = kubernetes.gke
  metadata {
    name = "storage-ssd"
  }
}

output "grafana_ingress_ip_address" {
  value = (
    terraform.workspace != "sandbox"
    ? google_compute_global_address.grafana_ingress_address[0].address
    : ""
  )
}