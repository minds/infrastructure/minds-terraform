/*
  Kubernetes Infrastructure:
  - Prometheus Operator
    - https://github.com/prometheus-operator/prometheus-operator
  - Kubernetes Metrics Server
    - https://github.com/kubernetes-sigs/metrics-server
  - Prometheus Adapter
    - https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus-adapter

  Resources:
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace
  - https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release
*/

resource "kubernetes_namespace" "eks_monitoring" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "monitoring"
    }

    labels = {
      name = "monitoring"
    }

    name = "monitoring"
  }
}

resource "helm_release" "eks-prometheus-stack" {
  provider  = helm.eks
  name      = "prometheus"
  namespace = kubernetes_namespace.eks_monitoring.metadata[0].name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"

  // TODO resolve the wait condition
  // This seems to be a known issue with this Helm chart with newer versions of Helm.
  // While the validating webhook fails, the pods are deployed successfully and Prometheus functions.
  // https://github.com/prometheus-community/helm-charts/issues/1041
  wait = false

  # We use grafana on the GKE environment, so skip for here
  set {
    name  = "grafana.enabled"
    value = false
  }

  ##
  # Prometheus
  ##
  # set {
  #   name = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.storageClassName"
  #   value = kubernetes_storage_class.ssd.metadata[0].name
  # }
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.accessModes[0]"
    value = "ReadWriteOnce"
  }
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.resources.requests.storage"
    value = "100Gi"
  }
  set {
    name  = "prometheus.service.type"
    value = "LoadBalancer"
  }
  set {
    name  = "prometheus.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-internal"
    value = "0.0.0.0/0"
    type  = "string"
  }
  set {
    name  = "prometheus.prometheusSpec.podMonitorSelectorNilUsesHelmValues"
    value = "false"
  }
  set {
    name  = "prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues"
    value = "false"
  }

  values = [yamlencode({
    alertmanager = { enabled = false },
    prometheusOperator = {
      resources = {
        requests = { cpu = "100m", memory = "256Mi" },
        limits   = { cpu = "200m", memory = "512Mi" }
      }
    }
    prometheus = {
      prometheusSpec = {
        nodeSelector = {
          monitoring = "true"
        },
        tolerations = [{
          key      = "monitoring",
          operator = "Equal",
          value    = "true",
          effect   = "NoSchedule"
        }],
        resources = {
          requests = { cpu = "3", memory = "24Gi" },
          limits   = { cpu = "4", memory = "26Gi" }
        }
      }
    },
    kube-state-metrics = {
      resources = {
        requests = { cpu = "100m", memory = "128Mi" },
        limits   = { cpu = "200m", memory = "256Mi" }
      }
    },
    prometheus-node-exporter = {
      resources = {
        requests = { cpu = "25m", memory = "30Mi" },
        limits   = { cpu = "50m", memory = "50Mi" }
      }
    }
  })]
}

#################################
### Kubernetes Metrics Server ###
#################################

resource "kubernetes_namespace" "metrics_server" {
  provider = kubernetes.eks
  metadata {
    name = "metrics-server"
  }
}

resource "helm_release" "metrics_server" {
  provider  = helm.eks
  name      = "metrics-server"
  namespace = "metrics-server"

  repository = "https://kubernetes-sigs.github.io/metrics-server"
  chart      = "metrics-server"

  depends_on = [kubernetes_namespace.metrics_server]
}

##########################
### Prometheus Adapter ###
##########################

locals {
  /*
    See these docs for info on defining custom rules for Prometheus Adapter:
    https://github.com/kubernetes-sigs/prometheus-adapter/blob/master/docs/config.md
  */
  prometheus_adapter_rules = [
    // Custom query tracking total process utilization for Minds engine.
    {
      seriesQuery = "phpfpm_total_processes",
      metricsQuery = (
        "(sum(phpfpm_active_processes{<<.LabelMatchers>>}) by (<<.GroupBy>>) / sum(phpfpm_total_processes{<<.LabelMatchers>>}) by (<<.GroupBy>>)) * 100"
      ),
      name = { matches = "phpfpm_total_processes", as = "engine_process_utilization" },
      resources = {
        template = "<<.Resource>>"
        overrides = {
          kubernetes_namespace = { resource = "namespace" },
          app_minds_io_name    = { group = "apps", resource = "deployment" }
        }
      }
    }
  ]
}

resource "helm_release" "prometheus_adapter" {
  provider  = helm.eks
  name      = "prometheus-adapter"
  namespace = "monitoring"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-adapter"

  set {
    name  = "prometheus.url"
    value = "http://prometheus-operated.monitoring.svc"
  }

  // Workaround due to issues when using a set block with a list
  values = [
    yamlencode({
      rules = { custom = local.prometheus_adapter_rules },
      resources = {
        requests = { cpu = "500m", memory = "768Mi" },
        limits   = { cpu = "750m", memory = "896Mi" }
      }
    })
  ]
}
