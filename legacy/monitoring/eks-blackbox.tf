resource "helm_release" "prometheus_blackbox_exporter" {
  provider   = helm.eks
  repository = "https://prometheus-community.github.io/helm-charts"
  namespace  = kubernetes_namespace.eks_monitoring.metadata[0].name
  name       = "prometheus-blackbox-exporter"
  chart      = "prometheus-blackbox-exporter"
  version    = "5.8.1"

  values = [yamlencode(
    {
      serviceMonitor = {
        enabled = true
        targets = [
          {
            name = "minds-frontend"
            url  = "https://www.minds.com"
          },
          {
            name = "minds-engine"
            url  = "https://www.minds.com/api/v1/minds/config"
          }
        ]
      }
    }
  )]
}
