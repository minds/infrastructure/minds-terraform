/*
  Kubernetes Infrastructure:
  - Jaeger Operator
    - https://github.com/jaegertracing/jaeger-operator

  Resources:
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace
  - https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release
*/

// Jaeger Operator

resource "kubernetes_namespace" "eks_tracing" {
  provider = kubernetes.eks
  metadata {
    name        = "tracing"
    annotations = { name = "tracing" }
    labels      = { name = "tracing" }
  }
}

resource "helm_release" "eks_jaeger_operator" {
  provider   = helm.eks
  repository = "https://jaegertracing.github.io/helm-charts"
  name       = "jaeger-operator"
  chart      = "jaeger-operator"
  namespace  = kubernetes_namespace.eks_tracing.metadata[0].name
  version    = "2.28.0"

  values = [yamlencode({
    resources = {
      requests = { cpu = "100m", memory = "128Mi" },
      limits   = { cpu = "100m", memory = "128Mi" }
    },
    jaeger = {
      create    = true,
      namespace = kubernetes_namespace.eks_tracing.metadata[0].name,
      spec = {
        ingress = { enabled = false }
        resources = {
          requests = { cpu = "300m", memory = "2048Mi", ephemeral-storage = "10Gi" },
          limits   = { cpu = "600m", memory = "4096Mi", ephemeral-storage = "25Gi" }
        }
        storage = {
          type = "badger"
          options = {
            badger = {
              ephemeral       = false
              directory-key   = "/badger/key"
              directory-value = "/badger/data"
            }
          }
        }
      }
    }
  })]
}
