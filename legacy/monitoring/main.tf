terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "environments/monitoring/terraform.tfstate"
  }
}

# AWS providers

provider "aws" {
  region = "us-east-1"
}

provider "aws" {
  alias  = "eks"
  region = "us-east-1"

  assume_role {
    role_arn     = "arn:aws:iam::324044571751:role/${var.eks_assume_role_name}"
    session_name = "monitoring-terraform"
  }
}

data "aws_eks_cluster" "cluster" {
  name = var.eks_cluster_name[terraform.workspace]
}

data "aws_eks_cluster_auth" "cluster" {
  provider = aws.eks
  name     = var.eks_cluster_name[terraform.workspace]
}

data "aws_caller_identity" "current" {}


# EKS Kubernetes

provider "kubernetes" {
  alias                  = "eks"
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

provider "helm" {
  alias = "eks"
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    token                  = data.aws_eks_cluster_auth.cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  }
}

# GKE Kubernetes

module "k8s_gke" {
  source = "../../modules/k8s-gke"

  gcp_project_id       = var.gcp_project_id
  gcp_region           = var.gcp_region
  gke_cluster_name     = var.gke_cluster_name
  gke_cluster_location = var.gcp_region
}

provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

provider "kubernetes" {
  alias                  = "gke"
  host                   = module.k8s_gke.k8s_host
  token                  = module.k8s_gke.k8s_token
  cluster_ca_certificate = module.k8s_gke.k8s_cert
}

provider "helm" {
  alias = "gke"
  kubernetes {
    host                   = module.k8s_gke.k8s_host
    token                  = module.k8s_gke.k8s_token
    cluster_ca_certificate = module.k8s_gke.k8s_cert
  }
}
