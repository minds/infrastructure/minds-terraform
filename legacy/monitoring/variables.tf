##

variable "eks_assume_role_name" {
  type        = string
  description = "Name of IAM role to assume before authenticating with the EKS cluster."

  default = "minds_eks_k8s_admins"
}

variable "eks_cluster_name" {
  type        = map(string)
  description = "The name of eks cluster to deploy to"

  default = {
    sandbox = "sandbox"
    default = "minds-eks-jkqjL8xS"
  }
}


##

variable "gcp_project_id" {
  type        = string
  description = "The name of gcp project"

  default = "minds-production"
}

variable "gcp_region" {
  type        = string
  description = "The region of the gcp project"

  default = "us-east1"
}

variable "gke_cluster_name" {
  type        = string
  description = "The name of the GKE cluster"

  default = "minds-production-gke"
}

##

variable "oauth_secret" {
  type      = string
  sensitive = true
}