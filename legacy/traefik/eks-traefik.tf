resource "kubernetes_namespace" "traefik" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "traefik"
    }

    labels = {
      name            = "traefik"
      istio-injection = terraform.workspace == "sandbox" ? "disabled" : "enabled"
    }

    name = "traefik"
  }
}

resource "helm_release" "traefik" {
  provider = helm.eks

  name      = "traefik"
  namespace = kubernetes_namespace.traefik.metadata[0].name

  repository = "https://helm.traefik.io/traefik"
  chart      = "traefik"
  version    = "10.33.0"

  timeout = 1800

  set {
    name  = "service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-cert"
    value = aws_acm_certificate.this.arn
  }

  set {
    name  = "service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-ssl-ports"
    value = "443"
    type  = "string"
  }

  set {
    name  = "deployment.kind"
    value = "DaemonSet" # ensures each node is running a traefik pod
  }

  ## Set ingress class
  set {
    name  = "ingressClass.enabled"
    value = "true"
    type  = "string"
  }
  set {
    name  = "ingressClass.isDefaultClass"
    value = "true"
    type  = "string"
  }

  ## Force https

  set {
    name  = "ports.web.redirectTo"
    value = "websecure"
  }

  ## Use service names

  set {
    name  = "additionalArguments[0]"
    value = "--metrics.prometheus.addServicesLabels=true"
  }

  ## Istio

  set {
    name  = "deployment.podAnnotations.traffic\\.sidecar\\.istio\\.io/excludeInboundPorts"
    value = "8000\\,8443"
    type  = "string"
  }

  ## Jaeger

  set {
    name  = "additionalArguments[1]"
    value = "--tracing.jaeger=true"
  }

  set {
    name  = "additionalArguments[2]"
    value = "--tracing.jaeger.samplingType=probabilistic"
  }

  set {
    name  = "additionalArguments[3]"
    value = "--tracing.jaeger.samplingParam=0.1"
  }

  set {
    name  = "additionalArguments[4]"
    value = "--tracing.jaeger.samplingServerURL=http://jaeger-operator-jaeger-agent.tracing.svc:5778/sampling"
  }

  set {
    name  = "additionalArguments[5]"
    value = "--tracing.jaeger.localAgentHostPort=jaeger-operator-jaeger-agent.tracing.svc:6831"
  }

  set {
    name  = "additionalArguments[6]"
    value = "--accesslog=true"
  }

  ## Trusted IPs (Production only)

  dynamic "set" {
    for_each = terraform.workspace == "sandbox" ? [] : [1]
    content {
      name  = "additionalArguments[7]"
      value = "--entryPoints.web.forwardedHeaders.trustedIPs=${join("\\,", local.trustedCidrs)}"
      type  = "string"
    }
  }

  dynamic "set" {
    for_each = terraform.workspace == "sandbox" ? [] : [1]
    content {
      name  = "additionalArguments[8]"
      value = "--entryPoints.websecure.forwardedHeaders.trustedIPs=${join("\\,", local.trustedCidrs)}"
      type  = "string"
    }
  }
}

##
# Deprecated. See ./aws-acm.tf and queue this for removal
##
# resource "aws_acm_certificate" "ssl_cert" {
#   domain_name       = "*.${var.top_level_domain}"
#   validation_method = var.certificate_validation_method

#   subject_alternative_names = [
#     var.top_level_domain
#   ]

#   tags = {}

#   options {
#     certificate_transparency_logging_preference = "ENABLED"
#   }

#   lifecycle {
#     create_before_destroy = true
#   }
# }

locals {
  trustedCidrs = concat(
    data.cloudflare_ip_ranges.cloudflare.ipv4_cidr_blocks,
    [data.aws_vpc.eks_vpc.cidr_block]
  )
}

data "aws_eks_cluster" "eks_cluster" {
  name = var.eks_cluster_name
}
data "aws_vpc" "eks_vpc" {
  id = data.aws_eks_cluster.eks_cluster.vpc_config.0.vpc_id
}
data "cloudflare_ip_ranges" "cloudflare" {}
