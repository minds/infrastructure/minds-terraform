##

variable "eks_cluster_name" {
  type        = string
  description = "The name of eks cluster to deploy to"
}

variable "eks_assume_role_name" {
  type        = string
  description = "Name of IAM role to assume before authenticating with the EKS cluster."
  default     = "minds_eks_k8s_devops"
}

variable "top_level_domain" {
  type        = string
  description = "The top level domain that our traefik cluster should assume"
}


variable "certificate_validation_method" {
  type        = string
  description = "DNS or EMAIL"
}