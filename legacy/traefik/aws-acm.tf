################
### ACM Cert ###
################

resource "aws_acm_certificate" "this" {
  domain_name       = "*.${var.top_level_domain}"
  validation_method = var.certificate_validation_method

  subject_alternative_names = terraform.workspace == "sandbox" ? [
    var.top_level_domain
    ] : [
    var.top_level_domain,
    "minds.tv",
    "*.minds.tv",
    "minds.org",
    "*.minds.org"
  ]

  tags = {}

  options {
    certificate_transparency_logging_preference = "ENABLED"
  }

  lifecycle {
    create_before_destroy = true
  }
}

