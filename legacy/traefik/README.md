# Traefik

## Workspaces

- Default is production (terraform apply --var-file=production.tfvars)
- sandbox is sandbox (terraform apply --var-file=sandbox.tfvars)

## Deploying

`terraform apply --var-file=production.tfvars`

## DNS

`kubectl -n traefik get svc`

## Dashboard

`kubectl -n traefik port-forward $(kubectl -n traefik get pods --selector "app.kubernetes.io/name=traefik" --output=name) 9000:9000`
