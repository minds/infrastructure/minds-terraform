locals {
  autoscaling_config = {
    front = {
      enabled     = false
      minReplicas = var.is_development ? 0 : 2
      maxReplicas = var.is_development ? 0 : 2
      environments = {
        production = {
          enabled     = var.is_development ? false : true
          minReplicas = var.is_development ? 0 : 3 # High availability (HA) min for prod
          maxReplicas = var.is_development ? 0 : 20
        }
        canary = {
          enabled     = var.is_development ? false : true
          minReplicas = var.is_development ? 0 : 2 # Lower bar of HA for canary users
          maxReplicas = var.is_development ? 0 : 8
        }
        staging = {
          enabled     = var.is_development ? false : true
          minReplicas = var.is_development ? 0 : 1 # Devs don't need HA
          maxReplicas = var.is_development ? 0 : 2
        }
      }
    }
    engine = {
      enabled     = false
      minReplicas = var.is_development ? 0 : 8
      maxReplicas = var.is_development ? 0 : 8
      environments = {
        production = {
          enabled     = var.is_development ? false : true
          minReplicas = var.is_development ? 0 : 8 # High availability (HA) min for prod
          maxReplicas = var.is_development ? 0 : 24
        }
        canary = {
          enabled     = var.is_development ? false : true
          minReplicas = var.is_development ? 0 : 2 # Lower bar of HA for canary users
          maxReplicas = var.is_development ? 0 : 8
        }
        staging = {
          enabled     = var.is_development ? false : true
          minReplicas = var.is_development ? 0 : 1 # Devs don't need HA
          maxReplicas = var.is_development ? 0 : 2
        }
      }
    }
  }
}

resource "helm_release" "minds" {
  provider = helm.eks
  name     = "minds"

  repository = "../../../helm-charts"
  chart      = "minds"

  timeout = 1800 # 30 mins

  wait = !var.is_development

  set {
    name  = "pulsar.sslSkipVerify"
    value = true
  }

  set {
    name  = "siteUrl"
    value = "https://${var.domain}/"
  }
  set {
    name  = "siteEmail"
    value = "info@minds.com"
  }
  set {
    name  = "domain"
    value = var.domain
  }
  set {
    name  = "baseDomain" # basically, non-www
    value = var.base_domain
  }
  set {
    name  = "cdn_domain"
    value = var.cdn_domain
  }
  set {
    name  = "dataroot"
    value = "/gluster/data/elgg/"
  }
  set {
    name  = "tracing.enabled"
    value = true
  }

  ##
  # NetworkPolicy
  ##

  dynamic "set" {
    for_each = toset(["staging", "canary", "production"])
    content {
      name  = "environments.${set.key}.front.enableNetworkPolicy"
      value = var.front_enable_networkpolicy[set.key]
    }
  }

  dynamic "set" {
    for_each = toset(["staging", "canary", "production"])
    content {
      name  = "environments.${set.key}.engine.enableNetworkPolicy"
      value = var.engine_enable_networkpolicy[set.key]
    }
  }

  ##
  # Pod Autoscaling
  ##

  // Enable/disable autoscaling globally for frontend/engine
  dynamic "set" {
    for_each = local.autoscaling_config
    content {
      name  = "${set.key}.autoscaling.enabled"
      value = set.value.enabled
    }
  }

  // Enable/disable autoscaling for frontend/engine per each environment
  dynamic "set" {
    for_each = flatten([
      // For each application (frontend, engine)
      for app_key, app_val in local.autoscaling_config : [
        // Build a new object describing the environment and whether it is enabled
        for env_key, env_val in app_val.environments : {
          app         = app_key
          environment = env_key
          enabled     = env_val.enabled
        }
      ]
    ])

    // ex: { app = "engine", environment = "staging", enabled = false }
    content {
      name  = "environments.${set.value.environment}.${set.value.app}.autoscaling.enabled"
      value = set.value.enabled
    }
  }

  // Set minimum replicas for frontend/engine globally
  dynamic "set" {
    for_each = local.autoscaling_config
    content {
      name  = "${set.key}.autoscaling.minReplicas"
      value = set.value.minReplicas
    }
  }

  // Set maximum replicas for frontend/engine globally
  dynamic "set" {
    for_each = local.autoscaling_config
    content {
      name  = "${set.key}.autoscaling.maxReplicas"
      value = set.value.maxReplicas
    }
  }

  // Set minimum replicas for frontend/engine per each environment
  dynamic "set" {
    for_each = flatten([
      // For each application (frontend, engine)
      for app_key, app_val in local.autoscaling_config : [
        // Build a new object describing the environment and whether it is enabled
        for env_key, env_val in app_val.environments : {
          app         = app_key
          environment = env_key
          minReplicas = env_val.minReplicas
        }
      ]
    ])

    // ex: { app = "engine", environment = "staging", minReplicas = 1 }
    content {
      name  = "environments.${set.value.environment}.${set.value.app}.autoscaling.minReplicas"
      value = set.value.minReplicas
    }
  }

  // Set maximum replicas for frontend/engine per each environment
  dynamic "set" {
    for_each = flatten([
      // For each application (frontend, engine)
      for app_key, app_val in local.autoscaling_config : [
        // Build a new object describing the environment and whether it is enabled
        for env_key, env_val in app_val.environments : {
          app         = app_key
          environment = env_key
          maxReplicas = env_val.maxReplicas
        }
      ]
    ])

    // ex: { app = "engine", environment = "staging", maxReplicas = 1 }
    content {
      name  = "environments.${set.value.environment}.${set.value.app}.autoscaling.maxReplicas"
      value = set.value.maxReplicas
    }
  }

  ##
  # Environments
  ##
  set {
    name  = "environments.production.routingCookie"
    value = "false"
    type  = "string"
  }
  set {
    name  = "environments.production.routingPriority"
    value = 10
  }
  set {
    name  = "environments.canary.routingPriority"
    value = 20
  }
  set {
    name  = "environments.staging.routingPriority"
    value = 30
  }

  ##
  # Front configs
  ##

  # Production
  set {
    name  = "environments.production.front.imageTag"
    value = "production"
  }
  set {
    name  = "environments.production.front.replicaCount"
    value = var.is_development ? 0 : 2
  }
  # Canary
  set {
    name  = "environments.canary.front.imageTag"
    value = "canary"
  }
  set {
    name  = "environments.canary.front.replicaCount"
    value = var.is_development ? 0 : 1
  }
  # Staging
  set {
    name  = "environments.staging.front.imageTag"
    value = "staging"
  }
  set {
    name  = "environments.staging.front.replicaCount"
    value = var.is_development ? 0 : 1
  }

  #

  set {
    name  = "front.service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "front.replicaCount" # we use environments, so 0 should mean 0
    value = 0
  }

  #

  // Staging
  set {
    name  = "environments.staging.front.ssr.resources.requests.cpu"
    value = "500m"
  }
  set {
    name  = "environments.staging.front.ssr.resources.requests.memory"
    value = "768Mi"
  }
  set {
    name  = "environments.staging.front.ssr.resources.limits.cpu"
    value = "1"
  }
  set {
    name  = "environments.staging.front.ssr.resources.limits.memory"
    value = "1Gi"
  }
  set {
    name  = "environments.staging.front.ssr.resources.limits.ephemeral-storage"
    value = "2Gi"
  }

  // Canary
  set {
    name  = "environments.canary.front.ssr.resources.requests.cpu"
    value = "900m"
  }
  set {
    name  = "environments.canary.front.ssr.resources.requests.memory"
    value = "2Gi"
  }
  set {
    name  = "environments.canary.front.ssr.resources.limits.cpu"
    value = "1.5"
  }
  set {
    name  = "environments.canary.front.ssr.resources.limits.memory"
    value = "2Gi"
  }
  set {
    name  = "environments.canary.front.ssr.resources.limits.ephemeral-storage"
    value = "2Gi"
  }

  // Production
  set {
    name  = "environments.production.front.ssr.resources.requests.cpu"
    value = "2"
  }
  set {
    name  = "environments.production.front.ssr.resources.requests.memory"
    value = "2Gi"
  }
  set {
    name  = "environments.production.front.ssr.resources.limits.cpu"
    value = "4"
  }
  set {
    name  = "environments.production.front.ssr.resources.limits.memory"
    value = "2Gi"
  }
  set {
    name  = "environments.production.front.ssr.resources.limits.ephemeral-storage"
    value = "2Gi"
  }

  ##
  # Nginx
  # Applies to both engine and front
  ##

  set {
    name  = "nginx.image.repository"
    value = "registry.gitlab.com/minds/infrastructure/nginx-opentracing"
  }
  set {
    name  = "nginx.image.tag"
    value = "nginx-1.21.6-alpine"
  }

  # set {
  #   name  = "nginx.resources.requests.cpu"
  #   value = "1"
  # }
  set {
    name  = "nginx.resources.limits.cpu"
    value = "1"
  }
  # set {
  #   name  = "nginx.resources.requests.memory"
  #   value = "1Gi"
  # }
  set {
    name  = "nginx.resources.limits.memory"
    value = "1Gi"
  }
  set {
    name  = "nginx.worker_connections"
    value = 20001
  }

  ##
  # Engine configs
  ##

  ## Production
  set {
    name  = "environments.production.engine.imageTag"
    value = "production"
  }
  set {
    name  = "environments.production.engine.replicaCount"
    value = var.is_development ? 0 : 8
  }
  ## Canary
  set {
    name  = "environments.canary.engine.replicaCount"
    value = var.is_development ? 0 : 2
  }
  set {
    name  = "environments.canary.engine.imageTag"
    value = "canary"
  }
  ## Staging
  set {
    name  = "environments.staging.engine.replicaCount"
    value = var.is_development ? 0 : 1
  }
  set {
    name  = "environments.staging.engine.imageTag"
    value = "staging"
  }

  #
  set {
    name  = "engine.serviceAccount.create"
    value = true
  }

  set {
    name  = "engine.service.type"
    value = "ClusterIP"
  }

  set {
    name  = "engine.replicaCount"
    value = 0 # we use environments, so 0 should mean 0
  }

  ##
  # PHP FPM Configs
  ##

  set {
    name  = "engine.phpfpm.max_children"
    value = 50 # we will not have more than 25 processes
  }
  # set {
  #   name  = "engine.phpfpm.min_spare_servers"
  #   value = 10 # we start we 10 processes
  # }
  set {
    name  = "engine.phpfpm.max_requests"
    value = 1000 # restart each process after 1k requests
  }

  #

  // Staging
  set {
    name  = "environments.staging.engine.resources.requests.cpu"
    value = "150m"
  }
  set {
    name  = "environments.staging.engine.resources.requests.memory"
    value = "1Gi"
  }
  set {
    name  = "environments.staging.engine.resources.limits.cpu"
    value = "350m"
  }
  set {
    name  = "environments.staging.engine.resources.limits.memory"
    value = "1.5Gi"
  }

  // Canary
  set {
    name  = "environments.canary.engine.resources.requests.cpu"
    value = "260m"
  }
  set {
    name  = "environments.canary.engine.resources.requests.memory"
    value = "1Gi"
  }
  set {
    name  = "environments.canary.engine.resources.limits.cpu"
    value = "750m"
  }
  set {
    name  = "environments.canary.engine.resources.limits.memory"
    value = "2Gi"
  }

  // Production
  set {
    name  = "environments.production.engine.resources.requests.cpu"
    value = "2"
  }
  set {
    name  = "environments.production.engine.resources.requests.memory"
    value = "2Gi"
  }
  set {
    name  = "environments.production.engine.resources.limits.cpu"
    value = "2.5"
  }
  set {
    name  = "environments.production.engine.resources.limits.memory"
    value = "2.2Gi"
  }

  # We use vault for secrets, so don't mount defaults

  set {
    name  = "engine.defaultSecrets"
    value = var.is_development
  }

  set {
    # Copies the vault template secrets to env variables
    name  = "engine.entrypointPrefix"
    value = "source /vault/secrets/env"
  }

  ##
  # Istio annotations
  ##

  set {
    name  = "engine.annotations.traffic\\.sidecar\\.istio\\.io/excludeOutboundIPRanges"
    value = "0.0.0.0/0"
    type  = "string"
  }

  ##
  # Secrets for engine
  ##
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-init-first"
    value = "true"
    type  = "string"
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-pre-populate-only"
    value = "true"
    type  = "string"
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject"
    value = "true"
    type  = "string"
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-status"
    value = "update"
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/role"
    value = var.vault_engine_role
  }

  # Generic
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-env"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-env"
    value = <<EOT
  {{- with secret "${var.vault_engine_path}" -}}
  export MINDS_ENV_matrix__admin_access_token="{{ .Data.data.matrix_admin_access_token }}"
  
  #
  export MINDS_ENV_email__smtp__host="{{ .Data.data.smtp_host }}"
  export MINDS_ENV_email__smtp__username="{{ .Data.data.smtp_username }}"
  export MINDS_ENV_email__smtp__password="{{ .Data.data.smtp_password }}"
  # Email secret is for campaign rewards mainly (for signing - has nothing to do with encryption)
  export MINDS_ENV_emails_secret="{{ .Data.data.email_hash_secret}}"
  export MINDS_ENV_email_confirmation__signing_key="{{ .Data.data.email_confirmation_signing_key }}"
  export MINDS_ENV_email__sendgrid__api_key="{{ .Data.data.sendgrid_api_key }}"

  #
  export MINDS_ENV_cassandra__username="{{ .Data.data.cassandra_username }}"
  export MINDS_ENV_cassandra__password="{{ .Data.data.cassandra_password }}"

  #
  export MINDS_ENV_captcha__jwt_secret="{{ .Data.data.captcha_bypass_key }}"
  export MINDS_ENV_captcha__bypass_key="{{ .Data.data.captcha_bypass_key }}"
  export MINDS_ENV_cypress__shared_key="{{ .Data.data.captcha_bypass_key }}"
  export MINDS_ENV_cypress__friendly_captcha__signing_secret="{{ .Data.data.captcha_friendly_captcha_key }}"

  #
  export MINDS_ENV_oauth__clients__mobile__secret="{{ .Data.data.oauth_mobile_secret }}"
  export MINDS_ENV_oauth__clients__matrix__secret="{{ .Data.data.oauth_matrix_secret }}"
  export MINDS_ENV_oauth__encryption_key="{{ .Data.data.oauth_encryption_key }}"

  #
  export MINDS_ENV_cloudflare__account_id="{{ .Data.data.cloudflare_account_id }}"
  export MINDS_ENV_cloudflare__api_key="{{ .Data.data.cloudflare_api_key }}"
  export MINDS_ENV_cloudflare__email="{{ .Data.data.cloudflare_email}}"
  export MINDS_ENV_cloudflare__webhook_secret="{{ .Data.data.cloudflare_webhook_secret }}"
  export MINDS_ENV_cloudflare__signing_key__id="{{ .Data.data.cloudflare_signing_key_id }}"
  export MINDS_ENV_cloudflare__signing_key__pem="{{ .Data.data.cloudflare_signing_key_pem }}"

  #
  export MINDS_ENV_payments__stripe__public_key="{{ .Data.data.stripe_public_key }}"
  export MINDS_ENV_payments__stripe__api_key="{{ .Data.data.stripe_secret_key }}"
  export MINDS_ENV_payments__stripe__test_api_key="{{ .Data.data.stripe_test_secret_key }}"
  export MINDS_ENV_payments__stripe__webhook_keys__default="{{ .Data.data.stripe_webhook_keys_default }}"
  export MINDS_ENV_payments__stripe__webhook_keys__connect="{{ .Data.data.stripe_webhook_keys_connect }}"

  #
  export MINDS_ENV_zendesk__private_key="{{ .Data.data.zendesk_private_key }}"

  #
  export MINDS_ENV_google__youtube__api_key="{{ .Data.data.google_api_key }}"
  export MINDS_ENV_google__youtube__client_id="{{ .Data.data.youtube_client_id }}"
  export MINDS_ENV_google__youtube__client_secret="{{ .Data.data.youtube_client_secret }}"
  export MINDS_ENV_google__translation="{{ .Data.data.google_api_key }}"
  export MINDS_ENV_google__geolocation="{{ .Data.data.google_api_key }}"
  export MINDS_ENV_google__vision__api_key="{{ .Data.data.google_api_key }}"
  export MINDS_ENV_google__bigquery__project_id="{{ .Data.data.google_project_id }}"

  #
  export MINDS_ENV_blockchain__contracts__boost__wallet_pkey="{{ .Data.data.blockchain_boost_pkey }}"
  export MINDS_ENV_blockchain__contracts__wire__wallet_pkey="{{ .Data.data.blockchain_wire_pkey }}"
  export MINDS_ENV_blockchain__contracts__withdraw__wallet_pkey="{{ .Data.data.blockchain_withdraw_pkey }}"
  export MINDS_ENV_phone_number_hash_salt="{{ .Data.data.phone_number_salt }}"
  export MINDS_ENV_blockchain__web3_service__base_url="http://web3-server:3333/"
  export MINDS_ENV_blockchain__web3_service__wallet_encryption_key="{{ .Data.data.blockchain_web3server_shared_key }}"

  #
  export MINDS_ENV_twilio__account_sid="{{ .Data.data.twilio_account_sid }}"
  export MINDS_ENV_twilio__auth_token="{{ .Data.data.twilio_auth_token }}"
  export MINDS_ENV_twilio__from="{{ .Data.data.twilio_from }}"
  export MINDS_ENV_twilio__from="{{ .Data.data.twilio_from }}"
  export MINDS_ENV_twilio__verify__service_sid="{{ .Data.data.twilio_verify_sid }}"

  #
  export MINDS_ENV_sockets__jwt_secret="{{ .Data.data.sockets_jwt_secret }}"

  #
  export MINDS_ENV_twitter__bearer_token="{{ .Data.data.twitter_bearer_token }}"

  # Twitter OAuth2 integration keys
  export MINDS_ENV_twitter__client_id="{{ .Data.data.twitter_client_id }}"
  export MINDS_ENV_twitter__client_secret="{{ .Data.data.twitter_client_secret }}"

  #
  export MINDS_ENV_iframely__key="{{ .Data.data.iframely_key }}"

  #
  export MINDS_ENV_elasticsearch__username="{{ .Data.data.elasticsearch_username }}"
  export MINDS_ENV_elasticsearch__password="{{ .Data.data.elasticsearch_password }}"

  ## Copied keys
  ## These are defined below
  export MINDS_ENV_sessions__private_key="/vault/secrets/sessions-private-key.pem"
  export MINDS_ENV_sessions__public_key="/vault/secrets/sessions-public-key.pem"

  export MINDS_ENV_encryptionKeys__email__private="/vault/secrets/email-private-key.pem"
  export MINDS_ENV_encryptionKeys__email__public="/vault/secrets/email-public-key.pem"

  export MINDS_ENV_encryptionKeys__twt_tokens__private="/vault/secrets/twitter-private-key.pem"
  export MINDS_ENV_encryptionKeys__twt_tokens__public="/vault/secrets/twitter-public-key.pem"

  export MINDS_ENV_oauth__private_key="/vault/secrets/oauth-private-key.pem"
  export MINDS_ENV_oauth__public_key="/vault/secrets/oauth-public-key.pem"

  export MINDS_ENV_google__firebase__key_path="/vault/secrets/fcm-client-credentials.json"
  export MINDS_ENV_google__bigquery__key_file_path="/vault/secrets/google-bigquery-key.json"

  #
  export MINDS_ENV_webpush_vapid_details__public_key="{{ .Data.data.webpush_vapid_details_public_key }}"
  export MINDS_ENV_webpush_vapid_details__private_key="{{ .Data.data.webpush_vapid_details_private_key }}"
  export MINDS_ENV_webpush_vapid_details__subject="{{ .Data.data.webpush_vapid_details_subject }}"

  #
  export MINDS_ENV_mysql__password="{{ .Data.data.mysql_password }}"

  #
  export MINDS_ENV_blockchain__unstoppable_domains__api_key="{{ .Data.data.unstoppable_domain_api_key }}"
  {{- end -}}
EOT
  }

  ##
  # Keys
  # Yes, this is way over the top, far too verbose
  ##

  # sessions-secret
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-sessions-private-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-sessions-private-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.sessions_private_key_pem }}
    {{- end -}}
EOT
  }
  # sessions-public
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-sessions-public-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-sessions-public-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.sessions_public_key_pem }}
    {{- end -}}
EOT
  }
  # email-secret
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-email-private-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-email-private-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.email_private_key_pem }}
    {{- end -}}
EOT
  }
  # email-public
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-email-public-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-email-public-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.email_public_key_pem }}
    {{- end -}}
EOT
  }
  # twitter-secret
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-twitter-private-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-twitter-private-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.twitter_private_key_pem }}
    {{- end -}}
EOT
  }
  # twitter-public
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-twitter-public-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-twitter-public-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.twitter_public_key_pem }}
    {{- end -}}
EOT
  }
  # oauth-private
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-oauth-private-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-oauth-private-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.oauth_private_key_pem }}
    {{- end -}}
EOT
  }
  # oauth-public
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-oauth-public-key\\.pem"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-oauth-public-key\\.pem"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.oauth_public_key_pem }}
    {{- end -}}
EOT
  }
  # firebase key
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-fcm-client-credentials\\.json"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-fcm-client-credentials\\.json"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.fcm_client_credentials_json }}
    {{- end -}}
EOT
  }
  # bigquery key
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-secret-google-bigquery-key\\.json"
    value = var.vault_engine_path
  }
  set {
    name  = "engine.annotations.vault\\.hashicorp\\.com/agent-inject-template-google-bigquery-key\\.json"
    value = <<EOT
    {{- with secret \"${var.vault_engine_path}\" -}}
    {{ .Data.data.google_bigquery_key_json }}
    {{- end -}}
EOT
  }

  ##
  # Runner configurations
  # Becareful of the crazy backlash escaping :/
  ##

  ####################################
  # Transcoder Configurations        #
  ####################################

  set {
    name  = "transcoder.dir"
    value = var.transcoder_dir
  }
  set {
    name  = "transcoder.cinemr_url"
    value = "https://cdn-cinemr.minds.com/cinemr_com/"
  }

  ####################################
  # EKS Load Balancer Configurations #
  ####################################

  set {
    name  = "ingress.enabled"
    value = true
  }

  ####################################
  # Cassandra Configurations         #
  ####################################

  set {
    name  = "cassandra.server"
    value = var.eks_cassandra_server
  }

  ####################################
  # ElasticSearch Configurations     #
  ####################################

  set {
    name  = "elasticsearch.server"
    value = var.eks_elasticsearch_server
  }

  ####################################
  # Pulsar Configurations     #
  ####################################

  set {
    name  = "pulsar.server"
    value = var.eks_pulsar_server
  }

  ##
  # Matrix setup
  ##

  set {
    name  = "matrix.homeserver_domain"
    value = "minds.com"
  }
  set {
    name  = "matrix.synapse_domain"
    value = "synapse.matrix.minds.com"
  }
  set {
    name  = "matrix.chat_url"
    value = "https://chat.minds.com"
  }
  set {
    name  = "matrix.sso_redirect_url"
    value = "https://synapse.matrix.minds.com/_synapse/client/oidc/callback"
  }

  ####################################
  # Blockchain Configs               #
  ####################################

  set {
    name  = "blockchain.tokenAddress"
    value = "0xb26631c6dda06ad89b93c71400d25692de89c068"
  }
  set {
    name  = "blockchain.blockchainEndpoint"
    value = "https://mainnet.infura.io/v3/708b51690a43476092936f9818f8c4fa"
  }
  set {
    name  = "blockchain.clientNetwork"
    value = "1"
  }
  set {
    name  = "blockchain.testNet"
    value = "false"
  }
  set {
    name  = "blockchain.withdrawAddress"
    value = "0xdd10ccb3100980ecfdcbb1175033f0c8fa40548c"
  }
  set {
    name  = "blockchain.withdrawWallet"
    value = "0x6F2548B1BEE178a49c8EA09BE6845F6AeAf3E8Da"
  }
  set {
    name  = "blockchain.boostAddress"
    value = "0x400e9f16c25ae165a5c526a13289cb7d0f12b966"
  }
  set {
    name  = "blockchain.wireAddress"
    value = "0x4b637bba81d24657d4c6acc173275f3e11a8d5d7"
  }
  set {
    name  = "blockchain.plusAddress"
    value = "0x6f2548b1bee178a49c8ea09be6845f6aeaf3e8da"
  }
  set {
    name  = "blockchain.plusUserGUID"
    value = "730071191229833224"
    type  = "string"
  }

  ##
  # Pro/Plus configs
  ##

  set {
    name  = "handler.plus"
    value = "730071191229833224"
  }
  set {
    name  = "handler.plus_support_tier_urn"
    value = "urn:support-tier:730071191229833224/10000000025000000"
  }
  set {
    name  = "handler.pro"
    value = "1030390936930099216"
  }
  set {
    name  = "handler.boost"
    value = "1330556528821800962"
  }

  ##
  # Video configs
  ##

  set {
    name  = "max_video_length"
    value = 1260 # 21 mins (20 with overflow)
  }
  set {
    name  = "max_video_length_plus"
    value = 5700 # 95 mins (90 with overflow)
  }
  set {
    name  = "max_video_file_size"
    value = 4000000000 # 4GB
  }

  ##
  # Boost configs
  ##

  set {
    name  = "max_daily_boost_views"
    value = 10000
  }

  ##
  # AWS configs
  ##

  set {
    name  = "aws.s3Bucket"
    value = "mindsfs"
  }
  set {
    name  = "aws.sqs_namespace"
    value = var.aws_sqs_namespace
  }

  ##
  # Other
  ##

  set {
    name  = "last_tos_update"
    value = 1558597098
  }
  set {
    name  = "permaweb.host"
    value = "minds-permaweb"
  }
  set {
    name  = "zendesk.base_url"
    value = "https://minds.zendesk.com/"
  }
  set {
    name  = "twitter.min_followers_for_sync"
    value = 25000
  }
  set {
    name  = "statuspage_io.url"
    value = "https://status.minds.com/"
  }
  set {
    name  = "monitoring.enabled"
    value = var.is_development ? false : true
  }
  set {
    name  = "default_recommendations_user"
    value = "100000000000000519"
    type  = "string"
  }
  set {
    name  = "growthbook.features_endpoint"
    value = var.is_development ? "https://growthbook-api.minds.com/api/features/key_sand_1cef5623259564fd" : "https://growthbook-api.minds.com/api/features/key_prod_281099ceee8eac9e"
  }
  set {
    name  = "istio.enabled"
    value = "true"
  }

  ####################################
  # Detect if configMap has changed  #
  ####################################
  set {
    name = "configMap.checksum"
    value = join(".", [
      filemd5("../../../helm-charts/minds/templates/front-configMap.yaml"),
      filemd5("../../../helm-charts/minds/templates/engine-configMap.yaml")
    ])
  }

  ##
  # Sockets
  ##
  # Socket secret mounting
  set {
    # Copies the vault template secrets to env variables
    name  = "sockets.entrypointPrefix"
    value = "source /vault/secrets/env"
  }
  set {
    name  = "sockets.enableNetworkPolicy"
    value = "true"
  }
  set {
    name  = "sockets.annotations.vault\\.hashicorp\\.com/agent-pre-populate-only"
    value = "true"
    type  = "string"
  }
  set {
    name  = "sockets.annotations.vault\\.hashicorp\\.com/agent-init-first"
    value = "true"
    type  = "string"
  }
  set {
    name  = "sockets.annotations.vault\\.hashicorp\\.com/agent-inject"
    value = "true"
    type  = "string"
  }
  set {
    name  = "sockets.annotations.vault\\.hashicorp\\.com/agent-inject-status"
    value = "update"
  }
  set {
    name  = "sockets.annotations.vault\\.hashicorp\\.com/role"
    value = var.vault_engine_role
  }
  set {
    name  = "sockets.annotations.vault\\.hashicorp\\.com/agent-inject-secret-env"
    value = var.vault_engine_path
  }
  set {
    name  = "sockets.annotations.vault\\.hashicorp\\.com/agent-inject-template-env"
    value = <<EOT
  {{- with secret "${var.vault_engine_path}" -}}
  export JWT_SECRET="{{ .Data.data.sockets_jwt_secret }}"
  export CASSANDRA_USERNAME="{{ .Data.data.cassandra_username }}"
  export CASSANDRA_PASSWORD="{{ .Data.data.cassandra_password }}"
  {{- end -}}
EOT
  }
  # Other socket configs
  set {
    name  = "sockets.image.repository"
    value = "registry.gitlab.com/minds/sockets"
  }
  set {
    name  = "sockets.serverUri"
    value = "ha-socket-alb-io-us-east-1.minds.com"
  }
  set {
    name  = "sockets.jwt.domain"
    value = "minds.com"
  }
  set {
    name  = "sockets.service.type"
    value = "ClusterIP"
  }

}
