##

variable "eks_assume_role_name" {
  type        = string
  description = "Name of IAM role to assume before authenticating with the EKS cluster."
}

variable "eks_cluster_name" {
  type        = string
  description = "The name of eks cluster to deploy to"
}

##

variable "domain" {
  type        = string
  description = "The domain for the minds stack"
}

variable "base_domain" {
  type        = string
  description = "The non-www domain, if necessary"
}

variable "cdn_domain" {
  type        = string
  description = "The cdn domain for the minds stack"
}

variable "front_replicas" {
  type        = number
  description = "The number of replicas to use for the frontend services"
  default     = 1
}

variable "backend_replicas" {
  type        = number
  description = "The number of replicas to use for the backend services"
  default     = 1
}

##

variable "vault_engine_path" {
  type        = string
  description = "The kv (key/value) secret path"
}

variable "vault_engine_role" {
  type        = string
  description = "The vault role to asumme (must have policy setup on vault)"
}

variable "vault_web3server_path" {
  type        = string
  description = "The kv (key/value) secret pathfor web3 server"
}

variable "vault_web3server_role" {
  type        = string
  description = "The vault role to asumme (must have policy setup on vault) for web3server"
}

##

variable "eks_elasticsearch_server" {
  type        = string
  description = "The endpoint for elasticsearch"
}

variable "eks_cassandra_server" {
  type        = string
  description = "The endpoint for cassandra"
}

variable "eks_pulsar_server" {
  type        = string
  description = "The endpoint for pulsar"
}

variable "eks_redis_master_server" {
  type        = string
  description = "The endpoint for redis (master)"
}

variable "eks_redis_slave_server" {
  type        = string
  description = "The endpoint for redis (salve)"
}

##

variable "is_development" {
  type        = bool
  description = "If this is a development mode setup"
  default     = false
}

variable "transcoder_dir" {
  type        = string
  description = "The S3 directory where videos are uploaded to"
  default     = "cinemr_com"
}

variable "aws_sqs_namespace" {
  type        = string
  description = "SQS (deprecated) requires a namespace (prefix) for different environments"
  default     = ""
}

##

variable "runners_version" {
  type    = string
  default = "production"
}


##

variable "front_enable_networkpolicy" {
  type        = map(bool)
  description = "Whether to enable NetworkPolicy resources frot Frontend."
  default = {
    staging    = true
    canary     = true
    production = false
  }
}

variable "engine_enable_networkpolicy" {
  type        = map(bool)
  description = "Whether to enable NetworkPolicy resources frot Engine."
  default = {
    staging    = true
    canary     = true
    production = false
  }
}
