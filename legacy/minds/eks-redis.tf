resource "kubernetes_namespace" "caching" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "caching-new"
    }

    labels = {
      name = "caching-new"
    }

    name = "caching-new"
  }
}

# commonConfiguration: |-
#   appendonly no
#   save ""
# master.disableCommands: []

locals {
  redis_values = {
    master = {
      nodeSelector = { stateful = "true" }
      tolerations = [
        {
          key      = "stateful"
          operator = "Equal"
          value    = "true"
          effect   = "NoSchedule"
        }
      ]
    }
    replica = {
      nodeSelector = { stateful = "true" }
      tolerations = [
        {
          key      = "stateful"
          operator = "Equal"
          value    = "true"
          effect   = "NoSchedule"
        }
      ]
    }
  }
}

resource "helm_release" "redis" {
  provider  = helm.eks
  name      = "redis"
  namespace = kubernetes_namespace.caching.metadata[0].name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "redis"

  set {
    name  = "auth.enabled"
    value = false
  }

  set {
    name  = "master.persistence.enabled"
    value = false
  }

  set {
    name  = "master.resources.requests.cpu"
    value = var.is_development ? "200m" : 2
  }

  set {
    name  = "master.resources.requests.memory"
    value = var.is_development ? "256Mi" : "9Gi"
  }

  set {
    name  = "master.extraFlags[0]"
    value = "--maxmemory 8gb"
  }

  set {
    name  = "master.extraFlags[1]"
    value = "--maxmemory-policy allkeys-lfu"
  }

  values = [
    yamlencode(local.redis_values),
    <<EOT
commonConfiguration: |-
  appendonly no
  save ""
master.disableCommands: []
EOT
  ]

  set {
    name  = "replica.persistence.enabled"
    value = false
  }

  set {
    name  = "replica.replicaCount"
    value = 1
  }

  set {
    name  = "replica.resources.requests.cpu"
    value = var.is_development ? "200m" : 2
  }

  set {
    name  = "replica.resources.requests.memory"
    value = var.is_development ? "256Mi" : "9Gi"
  }

  set {
    name  = "replica.extraFlags[0]"
    value = "--maxmemory 8gb"
  }

  set {
    name  = "replica.extraFlags[1]"
    value = "--maxmemory-policy allkeys-lfu"
  }

  ##
  # Metrics
  ##
  set {
    name  = "metrics.enabled"
    value = var.is_development ? "false" : "true"
  }
  set {
    name  = "metrics.serviceMonitor.enabled"
    value = var.is_development ? "false" : "true"
  }

}