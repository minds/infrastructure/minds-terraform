eks_assume_role_name = "minds_eks_k8s_devops"
eks_cluster_name     = "minds-eks-jkqjL8xS"
replicas             = 3

is_development = false

domain      = "www.minds.com"
cdn_domain  = "cdn.minds.com"
base_domain = "minds.com"

vault_engine_path = "kv/engine"
vault_engine_role = "eks-engine"

vault_web3server_path = "kv/web3-server"
vault_web3server_role = "eks-web3-server"

eks_elasticsearch_server = "https://elasticsearch-opendistro-es-client-service.elasticsearch.svc.cluster.local:9200"
eks_cassandra_server     = "minds-prod-k8ssandra-eks-us-east-1-service.cassandra.svc.cluster.local"
eks_pulsar_server        = "pulsar-proxy.pulsar.svc.cluster.local"
eks_redis_master_server  = "redis-master.caching-new.svc.cluster.local"
eks_redis_slave_server   = "redis-replicas.caching-new.svc.cluster.local"

runners_version = "production"
