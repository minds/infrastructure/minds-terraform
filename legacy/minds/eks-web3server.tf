resource "helm_release" "web3server" {
  provider = helm.eks
  name     = "web3-server"

  repository = "../../../web3-server/helm-chart"
  chart      = "web3-server"

  set {
    name  = "image.tag"
    value = "447265256"
  }

  set {
    # Copies the vault template secrets to env variables
    name  = "entrypointPrefix"
    value = "source /vault/secrets/env"
  }
  set {
    name  = "podAnnotations.vault\\.hashicorp\\.com/agent-pre-populate-only"
    value = "true"
    type  = "string"
  }
  set {
    name  = "podAnnotations.vault\\.hashicorp\\.com/agent-inject"
    value = "true"
    type  = "string"
  }
  set {
    name  = "podAnnotations.vault\\.hashicorp\\.com/agent-inject-status"
    value = "update"
  }
  set {
    name  = "podAnnotations.vault\\.hashicorp\\.com/role"
    value = var.vault_web3server_role
  }
  set {
    name  = "podAnnotations.vault\\.hashicorp\\.com/agent-inject-secret-env"
    value = var.vault_web3server_path
  }
  set {
    name  = "podAnnotations.vault\\.hashicorp\\.com/agent-inject-template-env"
    value = <<EOT
  {{- with secret "${var.vault_web3server_path}" -}}

  # Contract address for withdrawal contract.
  WITHDRAW_CONTACT_ADDRESS="{{ .Data.data.withdraw_contract_address }}"

  # Shared key between web3-server and client
  WALLET_ENCRYPTION_KEY="{{ .Data.data.shared_key }}"

  # Infura project id
  RINKEBY_PROJECT_ID="{{ .Data.data.infura_project_id }}"

  # Wallet public and private keys.
  WITHDRAW_WALLET_XPUB="{{ .Data.data.withdraw_wallet_address }}"
  WITHDRAW_WALLET_XPRIV="{{ .Data.data.withdraw_wallet_private_key }}"
  GENERAL_WALLET_XPUB="{{ .Data.data.general_wallet_address }}"
  GENERAL_WALLET_XPRIV="{{ .Data.data.general_wallet_private_key }}"

  {{- end -}}
EOT
  }

}