# Minds

## Deployment

This repository makes use of [Terraform Workspaces](https://www.terraform.io/language/state/workspaces) for deploying to multiple environments.

You can see the list of available workspaces with `terraform workspace list`:

```bash
$ terraform workspace list
* default
  sandbox
```

Production deployments make use of the `default` workspace, while Sandbox deployments use the `sandbox` workspace. Variables are also populated using separate `*.tfvars` files for each environment rather than the default `terraform.tfvars`. 

For example, do create a production deployment:

```bash
terraform apply -var-file production.tfvars
```

You can also set a default environment for your shell with the `TF_CLI_ARGS` environment variable. For example:

```bash
export TF_CLI_ARGS="-var-file production.tfvars

terraform apply
```

In order to deploy to Sandbox, first select the correct workspace and ensure that you point to the correct `sandbox.tfvars` file:

```bash
terraform workspace select sandbox

terraform apply -var-file sandbox.tfvars
```

### Role Assumption

By default, both workspaces are configured to assume the `minds_eks_k8s_developers` IAM role for authenticating with EKS. This is to enable developers to run `terraform plan`, although this role does not allow creating, updating, or deleting resources in production. To deploy changes to production, you must assume the `minds_eks_k8s_admins` role:

```bash
terraform workspace select default
terraform apply -var-file production.tfvars -var eks_assume_role_name="minds_eks_k8s_admins"
```

## Assumptions

- Vault is already setup
- A kubernetes auth method & role have been setup on vault
