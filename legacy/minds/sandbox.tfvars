eks_assume_role_name = "minds_eks_k8s_devops"
eks_cluster_name     = "sandbox"

is_development = true

replicas = 1

domain      = "www.minds.io"
cdn_domain  = "www.minds.io"
base_domain = "minds.io"

vault_engine_path = "minds-sandbox-kv/engine"
vault_engine_role = "eks-cluster"

vault_web3server_path = "kv/web3-server"
vault_web3server_role = "eks-web3-server"

eks_elasticsearch_server = "https://elasticsearch-opendistro-es-client-service.elasticsearch.svc.cluster.local:9200"
eks_cassandra_server     = "cassandra"
eks_pulsar_server        = "pulsar-proxy.pulsar.svc.cluster.local"
eks_redis_master_server  = "redis-master.caching-new.svc.cluster.local"
eks_redis_slave_server   = "redis-replicas.caching-new.svc.cluster.local"

aws_sqs_namespace = "Sandbox"

runners_version = "latest"

matrix_admin_access_token = "undefined"