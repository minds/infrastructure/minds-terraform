#######################
### Argo CD Projecs ###
#######################

// https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#projects

resource "kubernetes_manifest" "project_production" {
  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "AppProject"
    metadata = {
      name      = "production"
      namespace = kubernetes_namespace.this.metadata[0].name
      labels    = { eks_cluster = var.eks_cluster_name[terraform.workspace] }
    }
    spec = {
      clusterResourceWhitelist = [{ group = "*", kind = "*" }]
      destinations             = [{ namespace = "*", server = "*" }]
      orphanedResources        = { warn = false }
      sourceRepos              = ["*"]
    }
  }
}
