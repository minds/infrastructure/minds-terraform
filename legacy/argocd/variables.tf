###########
### EKS ###
###########

variable "eks_assume_role_name" {
  type        = map(string)
  description = "Name of IAM role to assume before authenticating with the EKS cluster."
  default = {
    sandbox = "minds_eks_k8s_devops"
    default = "minds_eks_k8s_admins"
  }
}

variable "eks_cluster_name" {
  type        = map(string)
  description = "The name of eks cluster to deploy to"

  default = {
    sandbox = "sandbox"
    default = "minds-eks-jkqjL8xS"
  }
}
