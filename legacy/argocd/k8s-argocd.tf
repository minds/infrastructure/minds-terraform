resource "kubernetes_namespace" "this" {
  metadata {
    name   = "argo-cd"
    labels = { name = "argo-cd" }
  }
}

resource "helm_release" "this" {
  name      = "argo-cd"
  namespace = kubernetes_namespace.this.metadata[0].name

  chart      = "argo-cd"
  repository = "https://argoproj.github.io/argo-helm"
  version    = "4.9.14"

  values = [yamlencode({
    applicationSet = {
      resources = {
        requests = {
          cpu    = "100m"
          memory = "128Mi"
        }
        limits = {
          cpu    = "100m"
          memory = "128Mi"
        }
      }
    }
    controller = {
      enableStatefulSet = true
      resources = {
        requests = {
          cpu    = "250m"
          memory = "256Mi"
        }
        limits = {
          cpu    = "500m"
          memory = "512Mi"
        }
      }
    }
    dex = {
      resources = {
        requests = {
          cpu    = "10m"
          memory = "32Mi"
        }
        limits = {
          cpu    = "50m"
          memory = "64Mi"
        }
      }
    }
    notifications = {
      resources = {
        requests = {
          cpu    = "100m"
          memory = "128Mi"
        }
        limits = {
          cpu    = "100m"
          memory = "128Mi"
        }
      }
    }
    repoServer = {
      replicas = 2
      resources = {
        requests = {
          cpu    = "10m"
          memory = "64Mi"
        }
        limits = {
          cpu    = "50m"
          memory = "128Mi"
        }
      }
    }
    server = {
      replicas = 2
      env      = [{ name = "ARGOCD_API_SERVER_REPLICAS", value = "2" }]
      resources = {
        requests = {
          cpu    = "50m"
          memory = "64Mi"
        }
        limits = {
          cpu    = "100m"
          memory = "128Mi"
        }
      }
    }
    redis-ha = {
      enabled      = true
      nodeSelector = { stateful = "true" }
      tolerations = [
        {
          key      = "stateful"
          operator = "Equal"
          value    = "true"
          effect   = "NoSchedule"
        }
      ]
      redis = {
        resources = {
          requests = {
            cpu    = "100m"
            memory = "200Mi"
          }
          limits = {
            cpu    = "200m"
            memory = "700Mi"
          }
        }
      }
      haproxy = {
        resources = {
          requests = {
            cpu    = "100m"
            memory = "200Mi"
          }
          limits = {
            cpu    = "250m"
            memory = "700Mi"
          }
        }
      }
    }
  })]
}
