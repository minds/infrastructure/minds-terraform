###############################
### Argo CD ApplicationSets ###
###############################

## Nostr Relay

resource "kubernetes_manifest" "appset_nostr_relay" {

  depends_on = [kubernetes_manifest.project_production]

  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "ApplicationSet"
    metadata = {
      name      = "nostr-relay"
      namespace = kubernetes_namespace.this.metadata[0].name
      labels    = { eks_cluster = var.eks_cluster_name[terraform.workspace] }
    }
    spec = {
      generators = [{
        list = {
          elements = [
            { project = "production", url = "https://kubernetes.default.svc" }
          ]
        }
      }]
      template = {
        metadata = { name = "nostr-relay-{{project}}" }
        spec = {
          project = "default"
          source = {
            repoURL        = "https://gitlab.com/minds/infrastructure/nostr-relay.git"
            path           = "k8s/helm"
            targetRevision = "HEAD"
          }
          destination = {
            server    = "{{url}}"
            namespace = "default"
          }
        }
      }
    }
  }
}

## Metascraper

resource "kubernetes_manifest" "appset_metascraper" {

  depends_on = [kubernetes_manifest.project_production]

  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "ApplicationSet"
    metadata = {
      name      = "metascraper-server"
      namespace = kubernetes_namespace.this.metadata[0].name
      labels    = { eks_cluster = var.eks_cluster_name[terraform.workspace] }
    }
    spec = {
      generators = [{
        list = {
          elements = [
            { project = "production", url = "https://kubernetes.default.svc" }
          ]
        }
      }]
      template = {
        metadata = { name = "metascraper-server" }
        spec = {
          project = "default"
          source = {
            repoURL        = "https://gitlab.com/minds/developers/metascraper-server.git"
            path           = "./helm-chart"
            targetRevision = "HEAD"
          }
          destination = {
            server    = "{{url}}"
            namespace = "default"
          }
        }
      }
    }
  }
}

## Istio

resource "kubernetes_manifest" "appset_istio" {

  depends_on = [kubernetes_manifest.project_production]

  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "ApplicationSet"
    metadata = {
      name      = "istio"
      namespace = kubernetes_namespace.this.metadata[0].name
      labels    = { eks_cluster = var.eks_cluster_name[terraform.workspace] }
    }
    spec = {
      generators = [{
        list = {
          elements = [
            { project = "production", url = "https://kubernetes.default.svc" }
          ]
        }
      }]
      template = {
        metadata = { name = "istio" }
        spec = {
          project = "default"
          source = {
            repoURL        = "https://gitlab.com/minds/infrastructure/minds-istio-control-plane.git"
            path           = "./"
            targetRevision = "HEAD"
            # helm           = { valueFiles = ["values-sandbox.yaml"] }
          }
          destination = {
            server    = "{{url}}"
            namespace = "istio-system"
          }
        }
      }
    }
  }
}
