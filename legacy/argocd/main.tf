provider "aws" {
  region = "us-east-1"
}

provider "aws" {
  alias  = "eks"
  region = "us-east-1"

  assume_role {
    role_arn     = "arn:aws:iam::${local.this_account}:role/${var.eks_assume_role_name[terraform.workspace]}"
    session_name = "argocd-terraform"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>3.75.1"
    }
  }
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "argocd/terraform.tfstate"
  }
}

data "aws_eks_cluster" "cluster" {
  name = var.eks_cluster_name[terraform.workspace]
}

data "aws_eks_cluster_auth" "cluster" {
  provider = aws.eks
  name     = var.eks_cluster_name[terraform.workspace]
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    token                  = data.aws_eks_cluster_auth.cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  }
}

# For retrieving information about the identity applying this workspace
data "aws_caller_identity" "this" {}
locals { this_account = data.aws_caller_identity.this.account_id }
