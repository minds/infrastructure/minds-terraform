# Minds Matrix Deployment

This is the Minds.com matrix deployment.

## Workspace

The `default` workspace is the production environment.

## Deploying

```
terraform apply
```

### Specifying versions

```
terraform apply --var matrix_riot_version=291107005
```


## Disaster recovery runbook

### Start a temporal pod

`kubectl -n postgresql run -i --tty restore-psql --image=minds/postgresql-backup --restart=Never -- sh`

### Setup a service account

- Get keys from kubernetes secret `kubectl -n matrix edit secret/matrix-backups-gcp-service-account`
- Copy in to restore-psql pod we made above
- `vi gcreds.json.base64` paste your secret in this file
- `(cat gcreds.json.base64 | base64 -d) > google-cloud-credentials.json`
- `gcloud auth activate-service-account --key-file google-cloud-credentials.json`

### Download the backup

```
filename="2021-07-26_00-00-17.sql"
gsutil cp gs://minds-prod-matrix-backups/$filename /tmp/$filename
```

### Restore the backup

```
The postgre password can be found in in the `env` variables
```

#### Setup the user and database

```
psql -U postgres

CREATE USER matrix WITH PASSWORD 'temp'; // So production can not connect

CREATE DATABASE matrix
 ENCODING 'UTF8'
 LC_COLLATE='C'
 LC_CTYPE='C'
 template=template0
 OWNER matrix;

exit;
```

#### Restore the data

```
psql -h postgresql-postgresql-ha-pgpool.postgresql.svc.cluster.local -U postgres matrix < /tmp/$filename
```

#### Set the password bac
```
psql -U postgres

ALTER USER matrix  WITH PASSWORD 'somethingtopsecret'
```