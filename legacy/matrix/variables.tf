##

variable "gcp_project_id" {
  type        = string
  description = "The name of gcp project"
}

variable "gcp_region" {
  type        = string
  description = "The region of the gcp project"
}

variable "gke_cluster_name" {
  type        = string
  description = "The name of the GKE cluster"
}

##

variable "matrix_sso_secret" {
  type      = string
  sensitive = true
}

variable "matrix_sygnal_apns_cert" {
  type        = string
  sensitive   = true
  description = "Base64 encoded pem of the apns certificate"
}

variable "matrix_signing_key" {
  type        = string
  sensitive   = true
  description = "The signing key to use for synapse"
}

##

variable "matrix_synapse_version" {
  type    = string
  default = "v1.32.2"
}

variable "matrix_riot_version" {
  type    = string
  default = "290638986"
}

variable "matrix_sygnal_version" {
  type    = string
  default = "v0.9.2"
}