resource "kubernetes_namespace" "matrix" {
  metadata {
    annotations = {
      name = "matrix"
    }

    labels = {
      name = "matrix"
    }

    name = "matrix"
  }
}

##
# Todo: move this to ../../modules directory
##
resource "helm_release" "matrix" {
  name      = "matrix"
  namespace = kubernetes_namespace.matrix.metadata[0].name

  repository = "../../../"
  chart      = "matrix-chart"

  ################################################
  ## Matrix ######################################
  ################################################

  ##
  # This is what the suffix of usernames with be
  # eg. @mark:minds.com
  ##
  set {
    name  = "matrix.serverName"
    value = "minds.com"
  }

  # set_sensitive {
  #   name  = "matrix.registration.sharedSecret"
  #   value = base64encode(random_password.matrix_shared_registration_secret.result)
  # }

  set {
    name  = "matrix.homeserverExtra.oidc_config.enabled"
    value = true
  }

  set {
    name  = "matrix.homeserverExtra.oidc_config.issuer"
    value = "https://www.minds.com/"
  }

  set {
    name  = "matrix.homeserverExtra.oidc_config.client_id"
    value = "matrix"
  }

  set_sensitive {
    name  = "matrix.homeserverExtra.oidc_config.client_secret"
    value = var.matrix_sso_secret
  }

  set {
    name  = "matrix.homeserverExtra.oidc_config.scopes[0]"
    value = "openid"
  }

  set {
    name  = "matrix.homeserverExtra.oidc_config.allow_existing_users"
    value = true
  }

  set {
    name  = "matrix.homeserverExtra.oidc_config.user_mapping_provider.config.subject_claim"
    value = "sub"
  }

  set {
    name  = "matrix.homeserverExtra.oidc_config.user_mapping_provider.config.localpart_template"
    value = "\\{\\{ user.username \\}\\}"
    type  = "string"
  }

  set {
    name  = "matrix.homeserverExtra.oidc_config.user_mapping_provider.config.display_name_template"
    value = "\\{\\{ user.name \\}\\}"
  }

  set {
    name  = "matrix.homeserverExtra.enable_set_avatar_url"
    value = true
  }

  set {
    name  = "matrix.urlPreviews.enabled"
    value = true
  }

  set {
    name  = "matrix.sso.client_whitelist[0]"
    value = "https://chat.minds.com"
  }

  set {
    name  = "matrix.sso.client_whitelist[1]"
    value = "element://connect"
  }

  # set {
  #   name = "matrix.autoJoinRooms[0]"
  #   value = "#canary:minds.com"
  # }

  ################################################
  ## Synapse #####################################
  ################################################

  set {
    name  = "synapse.image.tag"
    value = var.matrix_synapse_version
  }

  set {
    name  = "synapse.service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "synapse.service.federation.type"
    value = "LoadBalancer"
  }

  set {
    name  = "synapse.pushIncludeContent"
    value = false
  }

  set {
    name  = "synapse.signingKey"
    value = var.matrix_signing_key
  }

  set {
    name  = "synapse.resources.limits.cpu"
    value = 2
  }

  set {
    name  = "synapse.resources.requests.cpu"
    value = 2
  }

  set {
    name  = "synapse.resources.limits.memory"
    value = "6Gi"
  }

  set {
    name  = "synapse.resources.requests.memory"
    value = "6Gi"
  }

  set {
    name  = "synapse.probes.liveness.timeoutSeconds"
    value = 10
  }

  set {
    name  = "synapse.probes.liveness.periodSeconds"
    value = 3
  }

  ################################################
  ## Workers     #################################
  ################################################

  set {
    name  = "synapse.workers.enabled"
    value = true
  }

  set {
    name  = "synapse.workers.replicaCount"
    value = 1
  }

  set {
    name  = "synapse.redis.host"
    value = "redis-master.caching.svc.cluster.local"
  }

  set {
    name  = "synapse.workers.redeploy"
    value = 9
  }

  set {
    name  = "synapse.workers.resources.limits.cpu"
    value = 1
  }

  set {
    name  = "synapse.workers.resources.requests.cpu"
    value = 1
  }

  set {
    name  = "synapse.workers.resources.limits.memory"
    value = "756Mi"
  }

  set {
    name  = "synapse.workers.resources.requests.memory"
    value = "512Mi"
  }

  ################################################
  ## Sygnal     ##################################
  ################################################

  set {
    name  = "sygnal.enabled"
    value = true
  }

  set {
    name  = "sygnal.image.repository"
    value = "matrixdotorg/sygnal"
  }

  set {
    name  = "sygnal.image.tag"
    value = var.matrix_sygnal_version
  }

  set {
    name  = "sygnal.resources.limits.cpu"
    value = 1
  }

  set {
    name  = "sygnal.resources.limits.memory"
    value = "2Gi"
  }

  set_sensitive {
    name  = "sygnal.apns.pem"
    value = var.matrix_sygnal_apns_cert
  }


  ################################################
  ## Postgresql ##################################
  ################################################

  set {
    name  = "postgresql.enabled" # we run out own postgresql ha cluster
    value = false
  }

  set {
    name  = "postgresql.hostname"
    value = "10.147.0.2" # TODO make this a variable
  }

  set {
    name  = "postgresql.password"
    value = random_string.matrix_postgresql.result
  }


  ################################################
  ## Element #####################################
  ################################################

  set {
    name  = "riot.service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "riot.image.repository"
    value = "registry.gitlab.com/minds/chat-web/chat-web"
  }

  set {
    name  = "riot.image.redploy"
    value = 1
  }

  set {
    name  = "riot.image.tag"
    value = var.matrix_riot_version
  }

  set {
    name  = "riot.branding.brand"
    value = "Minds"
  }

  set {
    name  = "riot.ogImageUrl"
    value = "https://cdn.minds.com/static/en/assets/og-images/default-v3.png"
  }

  set {
    name  = "riot.disable3pidLogin"
    value = true
  }

  set {
    name  = "riot.embeddedPages.loginForWelcome"
    value = true
  }

  set {
    name  = "riot.permalinkPrefix"
    value = "https://chat.minds.com"
  }

  set {
    name  = "riot.settingDefaults.UIFeature\\.communities"
    value = false
  }

  set {
    name  = "riot.settingDefaults.UIFeature\\.passwordReset"
    value = false
  }

  set {
    name  = "riot.settingDefaults.UIFeature\\.deactivate"
    value = false
  }

  set {
    name  = "riot.settingDefaults.UIFeature\\.registration"
    value = false
  }

  set {
    name  = "riot.settingDefaults.UIFeature\\.voip"
    value = true
  }

  set {
    name  = "riot.settingDefaults.UIFeature\\.urlPreviews"
    value = true
  }

  set {
    name  = "riot.settingDefaults.UIFeature\\.identityServer"
    value = false
  }

  ################################################
  ## Ingress #####################################
  ################################################

  set {
    name  = "ingress.enabled"
    value = true
  }

  set {
    name  = "ingress.federation"
    value = true
  }

  set {
    name  = "ingress.hosts.synapse"
    value = "synapse.matrix.minds.com"
  }

  set {
    name  = "ingress.hosts.riot"
    value = "chat.minds.com"
  }

  set {
    name  = "ingress.hosts.federation"
    value = "federation.matrix.minds.com"
  }

  set {
    name  = "ingress.hosts.sygnal"
    value = "sygnal.matrix.minds.com"
  }

  set {
    name  = "ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "gce"
    type  = "string"
  }

  set {
    name  = "ingress.annotations.kubernetes\\.io/ingress\\.global-static-ip-name"
    value = google_compute_global_address.matrix_ingress_address.name
    type  = "string"
  }

  set {
    name  = "ingress.annotations.ingress\\.gcp\\.kubernetes\\.io/pre-shared-cert"
    value = google_compute_managed_ssl_certificate.matrix_ingress_cert.name
  }

  ################################################
  ## Storage #####################################
  ################################################

  set {
    name  = "volumes.signingKey.storageClass"
    value = kubernetes_storage_class.matrix_ssd.metadata[0].name
  }

  set {
    name  = "volumes.media.storageClass"
    value = kubernetes_storage_class.matrix_ssd.metadata[0].name
  }

  set {
    name  = "volumes.media.capacity"
    value = "256Gi"
  }

  ################################################
  ## Turn   ######################################
  ################################################

  set {
    name  = "coturn.enabled"
    value = true
  }

  set {
    name  = "coturn.kind"
    value = "Deployment"
  }

  set {
    name  = "coturn.service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "coturn.uris[0]"
    value = "turn:turn.matrix.org?transport=udp" # TODO: Change to minds.com
  }

  set {
    name  = "coturn.replicaCount"
    value = 0 # TODO: Increase when our coturn servers work well
  }

  ################################################
  ## Others ######################################
  ################################################
  set {
    name  = "mail.enabled"
    value = false
  }

  set {
    name  = "mail.relay.enabled"
    value = false
  }
}

##
# Secrets
##

resource "random_string" "matrix_postgresql" {
  length           = 16
  special          = true
  override_special = "/@£$"

  lifecycle {
    ignore_changes = all
  }
}

resource "random_string" "matrix_postgresql_replication" {
  length           = 16
  special          = true
  override_special = "/@£$"

  lifecycle {
    ignore_changes = all
  }
}

resource "random_password" "matrix_shared_registration_secret" {
  length  = 512
  special = true

  lifecycle {
    ignore_changes = all
  }
}

##
# Ingress
# Set your DNS configuration to this
##

resource "google_compute_managed_ssl_certificate" "matrix_ingress_cert" {
  provider = google-beta

  name = "matrix-ingress-ssl-cert"

  managed {
    domains = ["chat.minds.com", "synapse.matrix.minds.com", "federation.matrix.minds.com", "sygnal.matrix.minds.com"]
  }
}

resource "google_compute_global_address" "matrix_ingress_address" {
  name = "matrix-static-ingress"
}

output "matrix_ingress_ip_address" {
  value = google_compute_global_address.matrix_ingress_address.address
}

# Coturn IP

resource "google_compute_global_address" "coturn_ingress_address" {
  name = "coturn-static-ingress"
}

output "coturn_ingress_ip_address" {
  value = google_compute_global_address.coturn_ingress_address.address
}