gcp_project_id   = "minds-production"
gcp_region       = "us-east1"
gke_cluster_name = "minds-production-gke"

matrix_synapse_version = "v1.49.2"
matrix_riot_version    = "442623207"