
terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "environments/matrix/terraform.tfstate"
  }
}

##
# Import our kubernetes-gke platform module
##
module "k8s_gke" {
  source = "../../modules/k8s-gke"

  gcp_project_id       = var.gcp_project_id
  gcp_region           = var.gcp_region
  gke_cluster_name     = var.gke_cluster_name
  gke_cluster_location = var.gcp_region
}

provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

provider "kubernetes" {
  host                   = module.k8s_gke.k8s_host
  token                  = module.k8s_gke.k8s_token
  cluster_ca_certificate = module.k8s_gke.k8s_cert
}

provider "helm" {
  kubernetes {
    host                   = module.k8s_gke.k8s_host
    token                  = module.k8s_gke.k8s_token
    cluster_ca_certificate = module.k8s_gke.k8s_cert
  }
}