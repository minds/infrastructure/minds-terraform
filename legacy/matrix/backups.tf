##
# Make a service account for the backups
##
resource "google_service_account" "matrix_backups" {
  account_id   = "minds-prod-matrix-backups"
  display_name = "Minds Production Matrix Backups"
}

# Get the service account private key
resource "google_service_account_key" "matrix_backups" {
  service_account_id = google_service_account.matrix_backups.name
}

# Make this into k8s secrets we will later mount
resource "kubernetes_secret" "matrix_backups" {
  metadata {
    name      = "matrix-backups-gcp-service-account"
    namespace = kubernetes_namespace.matrix.metadata[0].name
  }
  data = {
    "credentials.json" = base64decode(google_service_account_key.matrix_backups.private_key)
  }
}

##
# Create the gcs bucket
##
resource "google_storage_bucket" "matrix_backups" {
  name          = "minds-prod-matrix-backups"
  location      = "US"
  force_destroy = true
}

# Give permission to our service account
resource "google_storage_bucket_iam_member" "matrix_backups" {
  bucket = google_storage_bucket.matrix_backups.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.matrix_backups.email}"
}

##
# Create the cronjob
##
resource "kubernetes_cron_job" "gke_postgresql_backup" {
  metadata {
    name      = "matrix-backup-postgres"
    namespace = kubernetes_namespace.matrix.metadata[0].name
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 3
    schedule                      = "0 */3 * * *" # every 3 hours
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 1

    job_template {
      metadata {}
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 10
        template {
          metadata {}
          spec {
            service_account_name = null
            volume {
              name = "google-cloud-key"
              secret {
                secret_name = kubernetes_secret.matrix_backups.metadata[0].name
                items {
                  key  = "credentials.json"
                  path = "credentials.json"
                }
              }
            }
            container {
              name  = "postgres"
              image = "minds/postgresql-backup:latest"
              command = [
                "/bin/sh",
                "-c",
                <<EOT
# Activate the service account for gsutil
gcloud auth activate-service-account --key-file /var/secrets/google/credentials.json

filename=$(date '+%Y-%m-%d_%H-%M-%S')

# Do the backup
echo "Starting backup to /tmp/$filename.sql"
pg_dump -h postgresql-postgresql-ha-pgpool.postgresql.svc.cluster.local -U matrix matrix > /tmp/$filename.sql

# Upload to gcs bucket
echo "Uploading backup to gcs bucket"
gsutil cp /tmp/$filename.sql gs://${google_storage_bucket.matrix_backups.name}/$filename.sql

echo "Completed"
EOT
              ]
              volume_mount {
                name       = "google-cloud-key"
                mount_path = "/var/secrets/google"
              }
              env {
                name  = "PGPASSWORD"
                value = random_string.matrix_postgresql.result
              }
              env {
                name  = "GOOGLE_APPLICATION_CREDENTIALS"
                value = "/var/secrets/google/credentials.json"
              }
            }
          }
        }
      }
    }

  }
}
