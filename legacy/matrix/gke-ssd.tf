resource "kubernetes_storage_class" "matrix_ssd" {
  metadata {
    name = "storage-ssd-wait"
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  reclaim_policy      = "Retain"
  parameters = {
    type = "pd-ssd"
  }
  volume_binding_mode = "WaitForFirstConsumer"
}