
// Streams

resource "aws_kinesis_stream" "snowplow_raw_good" {
  name             = "snowplow-raw-good"
  shard_count      = 1
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_stream" "snowplow_raw_bad" {
  name             = "snowplow-raw-bad"
  shard_count      = 1
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_stream" "snowplow_enriched_bad" {
  name             = "snowplow-enriched-bad"
  shard_count      = 1
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_stream" "snowplow_enriched_good" {
  name             = "snowplow-enriched-good"
  shard_count      = 1
  retention_period = 48

  shard_level_metrics = [
    "IncomingBytes",
    "OutgoingBytes",
  ]

  tags = {
    environment = "SnowPlow"
  }
}

// Lambda Function
data "archive_file" "snowplow_lambda_event_formatter_archive" {
  type        = "zip"
  source_file = "${path.module}/lambda/lambda_function.py"
  output_path = "${path.module}/lambda/lambda_function_payload.zip"
}

resource "aws_lambda_function" "snowplow_event_formatter_lambda_function" {
  description   = "This adds a newline to the end of each record."
  function_name = "SnowPlowFirehoseFormatter"
  handler       = "lambda_function.lambda_handler"
  role          = aws_iam_role.snowplow_lambda_role.arn
  runtime       = "python2.7"

  filename         = "lambda/lambda_function_payload.zip"
  source_code_hash = filebase64sha256("lambda/lambda_function_payload.zip")
  timeout          = "60"

  tags = {
    environment                = "SnowPlow"
    "lambda-console:blueprint" = "kinesis-firehose-process-record-python"
  }
}

// Firehose

resource "aws_kinesis_firehose_delivery_stream" "snowplow_enriched_bad_firehose" {
  destination = "extended_s3"
  name        = "SnowPlowEnrichedBad"

  kinesis_source_configuration {
    kinesis_stream_arn = aws_kinesis_stream.snowplow_enriched_bad.arn
    role_arn           = aws_iam_role.snowplow_firehose_delivery_role.arn
  }

  extended_s3_configuration {
    bucket_arn          = aws_s3_bucket.snowplow_s3_bucket.arn
    role_arn            = aws_iam_role.snowplow_firehose_delivery_role.arn
    compression_format  = "GZIP"
    prefix              = "snowplow/enriched-bad/"
    error_output_prefix = "snowplow/enriched-bad/"
    s3_backup_mode      = "Disabled"

    processing_configuration {
      enabled = "true"

      processors {
        type = "Lambda"

        parameters {
          parameter_name  = "LambdaArn"
          parameter_value = "${aws_lambda_function.snowplow_event_formatter_lambda_function.arn}:$LATEST"
        }
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_firehose_delivery_stream" "snowplow_enriched_good_firehose" {
  destination = "extended_s3"
  name        = "SnowPlowEnrichedGood"

  kinesis_source_configuration {
    kinesis_stream_arn = aws_kinesis_stream.snowplow_enriched_good.arn
    role_arn           = aws_iam_role.snowplow_firehose_delivery_role.arn
  }

  extended_s3_configuration {
    bucket_arn          = aws_s3_bucket.snowplow_s3_bucket.arn
    role_arn            = aws_iam_role.snowplow_firehose_delivery_role.arn
    compression_format  = "GZIP"
    prefix              = "snowplow/enriched/good/date=!{timestamp:yyyy}-!{timestamp:MM}-!{timestamp:dd}/"
    error_output_prefix = "snowplow/enriched-good/"
    s3_backup_mode      = "Disabled"
    buffer_size         = 64
    buffer_interval     = 60

    processing_configuration {
      enabled = "true"

      processors {
        type = "Lambda"

        parameters {
          parameter_name  = "LambdaArn"
          parameter_value = "${aws_lambda_function.snowplow_event_formatter_lambda_function.arn}:$LATEST"
        }
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_kinesis_firehose_delivery_stream" "snowplow_raw_bad_firehose" {
  destination = "extended_s3"
  name        = "SnowPlowRawBad"

  kinesis_source_configuration {
    kinesis_stream_arn = aws_kinesis_stream.snowplow_raw_bad.arn
    role_arn           = aws_iam_role.snowplow_firehose_delivery_role.arn
  }

  extended_s3_configuration {
    bucket_arn          = aws_s3_bucket.snowplow_s3_bucket.arn
    role_arn            = aws_iam_role.snowplow_firehose_delivery_role.arn
    compression_format  = "GZIP"
    prefix              = "snowplow/raw-bad/"
    error_output_prefix = "snowplow/raw-bad/"
    s3_backup_mode      = "Disabled"

    processing_configuration {
      enabled = "true"

      processors {
        type = "Lambda"

        parameters {
          parameter_name  = "LambdaArn"
          parameter_value = "${aws_lambda_function.snowplow_event_formatter_lambda_function.arn}:$LATEST"
        }
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}
