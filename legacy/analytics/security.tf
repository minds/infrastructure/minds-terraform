## K8S requires a service account in order for pods to talk to kinesis

resource "kubernetes_service_account" "snowplow-kinesis-collector" {
  provider = kubernetes.eks
  metadata {
    name = "snowplow-kinesis-collector"
    annotations = {
      "eks.amazonaws.com/role-arn" : aws_iam_role.snowplow_collector_role.arn
    }
  }
}


resource "kubernetes_service_account" "snowplow-kinesis-enricher" {
  provider = kubernetes.eks
  metadata {
    name = "snowplow-kinesis-enricher"
    annotations = {
      "eks.amazonaws.com/role-arn" : aws_iam_role.snowplow_enricher_role.arn
    }
  }
}

