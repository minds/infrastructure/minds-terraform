CREATE EXTERNAL TABLE `es_metrics`(
  `user_guid` string COMMENT 'from deserializer', 
  `tags` array<string> COMMENT 'from deserializer', 
  `type` string COMMENT 'from deserializer', 
  `action` string COMMENT 'from deserializer', 
  `product` string COMMENT 'from deserializer', 
  `user_phone_number_hash` string COMMENT 'from deserializer', 
  `@version` string COMMENT 'from deserializer', 
  `platform` string COMMENT 'from deserializer', 
  `@timestamp` string COMMENT 'from deserializer', 
  `ip_hash` string COMMENT 'from deserializer', 
  `user_agent` string COMMENT 'from deserializer', 
  `mobile_version` string COMMENT 'from deserializer', 
  `ip_range_hash` string COMMENT 'from deserializer', 
  `cookie_id` string COMMENT 'from deserializer', 
  `route_uri` string COMMENT 'from deserializer', 
  `logged_in` boolean COMMENT 'from deserializer', 
  `entity_type` string COMMENT 'from deserializer', 
  `entity_membership` int COMMENT 'from deserializer', 
  `entity_guid` string COMMENT 'from deserializer', 
  `entity_subtype` string COMMENT 'from deserializer', 
  `entity_owner_guid` string COMMENT 'from deserializer', 
  `entity_container_guid` string COMMENT 'from deserializer', 
  `entity_access_id` string COMMENT 'from deserializer', 
  `is_remind` boolean COMMENT 'from deserializer', 
  `ban_reason` string COMMENT 'from deserializer', 
  `referrer_uri` string COMMENT 'from deserializer', 
  `comment_guid` string COMMENT 'from deserializer', 
  `email_topic` string COMMENT 'from deserializer', 
  `email_state` string COMMENT 'from deserializer', 
  `email_campaign` string COMMENT 'from deserializer', 
  `ratelimit_key` string COMMENT 'from deserializer', 
  `ratelimit_period` int COMMENT 'from deserializer', 
  `boost_quality` int COMMENT 'from deserializer', 
  `boost_type` string COMMENT 'from deserializer', 
  `boost_rating` int COMMENT 'from deserializer', 
  `boost_entity_guid` string COMMENT 'from deserializer', 
  `referrer_guid` string COMMENT 'from deserializer', 
  `boost_reject_reason` int COMMENT 'from deserializer', 
  `pro_referrer` boolean COMMENT 'from deserializer')
PARTITIONED BY ( 
  `year` int, 
  `month` int, 
  `date` date)
ROW FORMAT SERDE 
  'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
  'paths'='@timestamp,@version,action,ban_reason,boost_entity_guid,boost_quality,boost_rating,boost_reject_reason,boost_type,comment_guid,cookie_id,email_campaign,email_state,email_topic,entity_access_id,entity_container_guid,entity_guid,entity_membership,entity_owner_guid,entity_subtype,entity_type,ip_hash,ip_range_hash,is_remind,logged_in,mobile_version,platform,pro_referrer,product,ratelimit_key,ratelimit_period,referrer_guid,referrer_uri,route_uri,tags,type,user_agent,user_guid,user_phone_number_hash') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://minds-analytics/es-metrics/'
TBLPROPERTIES (
  'classification'='json', 
  'compressionType'='gzip',
  'typeOfData'='file')