CREATE EXTERNAL TABLE `es_offchain`(
  `@timestamp` string COMMENT 'from deserializer', 
  `amount` double COMMENT 'from deserializer', 
  `tx` string COMMENT 'from deserializer', 
  `user_guid` bigint COMMENT 'from deserializer', 
  `wire_receiver_guid` bigint COMMENT 'from deserializer', 
  `wire_sender_guid` bigint COMMENT 'from deserializer', 
  `wire_entity_guid` bigint COMMENT 'from deserializer', 
  `contract` string COMMENT 'from deserializer', 
  `tags` string COMMENT 'from deserializer', 
  `boost_guid` bigint COMMENT 'from deserializer', 
  `boost_handler` string COMMENT 'from deserializer')
PARTITIONED BY ( 
  `year` int, 
  `month` int, 
  `date` date)
ROW FORMAT SERDE 
  'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
  'paths'='@timestamp,amount,tx,user_guid,wire_receiver_guid,wire_sender_guid,wire_entity_guid,contract,tags,boost_guid,boost_handler') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://minds-analytics/es-offchain'
TBLPROPERTIES (
  'classification'='json', 
  'compressionType'='gzip', 
  'transient_lastDdlTime'='1593683727', 
  'typeOfData'='file')