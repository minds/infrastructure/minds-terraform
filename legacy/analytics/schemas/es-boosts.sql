CREATE EXTERNAL TABLE `es_boosts`(
  `impressions` bigint COMMENT 'from deserializer', 
  `bid_type` string COMMENT 'from deserializer', 
  `owner_guid` string COMMENT 'from deserializer', 
  `@version` string COMMENT 'from deserializer', 
  `@completed` double COMMENT 'from deserializer', 
  `tags` array<string> COMMENT 'from deserializer', 
  `entity_guid` string COMMENT 'from deserializer', 
  `@timestamp` string COMMENT 'from deserializer', 
  `token_method` string COMMENT 'from deserializer', 
  `bid` string COMMENT 'from deserializer', 
  `type` string COMMENT 'from deserializer', 
  `@reviewed` double COMMENT 'from deserializer', 
  `boost_type` string COMMENT 'from deserializer', 
  `priority` boolean COMMENT 'from deserializer', 
  `rating` int COMMENT 'from deserializer', 
  `impressions_met` int COMMENT 'from deserializer', 
  `@rejected` double COMMENT 'from deserializer', 
  `@revoked` double COMMENT 'from deserializer')
PARTITIONED BY ( 
  `partition_0` string, 
  `partition_1` string, 
  `date` date)
ROW FORMAT SERDE 
  'org.openx.data.jsonserde.JsonSerDe' 
WITH SERDEPROPERTIES ( 
  'paths'='@completed,@rejected,@reviewed,@revoked,@timestamp,@version,bid,bid_type,boost_type,entity_guid,impressions,impressions_met,owner_guid,priority,rating,tags,token_method,type') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  's3://minds-analytics/es-boosts/'
TBLPROPERTIES (
  'classification'='json', 
  'compressionType'='gzip', 
  'typeOfData'='file')