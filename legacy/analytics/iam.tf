// Lambda
resource "aws_iam_role" "snowplow_lambda_role" {
  assume_role_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Principal":{
        "Service":"lambda.amazonaws.com"
        },
      "Action":"sts:AssumeRole"
    }
  ]
}
EOF
  path               = "/service-role/"
  tags = {
    environment = "SnowPlow"
  }
}

resource "aws_iam_policy" "snowplow_lambda_policy" {
  description = ""
  path        = "/service-role/"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "logs:CreateLogGroup",
            "Resource": "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/SnowPlowFirehoseFormatter:*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_role_policy_attachment" {
  role       = aws_iam_role.snowplow_lambda_role.name
  policy_arn = aws_iam_policy.snowplow_lambda_policy.arn
}

//// Roles

// Firehose

resource "aws_iam_role" "snowplow_firehose_delivery_role" {
  assume_role_policy = <<EOF
{
  "Version" : "2012-10-17",
  "Statement" : [
    {
      "Sid" : "",
      "Effect" : "Allow",
      "Principal" : {
        "Service" : "firehose.amazonaws.com"
      },
      "Action" : "sts:AssumeRole",
      "Condition" : {
        "StringEquals" : {
          "sts:ExternalId":"${data.aws_caller_identity.current.account_id}"
        }
      }
    }
  ]
}
EOF
  path               = "/"
  tags = {
    environment = "SnowPlow"
  }
}

// Enricher Role

resource "aws_iam_role" "snowplow_enricher_role" {
  assume_role_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::324044571751:oidc-provider/oidc.eks.us-east-1.amazonaws.com/id/FD41EC693A70483DD05CE94CB241A56A"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "oidc.eks.us-east-1.amazonaws.com/id/FD41EC693A70483DD05CE94CB241A56A:sub": "system:serviceaccount:default:snowplow-kinesis-enricher"
        }
      }
    }
  ]
}
EOF
  path               = "/"
  tags = {
    environment = "SnowPlow"
  }
}

// Collector Roles

resource "aws_iam_role" "snowplow_collector_role" {
  assume_role_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::324044571751:oidc-provider/oidc.eks.us-east-1.amazonaws.com/id/FD41EC693A70483DD05CE94CB241A56A"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "oidc.eks.us-east-1.amazonaws.com/id/FD41EC693A70483DD05CE94CB241A56A:sub": "system:serviceaccount:default:snowplow-kinesis-collector"
        }
      }
    }
  ]
}
EOF
  path               = "/"
  tags = {
    environment = "SnowPlow"
  }
}

//// Policies

// Firehose glue

resource "aws_iam_role_policy" "snowplow_firehose_glue_policy" {
  name   = "firehose_glue"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "glue:GetTable",
                "glue:GetTableVersion",
                "glue:GetTableVersions"
            ],
            "Resource": "*"
        }
    ]
}
EOF
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

// Firehose Lambda

resource "aws_iam_role_policy" "snowplow_firehose_lamda_invoke_policy" {
  name   = "firehose_lambda_invoke"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "lambda:InvokeFunction",
                "lambda:GetFunctionConfiguration"
            ],
            "Resource": "${aws_lambda_function.snowplow_event_formatter_lambda_function.arn}:$LATEST"
        }
    ]
}
EOF
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

resource "aws_iam_role_policy" "snowplow_firehose_s3_policy" {
  name   = "firehose_s3"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:GetBucketLocation",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads",
                "s3:PutObject"
            ],
            "Resource": [
                "${aws_s3_bucket.snowplow_s3_bucket.arn}",
                "${aws_s3_bucket.snowplow_s3_bucket.arn}/*",
                "arn:aws:s3:::%FIREHOSE_BUCKET_NAME%",
                "arn:aws:s3:::%FIREHOSE_BUCKET_NAME%/*"
            ]
        }
    ]
}
EOF
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

resource "aws_iam_role_policy" "snowplow_firehose_kinesis_policy" {
  name   = "firehose_kinesis"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "kinesis:DescribeStream",
                "kinesis:GetShardIterator",
                "kinesis:GetRecords"
            ],
            "Resource": [
                "${aws_kinesis_stream.snowplow_enriched_good.arn}",
                "${aws_kinesis_stream.snowplow_enriched_bad.arn}",
                "${aws_kinesis_stream.snowplow_raw_bad.arn}"
            ]
        }
    ]
}
EOF
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

resource "aws_iam_role_policy" "snowplow_firehose_logs_policy" {
  name   = "firehose_logs"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/kinesisfirehose/SnowPlowEnrichedGood:log-stream:*"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/kinesisfirehose/SnowPlowEnrichedBad:log-stream:*"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:us-east-1:${data.aws_caller_identity.current.account_id}:log-group:/aws/kinesisfirehose/SnowPlowRawBad:log-stream:*"
            ]
        }
    ]
}
EOF
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}

resource "aws_iam_role_policy" "snowplow_firehose_kms_policy" {
  name   = "firehose_kms"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt"
            ],
            "Resource": [
                "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:key/%SSE_KEY_ID%"
            ],
            "Condition": {
                "StringEquals": {
                    "kms:ViaService": "kinesis.us-east-1.amazonaws.com"
                },
                "StringLike": {
                    "kms:EncryptionContext:aws:kinesis:arn": "${aws_kinesis_stream.snowplow_enriched_good.arn}"
                }
            }
        },
        {
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt"
            ],
            "Resource": [
                "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:key/%SSE_KEY_ID%"
            ],
            "Condition": {
                "StringEquals": {
                    "kms:ViaService": "kinesis.us-east-1.amazonaws.com"
                },
                "StringLike": {
                    "kms:EncryptionContext:aws:kinesis:arn": "${aws_kinesis_stream.snowplow_enriched_bad.arn}"
                }
            }
        },
        {
            "Effect": "Allow",
            "Action": [
                "kms:Decrypt"
            ],
            "Resource": [
                "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:key/%SSE_KEY_ID%"
            ],
            "Condition": {
                "StringEquals": {
                    "kms:ViaService": "kinesis.us-east-1.amazonaws.com"
                },
                "StringLike": {
                    "kms:EncryptionContext:aws:kinesis:arn": "${aws_kinesis_stream.snowplow_raw_bad.arn}"
                }
            }
        }
    ]
}
EOF
  role   = aws_iam_role.snowplow_firehose_delivery_role.id
}


resource "aws_iam_role_policy_attachment" "snowplow_collector_role_policy_attachment" {
  role       = aws_iam_role.snowplow_collector_role.name
  policy_arn = aws_iam_policy.snowplow_collector_policy.arn
}

resource "aws_iam_role_policy_attachment" "snowplow_enricher_role_policy_attachment" {
  role       = aws_iam_role.snowplow_enricher_role.name
  policy_arn = aws_iam_policy.snowplow_enricher_policy.arn
}


resource "aws_iam_policy" "snowplow_collector_policy" {
  description = "Policy the allows the collector to access other AWS services such as Kinesis."
  name        = "snowplow-collector-policy"
  path        = "/"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "kinesis:*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_policy" "snowplow_enricher_policy" {
  description = ""
  name        = "snowplow-enricher-policy"
  path        = "/"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "dynamodb:*"
            ],
            "Resource": [
                "arn:aws:dynamodb:us-east-1:*:table/SnowplowEnrich-minds-us-east-1"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "kinesis:*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}
