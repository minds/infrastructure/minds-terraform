resource "aws_kinesis_stream_consumer" "snowplow_rdb_shredder" {
  name       = "snowplow-rdb-shredder"
  stream_arn = aws_kinesis_stream.snowplow_enriched_good.arn
}

resource "aws_iam_role" "snowplow_rdb_loader_role" {
  name               = "snowplow-rdb-loader"
  assume_role_policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::324044571751:oidc-provider/oidc.eks.us-east-1.amazonaws.com/id/FD41EC693A70483DD05CE94CB241A56A"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "oidc.eks.us-east-1.amazonaws.com/id/FD41EC693A70483DD05CE94CB241A56A:sub": "system:serviceaccount:snowplow:snowplow-rdb-loader"
        }
      }
    }
  ]
}
EOF
  path               = "/"
  tags = {
    environment = "SnowPlow"
  }
}


resource "aws_iam_role_policy_attachment" "snowplow_rdb_loader_role_policy_attachment" {
  role       = aws_iam_role.snowplow_rdb_loader_role.name
  policy_arn = aws_iam_policy.snowplow_rdb_loader_policy.arn
}

resource "aws_iam_policy" "snowplow_rdb_loader_policy" {
  description = ""
  name        = "snowplow-rdb-loader-policy"
  path        = "/"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "kinesis:List*",
                "kinesis:Describe*",
                "kinesis:Subscribe*",
                "kinesis:Get*"
            ],
            "Resource": [
                "${aws_kinesis_stream.snowplow_enriched_good.arn}",
                "${aws_kinesis_stream.snowplow_enriched_good.arn}/consumer/snowplow-rdb-shredder:*",
                "${aws_kinesis_stream_consumer.snowplow_rdb_shredder.arn}",
                "*"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "dynamodb:CreateTable",
                "dynamodb:DescribeTable",
                "dynamodb:Scan",
                "dynamodb:UpdateItem",
                "dynamodb:Get*",
                "dynamodb:Put*"
            ],
            "Resource": [
                "arn:aws:dynamodb:us-east-1:*:table/snowplow-rdb-shredder"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "s3:Put*",
                "s3:AbortMultipartUpload"
            ],
            "Resource": [
                "${aws_s3_bucket.snowplow_s3_bucket.arn}/snowplow/*"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "s3:List*",
                "s3:Get*",
                "s3:Describe*"
            ],
            "Resource": [
                "${aws_s3_bucket.snowplow_s3_bucket.arn}"
            ]
        },
        {
            "Sid": "",
            "Effect": "Allow",
            "Action": [
                "sqs:*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "helm_release" "snowplow_rdb_loader" {
  provider  = helm.eks
  name      = "snowplow-rdb-loader"
  namespace = kubernetes_namespace.snowplow.metadata[0].name
  chart     = "../../../helm-charts/snowplow-rdb-loader"

  values = [
    <<YAML
streamShredder:
  config:
    streamType: "kinesis"
    streamName: "${aws_kinesis_stream.snowplow_enriched_good.name}"
    streamRegion: "us-east-1"
    streamPosition: "LATEST"

    outputPath: "s3://${aws_s3_bucket.snowplow_s3_bucket.bucket}/snowplow/stream-shredded/"
    outputCompression: "GZIP"
    outputRegion: "us-east-1"

    windowing: "10 minutes"

rdbLoader:
  config:
    storageType: "redshift"
    storageDatabase: "snowplow"
    storageSchema: "atomic"
    storagePort: 5439
    storageRegion: "us-east-1"

config:
  queueType: "sqs"
  queueName: "snowplow-dataflow-runner.fifo"
  queueRegion: "us-east-1"
  igluResolverUri: ${var.iglu_resolver_uri}
  igluResolverReadonlyApikey: ${var.iglu_resolver_readonly_apikey}

serviceAccount:
  annotations:
    eks.amazonaws.com/role-arn: ${aws_iam_role.snowplow_rdb_loader_role.arn}

secretKeyRef: snowplow-rdb-loader-secrets
YAML
  ]
}