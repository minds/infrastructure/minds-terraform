##
# We need a very small postgres server
##
resource "aws_db_instance" "snowplow_iglu_postgres" {
  allocated_storage    = 10
  engine               = "postgres"
  instance_class       = "db.t3.micro"
  name                 = "snowplowiglu"
  username             = "snowplow"
  password             = random_password.snowplow_iglu_postgress.result
  skip_final_snapshot  = true
  db_subnet_group_name = aws_db_subnet_group.snowplow_iglu_postgres.id
}

resource "aws_db_subnet_group" "snowplow_iglu_postgres" {
  name       = "snowplow_iglu_postgres_subnet"
  subnet_ids = data.aws_subnet_ids.vpc_subnets.ids

  tags = {
    Name = "SnowplowIglu Postgres"
  }
}

data "aws_subnet_ids" "vpc_subnets" {
  vpc_id = var.vpc_id
}

resource "random_password" "snowplow_iglu_postgress" {
  length           = 16
  special          = true
  override_special = "/@£$_%"

  lifecycle {
    ignore_changes = all
  }
}

resource "helm_release" "snowplow_iglu" {
  provider  = helm.eks
  name      = "snowplow-iglu"
  namespace = kubernetes_namespace.snowplow.metadata[0].name
  chart     = "../../../helm-charts/snowplow-iglu"

  set {
    name  = "database.type"
    value = "postgres"
  }

  set {
    name  = "database.host"
    value = aws_db_instance.snowplow_iglu_postgres.address
  }

  set {
    name  = "database.port"
    value = aws_db_instance.snowplow_iglu_postgres.port
  }

  set {
    name  = "database.username"
    value = aws_db_instance.snowplow_iglu_postgres.username
  }

  set_sensitive {
    name  = "database.password"
    value = random_password.snowplow_iglu_postgress.result
  }

  set {
    name  = "database.dbname"
    value = aws_db_instance.snowplow_iglu_postgres.name
  }

  set {
    name  = "install"
    value = false
  }

  ##
  # Ingress
  ##

  set {
    name  = "service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
    value = "alb"
  }

}