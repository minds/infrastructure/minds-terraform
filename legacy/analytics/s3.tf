// S3 Buckets
resource "aws_s3_bucket" "snowplow_s3_bucket" {
  bucket = var.s3_bucket

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    environment = "SnowPlow"
  }
}

# resource "aws_s3_bucket_policy" "snowplow_s3_bucket_policy" {
#   bucket = aws_s3_bucket.snowplow_s3_bucket.id

#   policy = data.template_file.snowplow_s3_bucket_policy.rendered
# }

# resource "aws_s3_bucket_notification" "snowplow_bucket_notifications" {
#   bucket = aws_s3_bucket.snowplow_s3_bucket.bucket

#   queue {
#     queue_arn     = "arn:aws:sqs:us-east-1:730570900080:sf-snowpipe-AIDAJ5LKTH5PBBNSL6UC2-14u9uWhgmVmEsNRfTA5B9w"
#     events        = ["s3:ObjectCreated:*"]
#     filter_prefix = ""
#   }
# }
