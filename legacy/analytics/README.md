# Minds Analytics

This terraform module deploys the Minds Analytics platforms, which consists of Snowplow and Superset

## Requirements

- EKS stack
- ElasticSearch module deployed

## Running

1. Select your workspace

`terraform workspace select production`

2. Apply

`terraform apply --var-file=production.tfvars`

## Iglu setup

See https://docs.snowplowanalytics.com/docs/pipeline-components-and-applications/iglu/setting-up-iglu/step-2-setup-an-iglu-repository/2-4-iglu-server/ for setting up the iglu server.

```
kubectl -n snowplow run -i --tty snowplow-psql --image=alpine --restart=Never -- sh

apk add postgreq-client

psql ...

CREATE EXTENSION "uuid-ossp";

...

INSERT INTO iglu_permissions
VALUES (uuid_generate_v4(), '', TRUE, 'CREATE_VENDOR'::schema_action, '{"CREATE", "DELETE"}'::key_action[]);

...

select * from iglu_permissions;
```