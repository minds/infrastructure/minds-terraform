variable "eks_cluster_name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "s3_bucket" {
  type = string
}

#

variable "iglu_resolver_uri" {
  type      = string
  sensitive = true
}

variable "iglu_resolver_readonly_apikey" {
  type      = string
  sensitive = true
}