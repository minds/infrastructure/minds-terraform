resource "aws_glue_catalog_database" "snowplow" {
  name = "snowplow"
}

resource "aws_glue_catalog_table" "snowplow_enriched_good" {
  database_name = aws_glue_catalog_database.snowplow.name
  name          = "enriched_good"
  retention     = "0"

  storage_descriptor {
    columns {
      name = "app_id"
      type = "string"
    }

    columns {
      name = "platform"
      type = "string"
    }

    columns {
      name = "etl_tstamp"
      type = "timestamp"
    }

    columns {
      name = "collector_tstamp"
      type = "timestamp"
    }

    columns {
      name = "dvce_created_tstamp"
      type = "timestamp"
    }

    columns {
      name = "event"
      type = "string"
    }

    columns {
      name = "event_id"
      type = "string"
    }

    columns {
      name = "txn_id"
      type = "int"
    }

    columns {
      name = "name_tracker"
      type = "string"
    }

    columns {
      name = "v_tracker"
      type = "string"
    }

    columns {
      name = "v_collector"
      type = "string"
    }

    columns {
      name = "v_etl"
      type = "string"
    }

    columns {
      name = "user_id"
      type = "string"
    }

    columns {
      name = "user_ipaddress"
      type = "string"
    }

    columns {
      name = "user_fingerprint"
      type = "string"
    }

    columns {
      name = "domain_userid"
      type = "string"
    }

    columns {
      name = "domain_sessionidx"
      type = "int"
    }

    columns {
      name = "network_userid"
      type = "string"
    }

    columns {
      name = "geo_country"
      type = "string"
    }

    columns {
      name = "geo_region"
      type = "string"
    }

    columns {
      name = "geo_city"
      type = "string"
    }

    columns {
      name = "geo_zipcode"
      type = "string"
    }

    columns {
      name = "geo_latitude"
      type = "double"
    }

    columns {
      name = "geo_longitude"
      type = "double"
    }

    columns {
      name = "geo_region_name"
      type = "string"
    }

    columns {
      name = "ip_isp"
      type = "string"
    }

    columns {
      name = "ip_organization"
      type = "string"
    }

    columns {
      name = "ip_domain"
      type = "string"
    }

    columns {
      name = "ip_netspeed"
      type = "string"
    }

    columns {
      name = "page_url"
      type = "string"
    }

    columns {
      name = "page_title"
      type = "string"
    }

    columns {
      name = "page_referrer"
      type = "string"
    }

    columns {
      name = "page_urlscheme"
      type = "string"
    }

    columns {
      name = "page_urlhost"
      type = "string"
    }

    columns {
      name = "page_urlport"
      type = "int"
    }

    columns {
      name = "page_urlpath"
      type = "string"
    }

    columns {
      name = "page_urlquery"
      type = "string"
    }

    columns {
      name = "page_urlfragment"
      type = "string"
    }

    columns {
      name = "refr_urlscheme"
      type = "string"
    }

    columns {
      name = "refr_urlhost"
      type = "string"
    }

    columns {
      name = "refr_urlport"
      type = "int"
    }

    columns {
      name = "refr_urlpath"
      type = "string"
    }

    columns {
      name = "refr_urlquery"
      type = "string"
    }

    columns {
      name = "refr_urlfragment"
      type = "string"
    }

    columns {
      name = "refr_medium"
      type = "string"
    }

    columns {
      name = "refr_source"
      type = "string"
    }

    columns {
      name = "refr_term"
      type = "string"
    }

    columns {
      name = "mkt_medium"
      type = "string"
    }

    columns {
      name = "mkt_source"
      type = "string"
    }

    columns {
      name = "mkt_term"
      type = "string"
    }

    columns {
      name = "mkt_content"
      type = "string"
    }

    columns {
      name = "mkt_campaign"
      type = "string"
    }

    columns {
      name = "contexts"
      type = "string"
    }

    columns {
      name = "se_category"
      type = "string"
    }

    columns {
      name = "se_action"
      type = "string"
    }

    columns {
      name = "se_label"
      type = "string"
    }

    columns {
      name = "se_property"
      type = "string"
    }

    columns {
      name = "se_value"
      type = "double"
    }

    columns {
      name = "unstruct_event"
      type = "string"
    }

    columns {
      name = "tr_orderid"
      type = "string"
    }

    columns {
      name = "tr_affiliation"
      type = "string"
    }

    columns {
      name = "tr_total"
      type = "double"
    }

    columns {
      name = "tr_tax"
      type = "double"
    }

    columns {
      name = "tr_shipping"
      type = "double"
    }

    columns {
      name = "tr_city"
      type = "string"
    }

    columns {
      name = "tr_state"
      type = "string"
    }

    columns {
      name = "tr_country"
      type = "string"
    }

    columns {
      name = "ti_orderid"
      type = "string"
    }

    columns {
      name = "ti_sku"
      type = "string"
    }

    columns {
      name = "ti_name"
      type = "string"
    }

    columns {
      name = "ti_category"
      type = "string"
    }

    columns {
      name = "ti_price"
      type = "double"
    }

    columns {
      name = "ti_quantity"
      type = "int"
    }

    columns {
      name = "pp_xoffset_min"
      type = "int"
    }

    columns {
      name = "pp_xoffset_max"
      type = "int"
    }

    columns {
      name = "pp_yoffset_min"
      type = "int"
    }

    columns {
      name = "pp_yoffset_max"
      type = "int"
    }

    columns {
      name = "useragent"
      type = "string"
    }

    columns {
      name = "br_name"
      type = "string"
    }

    columns {
      name = "br_family"
      type = "string"
    }

    columns {
      name = "br_version"
      type = "string"
    }

    columns {
      name = "br_type"
      type = "string"
    }

    columns {
      name = "br_renderengine"
      type = "string"
    }

    columns {
      name = "br_lang"
      type = "string"
    }

    columns {
      name = "br_features_pdf"
      type = "boolean"
    }

    columns {
      name = "br_features_flash"
      type = "boolean"
    }

    columns {
      name = "br_features_java"
      type = "boolean"
    }

    columns {
      name = "br_features_director"
      type = "boolean"
    }

    columns {
      name = "br_features_quicktime"
      type = "boolean"
    }

    columns {
      name = "br_features_realplayer"
      type = "boolean"
    }

    columns {
      name = "br_features_windowsmedia"
      type = "boolean"
    }

    columns {
      name = "br_features_gears"
      type = "boolean"
    }

    columns {
      name = "br_features_silverlight"
      type = "boolean"
    }

    columns {
      name = "br_cookies"
      type = "boolean"
    }

    columns {
      name = "br_colordepth"
      type = "string"
    }

    columns {
      name = "br_viewwidth"
      type = "int"
    }

    columns {
      name = "br_viewheight"
      type = "int"
    }

    columns {
      name = "os_name"
      type = "string"
    }

    columns {
      name = "os_family"
      type = "string"
    }

    columns {
      name = "os_manufacturer"
      type = "string"
    }

    columns {
      name = "os_timezone"
      type = "string"
    }

    columns {
      name = "dvce_type"
      type = "string"
    }

    columns {
      name = "dvce_ismobile"
      type = "boolean"
    }

    columns {
      name = "dvce_screenwidth"
      type = "int"
    }

    columns {
      name = "dvce_screenheight"
      type = "int"
    }

    columns {
      name = "doc_charset"
      type = "string"
    }

    columns {
      name = "doc_width"
      type = "int"
    }

    columns {
      name = "doc_height"
      type = "int"
    }

    columns {
      name = "tr_currency"
      type = "string"
    }

    columns {
      name = "tr_total_base"
      type = "double"
    }

    columns {
      name = "tr_tax_base"
      type = "double"
    }

    columns {
      name = "tr_shipping_base"
      type = "double"
    }

    columns {
      name = "ti_currency"
      type = "string"
    }

    columns {
      name = "ti_price_base"
      type = "double"
    }

    columns {
      name = "base_currency"
      type = "string"
    }

    columns {
      name = "geo_timezone"
      type = "string"
    }

    columns {
      name = "mkt_clickid"
      type = "string"
    }

    columns {
      name = "mkt_network"
      type = "string"
    }

    columns {
      name = "etl_tags"
      type = "string"
    }

    columns {
      name = "dvce_sent_tstamp"
      type = "timestamp"
    }

    columns {
      name = "refr_domain_userid"
      type = "string"
    }

    columns {
      name = "refr_device_tstamp"
      type = "timestamp"
    }

    columns {
      name = "derived_contexts"
      type = "string"
    }

    columns {
      name = "domain_sessionid"
      type = "string"
    }

    columns {
      name = "derived_tstamp"
      type = "timestamp"
    }

    columns {
      name = "event_vendor"
      type = "string"
    }

    columns {
      name = "event_name"
      type = "string"
    }

    columns {
      name = "event_format"
      type = "string"
    }

    columns {
      name = "event_version"
      type = "string"
    }

    columns {
      name = "event_fingerprint"
      type = "string"
    }

    columns {
      name = "true_tstamp"
      type = "timestamp"
    }

    compressed        = "true"
    input_format      = "org.apache.hadoop.mapred.TextInputFormat"
    location          = "s3://minds-analytics/snowplow/enriched-good"
    number_of_buckets = "-1"
    output_format     = "org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat"

    ser_de_info {
      name = "ser_de_info"

      parameters = {
        "field.delim" = "\t"
      }

      serialization_library = "org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe"
    }

    stored_as_sub_directories = "true"
  }

  partition_keys {
    name    = "datehour"
    type    = "string"
    comment = "yyyy/MM/dd/HH"
  }

  table_type = "EXTERNAL_TABLE"

  parameters = {
    "projection.enabled"                = "true",
    "projection.datehour.type"          = "date",
    "projection.datehour.range"         = "2020/01/01/00,NOW",
    "projection.datehour.format"        = "yyyy/MM/dd/HH",
    "projection.datehour.interval"      = "1",
    "projection.datehour.interval.unit" = "HOURS",
    "storage.location.template"         = "s3://minds-analytics/snowplow/enriched-good/$${datehour}"
  }
}