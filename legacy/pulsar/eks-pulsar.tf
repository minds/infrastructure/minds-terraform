resource "kubernetes_namespace" "pulsar" {
  provider = kubernetes.eks
  metadata {
    annotations = {
      name = "pulsar"
    }

    labels = {
      name = "pulsar"
    }

    name = "pulsar"
  }
}

locals {

  stateful_node_tolerations = [
    {
      key      = "stateful"
      operator = "Equal"
      value    = "true"
      effect   = "NoSchedule"
    }
  ]

  values = {
    zookeeper = {
      nodeSelector = { stateful = "true" }
      tolerations  = local.stateful_node_tolerations
    }
    bookkeeper = {
      nodeSelector = { stateful = "true" }
      tolerations  = local.stateful_node_tolerations
    }
    autorecovery = {
      nodeSelector = { stateful = "true" }
      tolerations  = local.stateful_node_tolerations
    }
    broker = {
      nodeSelector = { stateful = "true" }
      tolerations  = local.stateful_node_tolerations
    }
    proxy = {
      nodeSelector = { stateful = "true" }
      tolerations  = local.stateful_node_tolerations
    }
    toolset = {
      nodeSelector = { stateful = "true" }
      tolerations  = local.stateful_node_tolerations
    }
  }
}

resource "helm_release" "pulsar" {
  provider  = helm.eks
  name      = "pulsar"
  namespace = kubernetes_namespace.pulsar.metadata[0].name

  chart = "./pulsar_charts/${var.pulsar_chart_version}/pulsar"

  timeout = 1800

  set {
    name  = "namespace"
    value = kubernetes_namespace.pulsar.metadata[0].name
  }

  set {
    name  = "initialize"
    value = var.initialize
  }

  set {
    name  = "components.autorecovery"
    value = true
  }

  dynamic "set" {
    for_each = var.zookeepr_pod_management_policy != "" ? [1] : []
    content {
      name  = "zookeeper.podManagementPolicy"
      value = var.zookeepr_pod_management_policy
    }
  }

  set {
    name  = "zookeeper.replicaCount"
    value = var.replicas
  }

  set {
    name  = "zookeeper.podMonitor.enabled"
    value = var.monitoring
  }

  ##
  # Bookie
  ##

  set {
    name  = "bookkeeper.replicaCount"
    value = var.replicas
  }

  set {
    name  = "bookkeeper.podMonitor.enabled"
    value = var.monitoring
  }

  ##
  # Broker
  ##

  set {
    name  = "broker.replicaCount"
    value = var.replicas
  }

  set {
    name  = "broker.configData.autoSkipNonRecoverableData"
    value = "true"
    type  = "string"
  }

  set {
    name  = "broker.configData.managedLedgerDefaultEnsembleSize"
    value = var.quorum_size
    type  = "string"
  }

  set {
    name  = "broker.configData.managedLedgerDefaultWriteQuorum"
    value = var.quorum_size
    type  = "string"
  }

  set {
    name  = "broker.configData.managedLedgerDefaultAckQuorum"
    value = var.quorum_size
    type  = "string"
  }

  set {
    name  = "broker.resources.requests.memory"
    value = "2Gi"
  }

  set {
    name  = "broker.configData.PULSAR_MEM"
    value = "-Xms1024m -Xmx1024m -XX:MaxDirectMemorySize=2048m"
  }


  set {
    name  = "broker.restartPodsOnConfigMapChange"
    value = true
  }

  set {
    name  = "broker.podMonitor.enabled"
    value = var.monitoring
  }

  ##
  # Pulsar proxy
  ##

  set {
    name  = "proxy.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-internal"
    value = "0.0.0.0/0"
    type  = "string"
  }

  set {
    name  = "proxy.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
    value = "nlb"
  }

  set {
    name  = "proxy.replicaCount"
    value = var.replicas
  }

  set {
    name  = "proxy.resources.requests.memory"
    value = "2Gi"
  }

  set {
    name  = "proxy.configData.PULSAR_MEM"
    value = "-Xms1024m -Xmx1024m -XX:MaxDirectMemorySize=2048m"
  }

  set {
    name  = "proxy.configData.maxConcurrentInboundConnections"
    value = "50000"
    type  = "string"
  }

  set {
    name  = "proxy.restartPodsOnConfigMapChange"
    value = true
  }

  set {
    name  = "proxy.podMonitor.enabled"
    value = var.monitoring
  }

  ##
  # Pulsar manager
  ##

  set {
    name  = "pulsar_manager.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-internal"
    value = "0.0.0.0/0"
    type  = "string"
  }

  ##
  # Tls
  ##

  dynamic "set" {
    for_each = var.ca_suffix != "" ? [1] : []
    content {
      name  = "tls.ca_suffix"
      value = var.ca_suffix
    }
  }

  set {
    name  = "tls.enabled"
    value = true
  }

  set {
    name  = "tls.broker.enabled"
    value = true
  }

  set {
    name  = "tls.zookeeper.enabled"
    value = true
  }

  set {
    name  = "tls.proxy.enabled"
    value = true
  }

  set {
    name  = "certs.internal_issuer.enabled"
    value = true
  }

  set {
    name  = "certs.internal_issuer.type"
    value = var.enable_vault_issuer ? "vault_" : "selfsigning"
  }

  dynamic "set" {
    for_each = var.enable_vault_issuer ? [1] : []
    content {
      name  = "certs.internal_issuer.kind"
      value = "ClusterIssuer"
    }
  }

  dynamic "set" {
    for_each = var.enable_vault_issuer ? [1] : []
    content {
      name  = "certs.internal_issuer.name"
      value = "vault-certs-issuer"
    }
  }

  dynamic "set" {
    for_each = var.enable_vault_issuer ? [1] : []
    content {
      name  = "certs.internal_issuer.apiVersion"
      value = "cert-manager.io/v1"
    }
  }

  ##
  # Monitoring
  ##

  set {
    name  = "monitoring.grafana"
    value = false
  }

  set {
    name  = "monitoring.prometheus"
    value = false
  }

  set {
    name  = "monitoring.node_exporter"
    value = false
  }

  set {
    name  = "monitoring.alert_manager"
    value = false
  }

  values = [yamlencode(local.values)]
}
