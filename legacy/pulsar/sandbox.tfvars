eks_cluster_name               = "sandbox"
replicas                       = 1
quorum_size                    = 1
monitoring                     = false
enable_vault_issuer            = true
pulsar_chart_version           = "2.7.13"
zookeepr_pod_management_policy = "OrderedReady"
ca_suffix                      = "tls-proxy"