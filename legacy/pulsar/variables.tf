##

variable "eks_cluster_name" {
  type        = string
  description = "The name of eks cluster to deploy to"
}

##

variable "initialize" {
  type        = bool
  description = "Set to true on first run"
}

variable "replicas" {
  type        = number
  description = "The number of replicas to use"
  default     = 1
}

variable "quorum_size" {
  type        = number
  description = "The size of the quorum"
  default     = 2
}

variable "monitoring" {
  type        = bool
  description = "Enable monitoring with prometheus"
  default     = true
}

variable "eks_assume_role_name" {
  type        = string
  description = "Name of IAM role to assume before authenticating with the EKS cluster."
  default     = "minds_eks_k8s_admins"
}

variable "enable_vault_issuer" {
  type        = bool
  description = "Whether to enable Vault issuer."
}

variable "pulsar_chart_version" {
  type        = string
  description = "Version of Helm chart to use for Pulsar."
}

variable "zookeepr_pod_management_policy" {
  description = "Pod management policy to apply to Zookeeper pods."
  type        = string
}

variable "ca_suffix" {
  description = "Suffix for certificate authority."
  type        = string
}
