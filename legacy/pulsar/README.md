# Pulsar

## Dependencies

- Ensure the `cert-manager` environment is already installed

## Creating the tenant and namespace

```
kubectl -n pulsar exec -it pod/pulsar-broker-0 -- bash
bin/pulsar-admin tenants create minds-com
bin/pulsar-admin namespaces create minds-com/engine
```