resource "google_compute_global_address" "ingress_address" {
  name = "minds-static-ingress"

}

resource "helm_release" "minds" {
  name = "minds"

  repository = "../../../helm-charts"
  chart      = "minds"

  set {
    name  = "siteUrl"
    value = "https://escape-pod-1.minds.com/"
  }

  set {
    name  = "domain"
    value = "escape-pod-1.minds.com"
  }

  set {
    name  = "cdn_domain"
    value = "cdn.minds.com"
  }

  set {
    name  = "replicaCount"
    value = 2
  }

  set {
    name  = "phpfpm.max_children"
    value = 500
  }

  set {
    name  = "phpfpm.max_requests"
    value = 20000
  }

  set {
    name  = "phpfpm.resources.requests.cpu"
    value = "2"
  }
  set {
    name  = "phpfpm.resources.requests.memory"
    value = "4Gi"
  }
  set {
    name  = "phpfpm.resources.limits.cpu"
    value = "2"
  }
  set {
    name  = "phpfpm.resources.limits.memory"
    value = "4Gi"
  }

  ####################################
  # Transcoder Configurations        #
  ####################################
  set {
    name  = "transcoder.dir"
    value = "cinemr_com"
  }

  ####################################
  # GKE Load Balancer Configurations #
  ####################################
  set {
    name  = "service.type"
    value = "LoadBalancer"
  }
  set {
    name  = "ingress.enabled"
    value = false
  }
  set {
    name  = "ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "gce"
    type  = "string"
  }
  set {
    name  = "ingress.annotations.kubernetes\\.io/ingress\\.global-static-ip-name"
    value = google_compute_global_address.ingress_address.name
    type  = "string"
  }

  ####################################
  # Cassandra Configurations         #
  ####################################
  set {
    name  = "cassandra.server"
    value = "${kubernetes_service.cassandra.metadata[0].name}.${kubernetes_service.cassandra.metadata[0].namespace}.svc.cluster.local"
  }
  set {
    name  = "cassandra.username"
    value = "engine"
  }
  set {
    name  = "cassandra.password"
    value = var.cassandra_password
  }

  ####################################
  # ElasticSearch Configurations     #
  ####################################
  set {
    name  = "elasticsearch.server"
    value = var.elasticsearch_server
  }
  set {
    name  = "elasticsearch.username"
    value = "engine"
  }
  set {
    name  = "elasticsearch.password"
    value = var.elasticsearch_password
  }

  ####################################
  # Redis Configurations             #
  ####################################
  set {
    name  = "redis.master"
    value = "redis-master.caching.svc.cluster.local"
  }
  set {
    name  = "redis.slave"
    value = "redis-slave.caching.svc.cluster.local"
  }

  ####################################
  # RabbitMQ Configurations          #
  ####################################
  set {
    name  = "rabbitmq.server"
    value = "rabbitmq.rabbitmq.svc.cluster.local"
  }
  set {
    name  = "rabbitmq.username"
    value = "user"
  }
  set {
    name  = "rabbitmq.password"
    value = data.kubernetes_secret.rabbitmq_password.data.rabbitmq-password
  }

  ####################################
  # Blockchain Configs               #
  ####################################
  set {
    name  = "blockchain.tokenAddress"
    value = "0xb26631c6dda06ad89b93c71400d25692de89c068"
  }
  set {
    name  = "blockchain.blockchainEndpoint"
    value = "https://mainnet.infura.io/v3/708b51690a43476092936f9818f8c4fa"
  }
  set {
    name  = "blockchain.clientNetwork"
    value = "1"
  }
  set {
    name  = "blockchain.testNet"
    value = "false"
  }

  set {
    name  = "zendesk.base_url"
    value = "https://minds.zendesk.com/"
  }

  ####################################
  # Detect if configMap has changed  #
  ####################################
  set {
    name  = "configMap.checksum"
    value = filemd5("../../../helm-charts/minds/templates/engine-configMap.yaml")
  }
}

output "ingress_ip_address" {
  value = google_compute_global_address.ingress_address.address
}