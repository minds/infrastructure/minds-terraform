variable "gcp_region" {
  type = string
}

variable "gcp_project_id" {
  type = string
}

variable "gcp_network_id" {
  type = string
}

##############################
# Kubernetes variables       #
##############################
variable "kubernetes_node_count" {
  type    = number
  default = 1
}

##############################
# Cassandra variables        #
##############################

variable "cassandra_node_count" {
  type    = number
  default = 1
}

variable "cassandra_join_existing_cluster" {
  type    = bool
  default = false
}

variable "cassandra_seed_node" {
  type    = string
  default = ""
}

##############################
# ElasticSearch variables    #
##############################

variable "elasticsearch_server" {
  type = string
}


##############################
# Minds variables            #
##############################

variable "cassandra_password" {
  type = string
}

variable "elasticsearch_password" {
  type = string
}