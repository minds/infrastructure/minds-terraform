gcp_region     = "us-east1"
gcp_project_id = "minds-production"
gcp_network_id = "projects/minds-production/global/networks/minds-production-vpc"

kubernetes_node_count = 2

cassandra_node_count            = 6
cassandra_join_existing_cluster = true
cassandra_seed_node             = "10.0.8.13"

elasticsearch_server = "internal-elasticsearch-production-659272660.us-east-1.elb.amazonaws.com"