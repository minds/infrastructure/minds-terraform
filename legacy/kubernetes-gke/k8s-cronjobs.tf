

resource "helm_release" "minds-cronjobs" {
  name = "minds-cronjobs"

  repository = "../../../helm-charts"
  chart      = "minds-cronjobs"

  set {
    name  = "configMapName"
    value = "minds" # TODO link this to our other helm release which creates this
  }

  set {
    name  = "imageTag"
    value = "production"
  }

  values = [
    "${file("k8s-cronjobs.yaml")}"
  ]
}
