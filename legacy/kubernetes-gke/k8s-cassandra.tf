resource "kubernetes_namespace" "cassandra" {
  metadata {
    annotations = {
      name = "cassandra"
    }

    labels = {
      name = "cassandra"
    }

    name = "cassandra"
  }
}

resource "kubernetes_service" "cassandra" {
  metadata {
    name      = "cassandra"
    namespace = kubernetes_namespace.cassandra.metadata[0].name
    labels = {
      app = "cassandra"
    }
  }

  spec {
    cluster_ip = "None"

    port {
      name = "cql"
      port = 9042
    }

    selector = {
      "app" = "cassandra"
    }

  }
}

resource "kubernetes_stateful_set" "cassandra" {
  metadata {
    name      = "cassandra"
    namespace = kubernetes_namespace.cassandra.metadata[0].name
    labels = {
      app = "cassandra"
    }
  }
  spec {
    service_name = "cassandra"
    replicas     = var.cassandra_node_count

    selector {
      match_labels = {
        app = "cassandra"
      }
    }

    template {
      metadata {
        labels = {
          app = "cassandra"
        }
      }

      spec {
        termination_grace_period_seconds = 1800

        affinity {
          pod_anti_affinity {
            required_during_scheduling_ignored_during_execution {
              label_selector {
                match_expressions {
                  key      = "app"
                  operator = "In"
                  values   = ["cassandra"]
                }
              }

              topology_key = "kubernetes.io/hostname"
            }
          }
        }

        ##
        # Main cassandra process
        ##
        container {
          name              = "cassandra"
          image             = "minds/cassandra:3.11.11"
          image_pull_policy = "Always"

          port {
            container_port = 7000
            name           = "intra-node"
          }

          port {
            container_port = 7001
            name           = "tls-intra-node"
          }

          port {
            container_port = 7199
            name           = "jmx"
          }

          port {
            container_port = 9042
            name           = "cql"
          }

          resources {
            limits = {
              cpu    = "8"
              memory = "32Gi"
            }
            requests = {
              cpu    = "8"
              memory = "32Gi"
            }
          }

          security_context {
            capabilities {
              add = ["IPC_LOCK"]
            }
          }

          lifecycle {
            pre_stop {
              exec {
                command = ["/bin/sh", "-c", "nodetool drain"]
              }
            }
          }

          env {
            name  = "MAX_HEAP_SIZE"
            value = "16G"
          }
          env {
            name  = "HEAP_NEWSIZE"
            value = "4G"
          }
          env {
            name  = "CASSANDRA_SEEDS"
            value = var.cassandra_join_existing_cluster ? var.cassandra_seed_node : "cassandra-0.cassandra.cassandra.svc.cluster.local"
          }
          env {
            name  = "CASSANDRA_CLUSTER_NAME"
            value = "Minds Cluster"
          }
          env {
            name  = "CASSANDRA_DC"
            value = "gke-k8s"
          }
          env {
            name  = "CASSANDRA_RACK"
            value = "gke-k8s-rack1"
          }
          env {
            name  = "CASSANDRA_ENDPOINT_SNITCH"
            value = "GossipingPropertyFileSnitch"
          }
          # env {
          #   name = "LOCAL_JMX"
          #   value = "no"
          # }
          env {
            name = "POD_IP"
            value_from {
              field_ref {
                field_path = "status.podIP"
              }
            }
          }

          readiness_probe {
            exec {
              command = ["/bin/bash", "-c", "/ready-probe.sh"]
            }
            initial_delay_seconds = 15
            timeout_seconds       = 5
          }
          # These volume mounts are persistent. They are like inline claims,
          # but not exactly because the names need to match exactly one of
          # the stateful pod volumes.
          volume_mount {
            name       = "cassandra-data"
            mount_path = "/var/lib/cassandra"
          }
        }

        ##
        # Reaper (repair) process
        ##
        container {
          name              = "cassandra-reaper-sidecar"
          image             = "thelastpickle/cassandra-reaper:2.2.4"
          image_pull_policy = "Always"

          port {
            container_port = 8080
            name           = "reaper-webui"
          }

          env {
            name  = "REAPER_STORAGE_TYPE"
            value = "cassandra"
          }

          env {
            name  = "REAPER_CASS_CLUSTER_NAME"
            value = "Minds Cluster"
          }

          env {
            name  = "REAPER_CASS_CONTACT_POINTS"
            value = "[ \"127.0.0.1\" ]"
          }

          env {
            name  = "REAPER_CASS_LOCAL_DC"
            value = "gke-k8s"
          }

          env {
            name  = "REAPER_DATACENTER_AVAILABILITY"
            value = "sidecar"
          }

          env {
            name  = "REAPER_ENABLE_WEBUI_AUTH"
            value = "false"
          }

          env {
            name  = "REAPER_HANGING_REPAIR_TIMEOUT_MINS"
            value = 90
          }

          resources {
            limits = {
              cpu    = "2"
              memory = "2Gi"
            }
            requests = {
              cpu    = "1"
              memory = "1Gi"
            }
          }
        }
      }
    }

    update_strategy {
      type = "RollingUpdate"
    }

    # These are converted to volume claims by the controller
    # and mounted at the paths mentioned above.
    # do not use these in production until ssd GCEPersistentDisk or other ssd pd
    volume_claim_template {
      metadata {
        name = "cassandra-data"
      }
      spec {
        access_modes       = ["ReadWriteOnce"]
        storage_class_name = kubernetes_storage_class.ssd.metadata[0].name
        resources {
          requests = {
            storage = "512Gi"
          }
        }
      }
    }
  }
}
