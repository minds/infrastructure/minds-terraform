#!/bin/sh

i=0
total=$(wc -l < tokens.txt)

while read line; do   
    ((i++))     

    if [[ $i -lt 228 ]];
    then
      continue
    fi

    ST=$(echo $line | awk '{ print $1 }')
    ET=$(echo $line | awk '{ print $2 }')
    echo "[$i/$total] Rebuilding $ST - $ET" 
    kubectl -n cassandra exec -t pod/cassandra-1 -- nodetool rebuild -ks minds -ts "($ST,$ET]" -- datacenter1
done < tokens.txt

