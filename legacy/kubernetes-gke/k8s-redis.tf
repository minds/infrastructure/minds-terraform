resource "kubernetes_namespace" "caching" {
  metadata {
    annotations = {
      name = "caching"
    }

    labels = {
      name = "caching"
    }

    name = "caching"
  }
}

resource "helm_release" "redis" {
  name      = "redis"
  namespace = kubernetes_namespace.caching.metadata[0].name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "redis"

  set {
    name  = "usePassword"
    value = false
  }


  set {
    name  = "cluster.slaveCount"
    value = 1
  }

  set {
    name  = "master.resources.requests.cpu"
    value = 2
  }

  set {
    name  = "slave.resources.requests.cpu"
    value = 2
  }
}