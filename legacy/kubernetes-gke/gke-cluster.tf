# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.gcp_project_id}-subnet-gke"
  region        = var.gcp_region
  network       = var.gcp_network_id
  ip_cidr_range = "10.10.1.0/24"
}

# GKE Service Account

resource "google_service_account" "default" {
  account_id   = "gke-service-account"
  display_name = "${var.gcp_project_id} Kubernetes Engine Service Account"
}

# GKE Cluster

resource "google_container_cluster" "primary" {
  provider = google-beta

  name     = "${var.gcp_project_id}-gke"
  location = var.gcp_region

  remove_default_node_pool = true
  initial_node_count       = 1

  network    = var.gcp_network_id
  subnetwork = google_compute_subnetwork.subnet.name

  networking_mode = "VPC_NATIVE"
  ip_allocation_policy {}

  logging_service = "logging.googleapis.com/kubernetes"

  master_auth {
    # username = var.gke_username
    # password = var.gke_password

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "primary_nodes_standard_e2s16" {
  name       = "${var.gcp_project_id}-gke-nodes-16cpu"
  location   = var.gcp_region
  cluster    = google_container_cluster.primary.name
  node_count = var.kubernetes_node_count

  node_config {
    preemptible  = false
    machine_type = "e2-standard-16"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}

data "google_container_cluster" "primary" {
  name     = google_container_cluster.primary.name
  location = google_container_cluster.primary.location
}

data "google_client_config" "provider" {}
