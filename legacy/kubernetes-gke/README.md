# Minds Kubernetes on GKE (Google Kubernetes Engine)

This is still a work in progress. 

## Assumptions

- You already have a GCP VPC setup

## What this module is for

This module controls the entire Minds stack on GCP.

## How to use

`terraform workspace new production`
`terraform apply --var-file=production.tfvars`

## Connecting to the cluster

`gcloud container clusters get-credentials minds-production-gke --region us-east1 --project minds-production`

You can then use `kubectl` as normal - eg. `kubectl get all`

## Joining an existing cassandra cluster

### Run this on each node in the cluster

`kubectl -n cassandra exec -it pod/cassandra-0 -- nodetool rebuild -ks minds -- datacenter1`

### Update the replica count

`ALTER KEYSPACE minds WITH replication = { 'class' : 'NetworkTopologyStrategy', 'datacenter1' : 3, 'gke-k8s': 1};`

### Issues with rebuilding (OOM)

If the rebuild process is resulting in OOM errors, try to rebuild each token range individually to determine where the issue lies.

`/cassandra/bin/nodetool describering -- minds | grep YOUR_NODE_IP | awk -F: '{print $2,$3}' | awk -F'[, ]' '{ print $1,$4 }' > images/cassandra/tokens.txt`
`sh images/cassandra/rebuild.sh`

## Logs not showing in GCP

The service account may not have the neccesary role bindings.

`gcloud projects add-iam-policy-binding minds-production --member="serviceAccount:gke-service-account@minds-production.iam.gserviceaccount.com" --role="roles/monitoring.metricWriter"
`
`gcloud projects add-iam-policy-binding minds-production --member="serviceAccount:gke-service-account@minds-production.iam.gserviceaccount.com" --role="roles/monitoring.metricWriter"
`