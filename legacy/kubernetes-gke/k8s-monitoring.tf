resource "kubernetes_namespace" "monitoring" {
  metadata {
    annotations = {
      name = "monitoring"
    }

    labels = {
      name = "monitoring"
    }

    name = "monitoring"
  }
}

resource "helm_release" "kube-prometheus-stack" {
  name      = "prometheus"
  namespace = kubernetes_namespace.monitoring.metadata[0].name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"

  set {
    name  = "grafana.service.type"
    value = "LoadBalancer"
  }
  set {
    name  = "grafana.ingress.enabled"
    value = true
  }
  set {
    name  = "grafana.ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "gce"
    type  = "string"
  }
  set {
    name  = "grafana.ingress.annotations.kubernetes\\.io/ingress\\.global-static-ip-name"
    value = google_compute_global_address.grafana_ingress_address.name
    type  = "string"
  }
  set {
    name  = "grafana.ingress.annotations.ingress\\.gcp\\.kubernetes\\.io/pre-shared-cert"
    value = google_compute_managed_ssl_certificate.grafana_ingress.name
  }
  set {
    name  = "grafana.ingress.path"
    value = "/*"
  }
  set {
    name  = "grafana.ingress.hosts.0"
    value = "dashboards.minds.com"
  }
  set {
    name  = "grafana.persistence.enabled"
    value = true
  }
  set {
    name  = "grafana.grafana\\.ini.auth\\.anonymous.enabled"
    value = true
  }
  set {
    name  = "grafana.grafana\\.ini.auth\\.anonymous.org_name"
    value = "Main Org."
  }
  set {
    name  = "grafana.grafana\\.ini.auth\\.anonymous.org_role"
    value = "Viewer"
  }
  set {
    name  = "grafana.plugins"
    value = "grafana-piechart-panel"
  }

  ##
  # Prometheus
  ##
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.storageClassName"
    value = kubernetes_storage_class.ssd.metadata[0].name
  }
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.accessModes[0]"
    value = "ReadWriteOnce"
  }
  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.resources.requests.storage"
    value = "50Gi"
  }
}

resource "google_compute_global_address" "grafana_ingress_address" {
  name = "grafana-static-ingress"
}

resource "google_compute_managed_ssl_certificate" "grafana_ingress" {
  provider = google-beta

  name = "graphana-ingress-ssl"

  managed {
    domains = ["dashboards.minds.com"]
  }
}

output "grafana_ingress_ip_address" {
  value = google_compute_global_address.grafana_ingress_address.address
}