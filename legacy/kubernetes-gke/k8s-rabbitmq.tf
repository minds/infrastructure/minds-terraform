resource "kubernetes_namespace" "rabbitmq" {
  metadata {
    annotations = {
      name = "rabbitmq"
    }

    labels = {
      name = "rabbitmq"
    }

    name = "rabbitmq"
  }
}

resource "helm_release" "rabbitmq" {
  name      = "rabbitmq"
  namespace = kubernetes_namespace.rabbitmq.metadata[0].name

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "rabbitmq"
}

data "kubernetes_secret" "rabbitmq_password" {
  metadata {
    name      = "rabbitmq"
    namespace = "rabbitmq"
  }
}

