resource "kubernetes_namespace" "elasticsearch" {
  metadata {
    annotations = {
      name = "elasticsearch"
    }

    labels = {
      name = "elasticsearch"
    }

    name = "elasticsearch"
  }
}

# resource "helm_release" "elasticsearch" {
#   name      = "elasticsearch"
#   namespace = kubernetes_namespace.elasticsearch.metadata[0].name

#   repository = "https://helm.elastic.co"
#   chart      = "elasticsearch"
#   version = "6.8"

#   set {
#     name = "clusterName"
#     value = "k8s-elasticsearch"
#   }

#   set {
#     name = "imageTag"
#     value = "6.7.2"
#   }

#   set {
#     name = "image"
#     value = "minds/elasticsearch"
#   }

# }