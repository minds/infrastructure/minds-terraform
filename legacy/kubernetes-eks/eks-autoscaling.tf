locals {
  // Autoscaler is dependent on OIDC provider
  enable_cluster_autoscaler = (
    var.enable_cluster_autoscaler[terraform.workspace]
    && var.enable_oidc_provider[terraform.workspace]
  )
}

##########################
### Cluster Autoscaler ###
##########################

// IAM Policy

resource "aws_iam_policy" "cluster_autoscaler" {
  count = local.enable_cluster_autoscaler ? 1 : 0

  name   = "AmazonEKSClusterAutoscalerPolicy-${random_string.suffix.result}"
  policy = data.aws_iam_policy_document.cluster_autoscaler[0].json
}

// https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/cloudprovider/aws/README.md
data "aws_iam_policy_document" "cluster_autoscaler" {
  count = local.enable_cluster_autoscaler ? 1 : 0

  statement {
    actions = [
      "autoscaling:DescribeAutoScalingGroups"
    ]
    resources = ["*"]
  }

  statement {
    actions = [
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeScalingActivities",
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "eks:DescribeNodegroup"
    ]
    resources = [
      for ng in data.aws_eks_node_group.stateless : (
        "arn:aws:autoscaling:us-east-1:${local.this_account}:autoScalingGroup:*:autoScalingGroupName/${ng.resources[0].autoscaling_groups[0].name}"
      )
    ]
  }
}

// IAM Role

resource "aws_iam_role" "cluster_autoscaler" {
  count = local.enable_cluster_autoscaler ? 1 : 0

  name               = "AmazonEKSClusterAutoscalerRole-${random_string.suffix.result}"
  assume_role_policy = data.aws_iam_policy_document.cluster_autoscaler_assume_role[0].json
}

data "aws_iam_policy_document" "cluster_autoscaler_assume_role" {
  count = local.enable_cluster_autoscaler ? 1 : 0

  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.cluster[0].arn]
    }
    condition {
      test     = "StringEquals"
      variable = "${local.eks_oidc_provider_id}:aud"
      values   = ["sts.amazonaws.com"]
    }
  }
}

// Role/Policy Attachment

resource "aws_iam_role_policy_attachment" "cluster_autoscaler" {
  count = local.enable_cluster_autoscaler ? 1 : 0

  role       = aws_iam_role.cluster_autoscaler[0].name
  policy_arn = aws_iam_policy.cluster_autoscaler[0].arn
}

// Helm Chart

resource "kubernetes_namespace" "cluster_autoscaler" {
  count = local.enable_cluster_autoscaler ? 1 : 0

  metadata {
    name = "cluster-autoscaler"
  }
}

// Retrieve ASG names created by node groups
data "aws_eks_node_group" "stateless" {
  for_each = local.stateless_nodegroups

  cluster_name    = local.cluster_name
  node_group_name = each.value.name
  depends_on      = [module.eks]
}

resource "helm_release" "cluster_autoscaler" {
  count = local.enable_cluster_autoscaler ? 1 : 0

  name       = "cluster-autoscaler"
  namespace  = "cluster-autoscaler"
  repository = "https://kubernetes.github.io/autoscaler"
  chart      = "cluster-autoscaler"

  version = var.cluster_autoscaler_version[terraform.workspace]

  set {
    name  = "awsRegion"
    value = "us-east-1"
  }

  set {
    name  = "rbac.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.cluster_autoscaler[0].arn
  }

  set {
    name  = "extraArgs.balance-similar-node-groups"
    value = true
  }

  set {
    name  = "extraArgs.scale-down-utilization-threshold"
    value = "0.6"
  }

  set {
    name  = "extraArgs.skip-nodes-with-local-storage"
    value = false
  }

  # values = [yamlencode(local.cluster_autoscaler_values)]

  values = [yamlencode({
    resources = {
      // The autoscaler uses a large amount of memory when initially starting, hence the large range
      requests = { cpu = "50m", memory = "300Mi" },
      limits   = { cpu = "100m", memory = "768Mi" }
    },
    // Build autoscalingGroups list using the attributes we retrieved with the node group
    autoscalingGroups = [for ng in data.aws_eks_node_group.stateless : {
      name    = ng.resources[0].autoscaling_groups[0].name
      maxSize = ng.scaling_config[0].max_size
      minSize = ng.scaling_config[0].min_size
    }]
  })]

  depends_on = [kubernetes_namespace.cluster_autoscaler]
}

resource "kubernetes_manifest" "cluster_autoscaler_servicemonitor" {
  count = var.enable_cluster_autoscaler[terraform.workspace] ? 1 : 0

  manifest = {
    apiVersion = "monitoring.coreos.com/v1"
    kind       = "ServiceMonitor"
    metadata = {
      name      = "cluster-autoscaler"
      namespace = "cluster-autoscaler"
    }
    spec = {
      selector = {
        matchLabels = { "app.kubernetes.io/name" = "aws-cluster-autoscaler" }
      }
      endpoints = [{ port = "http" }]
    }
  }

  depends_on = [kubernetes_namespace.cluster_autoscaler]
}
