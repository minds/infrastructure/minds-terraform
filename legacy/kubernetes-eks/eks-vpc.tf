/*
  Modules:
  - https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/2.6.0

  Resources:
  - https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string
*/

locals {
  cluster_name = local.is_development ? "sandbox" : "minds-eks-${random_string.suffix.result}"
  vpc_id       = module.vpc.vpc_id
}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.78.0"

  name = var.vpc_name[terraform.workspace]

  // Network
  cidr = var.vpc_cidr[terraform.workspace]
  azs  = ["us-east-1a", "us-east-1b", "us-east-1d"]

  private_subnets = var.private_subnets_cidr[terraform.workspace]
  public_subnets  = var.public_subnets_cidr[terraform.workspace]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  // Flow logs
  enable_flow_log           = true
  flow_log_destination_type = "s3"
  flow_log_destination_arn  = module.flow_log_s3.s3_bucket_arn

  // Tagging
  tags = { "kubernetes.io/cluster/${local.cluster_name}" = "shared" }
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}

