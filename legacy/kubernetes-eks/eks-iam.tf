/*
  Resources:
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy

  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role_binding
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map
  - https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account

  - https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release

  Data Sources:
  - https://registry.terraform.io/providers/hashicorp/external/latest/docs/data-sources/data_source
  
  TODO:
  - Refactor IAM resources to create roles based on a map of supplied roles
    - Will require statefile changes to move rename resources.
  - Refactor K8s RBAC resources to create based on a map of supplied roles/bindings
    - Will require statefile changes to move rename resources.
  - Build aws-auth configmap using HCL for aditional safety.
    - Potentially we can incorporate this into the same variable we use for building RBAC resources.
    - We can convert to YAML using the yamlencode()
*/

### OIDC config

// Retrieve SHA1 fingerprint for cluster identity provider
data "tls_certificate" "cluster" { url = data.aws_eks_cluster.cluster.identity.0.oidc.0.issuer }
locals { eks_oidc_sha1_fingerprint = data.tls_certificate.cluster.certificates.0.sha1_fingerprint }

// OIDC Provider ID
// This isn't exposed as an attribute on aws_iam_openid_connect_provider, so we trim it from the URI
locals {
  eks_oidc_provider_id = (
    var.enable_oidc_provider[terraform.workspace]
    ? trim(aws_iam_openid_connect_provider.cluster[0].url, "https://")
    : null
  )
}

resource "aws_iam_openid_connect_provider" "cluster" {
  count           = var.enable_oidc_provider[terraform.workspace] ? 1 : 0
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.cluster.certificates.0.sha1_fingerprint]
  url             = data.aws_eks_cluster.cluster.identity.0.oidc.0.issuer
}

### IAM

locals {
  eks_iam_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${local.this_account}:root"
        }
        Action = "sts:AssumeRole"
      },
    ]
  })

  eks_iam_policy_developers = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${local.this_account}:root"
        }
        Action = "sts:AssumeRole"
      },
      {
        Action = "sts:AssumeRoleWithSAML"
        Effect = "Allow"
        Principal = {
          Federated = "arn:aws:iam::${local.this_account}:saml-provider/Keycloak"
        }
        Condition = {
          StringEquals = {
            "SAML:aud" = "https://signin.aws.amazon.com/saml"
          }
        }
      }
    ]
  })
}

###################
### Admins Role ###
###################

resource "aws_iam_role" "k8s_admins" {
  count              = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name               = "minds_eks_k8s_admins"
  assume_role_policy = local.eks_iam_policy
}

resource "aws_iam_group" "k8s_admins" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_admins"
  path  = "/"
}

resource "aws_iam_group_policy" "k8s_admins" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_admins"
  group = aws_iam_group.k8s_admins[0].name
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid      = "AllowAssumeOrganizationAccountRole",
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.k8s_admins[0].arn
      }
    ]
  })
}

######################
### Superhero Role ###
######################

resource "aws_iam_role" "k8s_superheroes" {
  count              = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name               = "minds_eks_k8s_superheroes"
  assume_role_policy = local.eks_iam_policy
}

resource "aws_iam_group" "k8s_superheroes" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_superheroes"
  path  = "/"
}

resource "aws_iam_group_policy" "k8s_superheroes" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_superheroes"
  group = aws_iam_group.k8s_superheroes[0].name
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid      = "AllowAssumeOrganizationAccountRole",
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.k8s_superheroes[0].arn
      }
    ]
  })
}

######################
### Developer Role ###
######################

resource "aws_iam_role" "k8s_developers" {
  count              = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name               = "minds_eks_k8s_developers"
  assume_role_policy = local.eks_iam_policy_developers
}

resource "aws_iam_group" "k8s_developers" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_developers"
  path  = "/"
}

resource "aws_iam_group_policy" "k8s_developers" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_developers"
  group = aws_iam_group.k8s_developers[0].name
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid      = "AllowAssumeOrganizationAccountRole",
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.k8s_developers[0].arn
      }
    ]
  })
}

###################
### DevOps Role ###
###################

resource "aws_iam_role" "k8s_devops" {
  count              = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name               = "minds_eks_k8s_devops"
  assume_role_policy = local.eks_iam_policy_developers
}

resource "aws_iam_group" "k8s_devops" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_devops"
  path  = "/"
}

resource "aws_iam_group_policy" "k8s_devops" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_devops"
  group = aws_iam_group.k8s_devops[0].name
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid      = "AllowAssumeOrganizationAccountRole",
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.k8s_devops[0].arn
      }
    ]
  })
}

###################
### Deploy Role ###
###################

resource "aws_iam_role" "k8s_deploy" {
  count              = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name               = "minds_eks_k8s_deploy"
  assume_role_policy = local.eks_iam_policy
}

resource "aws_iam_group" "k8s_deploy" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_deploy"
  path  = "/"
}

resource "aws_iam_group_policy" "k8s_deploy" {
  count = var.enable_iam_roles[terraform.workspace] ? 1 : 0
  name  = "minds_eks_k8s_deploy"
  group = aws_iam_group.k8s_deploy[0].name
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid      = "AllowAssumeOrganizationAccountRole",
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.k8s_deploy[0].arn
      }
    ]
  })
}

##

locals {
  mapRoles = (
    local.is_development
    ? <<YAML
- groups:
  - system:bootstrappers
  - system:nodes
  rolearn: ${local.nodegroup_cluster_role}
  username: "system:node:{{EC2PrivateDNSName}}"
- groups:
  - system:masters
  rolearn: arn:aws:iam::${local.this_account}:role/minds_eks_k8s_developers
  username: dev-user
- groups:
  - system:masters
  rolearn: arn:aws:iam::${local.this_account}:role/minds_eks_k8s_devops
  username: devops-user
- groups:
  - system:masters
  rolearn: ${aws_iam_role.gitlab_deploy[0].arn}
  username: gitlab-deploy
YAML
    : <<YAML
- "groups":
  - "system:bootstrappers"
  - "system:nodes"
  "rolearn": "${module.eks.worker_iam_role_arn}"
  "username": "system:node:{{EC2PrivateDNSName}}"
- groups:
  - system:masters
  rolearn: ${aws_iam_role.k8s_superheroes[0].arn}
  username: superhero-user
- groups:
  - system:masters
  rolearn: ${aws_iam_role.k8s_admins[0].arn}
  username: admin-user
- rolearn: ${aws_iam_role.k8s_developers[0].arn}
  username: dev-user
- rolearn: ${aws_iam_role.k8s_devops[0].arn}
  username: devops-user
- rolearn: ${aws_iam_role.k8s_deploy[0].arn}
  username: deploy-user
YAML 
  )
}

resource "kubernetes_config_map" "aws_auth" {
  metadata {
    namespace = "kube-system"
    name      = "aws-auth"
  }

  data = {
    mapAccounts = <<YAML
[]
YAML
    mapUsers    = <<YAML
[]
YAML
    mapRoles    = local.mapRoles
  }
}


### ALB Controller

resource "aws_iam_policy" "alb_controller_policy" {
  count = var.enable_alb_controller[terraform.workspace] ? 1 : 0

  name        = var.alb_controller_policy_name[terraform.workspace]
  path        = "/"
  description = "Minds EKS Controller Policy"

  policy = file("${path.module}/policies/alb-controller-policy.json")
}


resource "aws_iam_role" "alb_controller_role" {
  count = var.enable_alb_controller[terraform.workspace] ? 1 : 0

  name = var.alb_controller_role_name[terraform.workspace]

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = aws_iam_openid_connect_provider.cluster[0].arn
        }
        Condition = {
          StringEquals = {
            "${aws_iam_openid_connect_provider.cluster[0].url}:sub" : "system:serviceaccount:kube-system:aws-load-balancer-controller"
          }
        }
      },
    ]
  })

  managed_policy_arns = [aws_iam_policy.alb_controller_policy[0].arn]
}

resource "kubernetes_service_account" "alb_controller_k8s_service_account" {
  count = var.enable_alb_controller[terraform.workspace] ? 1 : 0
  metadata {
    name      = "aws-load-balancer-controller"
    namespace = "kube-system"

    labels = {
      "app.kubernetes.io/component" = "controller"
      "app.kubernetes.io/name"      = "aws-load-balancer-controller"
    }

    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.alb_controller_role[0].arn
    }
  }
}

resource "helm_release" "alb_controller" {
  count = var.enable_alb_controller[terraform.workspace] ? 1 : 0
  name  = "aws-load-balancer-controller"

  chart      = "aws-load-balancer-controller"
  repository = "https://aws.github.io/eks-charts"
  namespace  = "kube-system"

  set {
    name  = "clusterName"
    value = local.cluster_name
  }

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller"
  }
}
