################
### IAM Role ###
################

locals {
  oidc_url = replace(data.aws_eks_cluster.cluster.identity.0.oidc.0.issuer, "https://", "")
}

resource "aws_iam_role" "eks_ebs_csi" {
  name = "AmazonEKS_EBS_CSI_DriverRole-${terraform.workspace}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = "arn:aws:iam::${local.this_account}:oidc-provider/${local.oidc_url}"
        },
        Condition = {
          StringEquals = {
            "${local.oidc_url}:aud" = "sts.amazonaws.com",
            "${local.oidc_url}:sub" = "system:serviceaccount:kube-system:ebs-csi-controller-sa"
          }
        }
      },
    ]
  })
}

##################################
### IAM Role/Policy Attachment ###
##################################

resource "aws_iam_role_policy_attachment" "eks_ebs_csi" {
  role       = aws_iam_role.eks_ebs_csi.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
}

##########################
### K8s ServiceAccount ###
##########################

resource "kubernetes_service_account" "eks_ebs_csi" {
  metadata {
    name      = "ebs-csi-controller-sa"
    namespace = "kube-system"
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.eks_ebs_csi.arn
    }
  }
}

########################
### GP3 StorageClass ###
########################

resource "kubernetes_storage_class" "gp3" {
  metadata {
    name        = "gp3"
    annotations = { "storageclass.kubernetes.io/is-default-class" = true } // Set GP3 as default SC for new volumes
  }
  parameters          = { type = "gp3" }
  storage_provisioner = "ebs.csi.aws.com"
  volume_binding_mode = "WaitForFirstConsumer"
  reclaim_policy      = "Delete"
}
