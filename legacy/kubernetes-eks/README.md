# Minds Kubernetes

**We have a VPC per kubernetes environment**

## Local requirements

- wget (brew install wget)

## Installation

Unfortunely this environment needs to run `kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller/crds?ref=master"` in order for load balancers to work. This is due to the fact that the kubernetes prodivers do not support crds.

## Connecting to production

Authenticate following this guide: https://developers.minds.com/docs/handbook/security-practices/#cli

### As a superhero
`aws eks --profile=saml --region us-east-1 update-kubeconfig --name minds-eks-jkqjL8xS --role-arn=arn:aws:iam::324044571751:role/minds_eks_k8s_superheroes` 

### As a developer
`aws eks --profile=saml --region us-east-1 update-kubeconfig --name minds-eks-jkqjL8xS --role-arn=arn:aws:iam::324044571751:role/minds_eks_k8s_developers` 

### As a deployer / ci
`aws eks --profile=saml --region us-east-1 update-kubeconfig --name minds-eks-jkqjL8xS --role-arn=arn:aws:iam::324044571751:role/minds_eks_k8s_deploy` 

## Troubleshooting

```
Error: Failed to update Config Map: Unauthorized

  on .terraform/modules/eks/terraform-aws-eks-12.0.0/aws_auth.tf line 62, in resource "kubernetes_config_map" "aws_auth":
  62: resource "kubernetes_config_map" "aws_auth" {
```

Try running: `aws eks --region $(eval terraform output region) update-kubeconfig --name $(eval terraform output cluster_name)`

## VPC Flow Logs

You can generate and deploy a CloudFormation stack for querying VPC flow logs with Athena using these commands:

```bash
aws ec2 get-flow-logs-integration-template \
  --region us-east-1 \
  --flow-log-id $(terraform output -raw vpc_flow_log_id) \
  --config-delivery-s3-destination-arn $(terraform output -raw vpc_flow_log_s3_arn)/templates \
  --integrate-services "AthenaIntegrations=[{IntegrationResultS3DestinationArn="$(terraform output -raw vpc_flow_log_s3_arn)/query-results/",PartitionLoadFrequency=daily}]"
```

The above command should output an S3 object. Use the URI for this object in this command to deploy the stack:

```bash
aws cloudformation create-stack \
  --region us-east-1 \
  --stack-name minds-vpc-flow-logs-athena-production \
  --capabilities CAPABILITY_IAM \
  --template-url <RESULTS_URI>
```
