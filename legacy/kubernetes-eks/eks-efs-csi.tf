locals {
  efs_cidrs = local.is_development ? var.private_subnets_cidr[terraform.workspace] : slice(var.private_subnets_cidr[terraform.workspace], 6, 9)
}

################
### IAM Role ###
################

resource "aws_iam_role" "eks_efs_csi" {
  name = "AmazonEKS_EFS_CSI_DriverRole-${terraform.workspace}"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = "arn:aws:iam::${local.this_account}:oidc-provider/${local.oidc_url}"
        },
        Condition = {
          StringEquals = {
            "${local.oidc_url}:sub" = "system:serviceaccount:kube-system:efs-csi-controller-sa"
          }
        }
      },
    ]
  })
}

##################################
### IAM Role/Policy Attachment ###
##################################

resource "aws_iam_policy" "eks_efs_csi_driver_policy" {
  name        = "AmazonEKS_EFS_CSI_Driver_Policy-${terraform.workspace}"
  path        = "/"
  description = "Minds EFS Controller Policy"

  policy = file("${path.module}/policies/efs-csi-driver-policy.json")
}

resource "aws_iam_role_policy_attachment" "eks_efs_csi" {
  role       = aws_iam_role.eks_efs_csi.name
  policy_arn = aws_iam_policy.eks_efs_csi_driver_policy.arn
}

##########################
### K8s ServiceAccount ###
##########################

resource "kubernetes_service_account" "eks_efs_csi" {
  metadata {
    name      = "efs-csi-controller-sa"
    namespace = "kube-system"
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.eks_efs_csi.arn
    }
  }
}

#######################
### EFS Helm Chart ###
######################


resource "helm_release" "efs_driver" {
  name = "aws-efs-csi-driver"

  chart      = "aws-efs-csi-driver"
  repository = "https://kubernetes-sigs.github.io/aws-efs-csi-driver/"
  namespace  = "kube-system"

  set {
    name  = "image.repository"
    value = "${local.this_account}.dkr.ecr.us-east-1.amazonaws.com/eks/aws-efs-csi-driver"
  }

  set {
    name  = "controller.serviceAccount.create"
    value = "false"
  }

  set {
    name  = "controller.serviceAccount.name"
    value = "efs-csi-controller-sa"
  }
}

###################
### EFS Volume ###
##################

resource "aws_efs_file_system" "efs_file_system" {
  creation_token   = "efs-${terraform.workspace}"
  encrypted        = true
  performance_mode = "generalPurpose"
}


data "aws_subnet" "efs_mount_subnet" {
  count = length(local.efs_cidrs)

  cidr_block = local.efs_cidrs[count.index]
}

resource "aws_security_group" "efs_mount_sg" {
  name        = "allow_efs"
  description = "Allow EFS"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "EFS from VPC"
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = local.efs_cidrs
  }
}

resource "aws_efs_mount_target" "efs_mount" {
  count = length(local.efs_cidrs)

  file_system_id  = aws_efs_file_system.efs_file_system.id
  subnet_id       = data.aws_subnet.efs_mount_subnet[count.index].id
  security_groups = [aws_security_group.efs_mount_sg.id]
}

########################
### EFS StorageClass ###
########################

resource "kubernetes_storage_class" "efs" {
  metadata {
    name = "efs-sc"
  }
  parameters = {
    #type = "efs"
    provisioningMode = "efs-ap"
    fileSystemId     = aws_efs_file_system.efs_file_system.id
    directoryPerms   = "700"
    gidRangeStart    = "1000"                  # optional
    gidRangeEnd      = "2000"                  # optional
    basePath         = "/dynamic_provisioning" # optional
  }
  storage_provisioner = "efs.csi.aws.com"
}
