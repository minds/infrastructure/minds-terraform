locals {
  flow_log_s3_bucket = "minds-${terraform.workspace}-vpc-flow-logs"
}

module "flow_log_s3" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "3.4.0"

  bucket        = local.flow_log_s3_bucket
  force_destroy = true

  attach_lb_log_delivery_policy = true

  tags = {
    Name = local.flow_log_s3_bucket
  }
}
