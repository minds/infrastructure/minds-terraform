provider "aws" {
  region = "us-east-1"
}

provider "aws" {
  alias  = "eks"
  region = "us-east-1"

  assume_role {
    role_arn     = "arn:aws:iam::${local.this_account}:role/${var.eks_assume_role_name}"
    session_name = "monitoring-terraform"
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.35.0"
    }
  }
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "kubernetes/terraform.tfstate"
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    token                  = data.aws_eks_cluster_auth.cluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  }
}

# For retrieving information about the identity applying this workspace
data "aws_caller_identity" "this" {}
locals { this_account = data.aws_caller_identity.this.account_id }
