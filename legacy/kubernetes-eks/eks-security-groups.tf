/*
  Resources:
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group

  TODO:
  - Define one security group for each node group
  - Create security_group_rule's with a for/each loop based on map
*/


resource "aws_security_group" "node_group_one" {
  count = local.is_development ? 0 : 1

  name_prefix = "node_group_one"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.2.0.0/16",
    ]
  }
}

resource "aws_security_group" "all_node_groups" {
  count = local.is_development ? 0 : 1

  name_prefix = "all_node_groups"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.2.0.0/16",
      "172.16.0.0/12",
    ]
  }
}

resource "aws_security_group_rule" "allow_tasman" {
  count = local.is_development ? 0 : 1

  type                     = "ingress"
  from_port                = 9042
  to_port                  = 9042
  protocol                 = "tcp"
  source_security_group_id = "sg-0ddc16e6eb014dfc1"
  security_group_id        = module.eks.cluster_primary_security_group_id
}
