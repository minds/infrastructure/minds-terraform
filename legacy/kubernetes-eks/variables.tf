variable "region" {
  type    = string
  default = "us-east-1"
}

variable "eks_assume_role_name" {
  type        = string
  description = "Name of IAM role to assume before authenticating with the EKS cluster."
  default     = "minds_eks_k8s_devops"
}

variable "eks_version" {
  type = map(string)
  default = {
    sandbox    = "1.23"
    production = "1.21"
  }
}

variable "eks_ebs_csi_addon_version" {
  type = map(string)
  default = {
    sandbox    = "v1.11.4-eksbuild.1"
    production = "v1.11.2-eksbuild.1"
  }
}

################
#### Network ###
################

// VPC
variable "vpc_name" {
  description = "Name of the deployed VPC."
  type        = map(string)

  default = {
    sandbox    = "eksctl-sandbox-cluster/VPC"
    production = "minds-eks-vpc"
  }
}

variable "vpc_cidr" {
  description = "CIDR range to supply to the deployed VPC."
  type        = map(string)

  default = {
    sandbox    = "192.168.0.0/16"
    production = "10.2.0.0/16"
  }
}
// Subnets
variable "private_subnets_cidr" {
  description = "CIDR ranges for the deployed public subnets."
  type        = map(list(string))

  default = {
    sandbox    = ["192.168.96.0/19", "192.168.128.0/19", "192.168.160.0/19"]
    production = ["10.2.1.0/24", "10.2.2.0/24", "10.2.3.0/24", "10.2.7.0/24", "10.2.8.0/24", "10.2.9.0/24", "10.2.160.0/19", "10.2.192.0/19", "10.2.224.0/19"]
  }
}

variable "public_subnets_cidr" {
  description = "CIDR ranges for the deployed public subnets."
  type        = map(list(string))

  default = {
    sandbox    = ["192.168.0.0/19", "192.168.32.0/19", "192.168.64.0/19"]
    production = ["10.2.4.0/24", "10.2.5.0/24", "10.2.6.0/24"]
  }
}

######################
### ALB Controller ###
######################

variable "enable_alb_controller" {
  description = "Boolean describing whether to enable the AWS ALB controller and related resources."
  type        = map(bool)

  default = {
    sandbox    = true
    production = true
  }
}

variable "alb_controller_role_name" {
  description = "Name for ALB controller IAM role."
  type        = map(string)

  default = {
    sandbox    = "minds_eks_alb_controller_role_sandbox"
    production = "minds_eks_alb_controller_role"
  }
}

variable "alb_controller_policy_name" {
  description = "Name for ALB controller IAM policy."
  type        = map(string)

  default = {
    sandbox    = "minds_eks_alb_controller_policy_sandbox"
    production = "minds_eks_alb_controller_policy"
  }
}


###########
### IAM ###
###########

variable "enable_oidc_provider" {
  description = "Boolean determining whether to create an OIDC provider for the cluster."
  type        = map(bool)

  default = {
    sandbox    = true
    production = true
  }
}

variable "enable_iam_roles" {
  description = "Boolean determining whether to create IAM Resources for the cluster."
  type        = map(bool)

  default = {
    sandbox    = false
    production = true
  }
}


##########################
### Cluster Autoscaler ###
##########################

variable "enable_cluster_autoscaler" {
  description = "Boolean determining whether to enable the Cluster Autoscaler resources."
  type        = map(bool)

  default = {
    sandbox    = true
    production = true
  }
}

variable "cluster_autoscaler_version" {
  type        = map(string)
  description = "Versino of Cluster Autoscaler Helm chart to deploy."

  default = {
    sandbox    = "9.9.2" // 1.20.0
    production = "9.4.0" // 1.18.1
  }
}
