locals {
  k8s_cluster_roles = {
    "minds-eks-devs" = {
      user = "dev-user" // Currently we only map one user to each role
      rules = [
        {
          api_groups = ["", "apps"],
          verbs      = ["get", "list", "watch"],
          resources = [
            "nodes",
            "namespaces",
            "pods",
            "pods/log",
            "deployments",
            "statefulsets",
            "configmaps",
            "serviceaccounts"
          ]
        },
        {
          api_groups = ["batch"],
          verbs      = ["get", "list", "watch"],
          resources  = ["cronjobs", "jobs"]
        },
        {
          api_groups = ["apiextensions.k8s.io"],
          verbs      = ["get", "list", "watch"],
          resources  = ["customresourcedefinitions"]
        },
        {
          api_groups = ["monitoring.coreos.com"],
          verbs      = ["get", "list", "watch"],
          resources  = ["servicemonitors"]
        },
        {
          api_groups = ["metrics.k8s.io"],
          verbs      = ["get", "list"],
          resources  = ["pods", "nodes"]
        },
        // Read K8s RBAC resources
        {
          api_groups = ["rbac.authorization.k8s.io"],
          verbs      = ["get", "watch", "list"],
          resources  = ["clusterroles", "clusterrolebindings", "roles", "rolebindings"]
        },
        // For Helm provider, since Helm stores its releases as Secrets by default
        {
          api_groups = [""],
          verbs      = ["list"],
          resources  = ["secrets"]
        }
      ]
    },
    "minds-eks-devops" = {
      user = "devops-user" // Currently we only map one user to each role
      rules = [
        {
          api_groups = ["", "apps"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources = [
            "nodes",
            "namespaces",
            "pods",
            "pods/log",
            "deployments",
            "statefulsets",
            "configmaps",
            "services",
            "serviceaccounts",
            "replicasets",
            "daemonsets",
            "events",
            "persistentvolumeclaims",
            "persistentvolumes"
          ]
        },
        {
          api_groups = ["batch"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["cronjobs", "jobs"]
        },
        {
          api_groups = ["policy"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["podsecuritypolicies"]
        },
        {
          api_groups = ["autoscaling"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["horizontalpodautoscalers"]
        },
        {
          api_groups = ["storage.k8s.io"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["storageclasses", "csidrivers", "csinodes"]
        },
        {
          api_groups = ["apiextensions.k8s.io"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["customresourcedefinitions"]
        },
        {
          api_groups = ["monitoring.coreos.com"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["servicemonitors", "podmonitors"]
        },
        {
          api_groups = ["operator.tigera.io"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["installations", "apiservers"]
        },
        {
          api_groups = ["metrics.k8s.io"],
          verbs      = ["get", "list"],
          resources  = ["pods", "nodes"]
        },
        {
          api_groups = ["traefik.containo.us"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["ingressroutes", "middlewares"]
        },
        {
          api_groups = ["cert-manager.io"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["certificates"]
        },
        // Manage K8s NetworkPolicy resources
        {
          api_groups = ["crd.projectcalico.org", "projectcalico.org"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete"],
          resources  = ["networkpolicies"]
        },
        // Manage K8s RBAC resources
        {
          api_groups = ["rbac.authorization.k8s.io"],
          verbs      = ["get", "list", "watch", "create", "update", "patch", "delete", "escalate"], // "escalate" is needed in order for the role to manage other roles and bindings
          resources  = ["clusterroles", "clusterrolebindings", "roles", "rolebindings"]
        },
        // For Helm provider, since Helm stores its releases as Secrets by default
        {
          api_groups = [""],
          verbs      = ["get", "create", "update", "list", "delete"],
          resources  = ["secrets"]
        },
        // For the efs-csi
        {
          api_groups = ["coordination.k8s.io"],
          verbs      = ["get", "watch", "list", "delete", "update", "create"],
          resources  = ["leases"]
        }
      ]
    },
    "minds-eks-deploy" = {
      namespace = "default",
      rules = [
        {
          api_groups = ["", "apps"],
          verbs      = ["get", "list", "watch", "patch"],
          resources  = ["namespaces", "pods", "pods/log", "deployments", "statefulsets"]
        }
      ]
    }
  }

  k8s_cluster_role_bindings = {
    "minds-eks-devs"        = { user = "dev-user", role_ref = "minds-eks-devs" },
    "minds-eks-devops"      = { user = "devops-user", role_ref = "minds-eks-devops" },
    "minds-eks-admins"      = { user = "admin-user", role_ref = "cluster-admin" },
    "minds-eks-superheroes" = { user = "superhero-user", role_ref = "cluster-admin" },
    "minds-eks-deploy"      = { user = "deploy-user", role_ref = "minds-eks-deploy" }
  }

  k8s_roles = {
    "minds-eks-deploy" = {
      namespace = "default",
      rules = [
        {
          api_groups = ["", "apps"],
          verbs      = ["get", "list", "watch", "patch"],
          resources  = ["namespaces", "pods", "pods/log", "deployments", "statefulsets"]
        }
      ]
    }
  }

  k8s_role_bindings = {
    "minds-eks-deploy" = { namespace = "default", user = "deploy-user", role_ref = "minds-eks-deploy" }
  }
}

###################
### ClusterRole ###
###################

resource "kubernetes_cluster_role" "cluster_role" {
  for_each = local.k8s_cluster_roles

  metadata { name = each.key }

  dynamic "rule" {
    for_each = each.value.rules
    content {
      api_groups = rule.value.api_groups
      resources  = rule.value.resources
      verbs      = rule.value.verbs
    }
  }
}

resource "kubernetes_cluster_role_binding" "cluster_role_binding" {
  for_each   = local.k8s_cluster_role_bindings
  depends_on = [kubernetes_cluster_role.cluster_role]

  metadata { name = each.key }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = each.value.role_ref
  }

  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = each.value.user
  }
}

############
### Role ###
############

resource "kubernetes_role" "role" {
  for_each = local.k8s_roles

  metadata {
    name      = each.key
    namespace = each.value.namespace
  }

  dynamic "rule" {
    for_each = each.value.rules
    content {
      api_groups = rule.value.api_groups
      resources  = rule.value.resources
      verbs      = rule.value.verbs
    }
  }
}

resource "kubernetes_role_binding" "role_binding" {
  for_each   = local.k8s_role_bindings
  depends_on = [kubernetes_role.role]

  metadata {
    name      = each.key
    namespace = each.value.namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = each.value.role_ref
  }

  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = each.value.user
  }
}
