// The Calico Operator chart wants to manage this namespace, so label and annotating appropriately
resource "kubernetes_namespace" "tigera_operator" {
  metadata {
    name = "tigera-operator"
    labels = {
      "name"                         = "tigera-operator"
      "app.kubernetes.io/managed-by" = "Helm"
    }
    annotations = {
      "meta.helm.sh/release-name"      = "calico"
      "meta.helm.sh/release-namespace" = "tigera-operator"
    }
  }
}

resource "helm_release" "calico_operator" {
  repository = "https://docs.projectcalico.org/charts"
  name       = "calico"
  chart      = "tigera-operator"
  namespace  = kubernetes_namespace.tigera_operator.metadata[0].name
  version    = "v3.21.4"
}

// TODO This cannot be deployed until the Operator has finished creating CRDs
resource "kubernetes_manifest" "calico_api_server" {
  depends_on = [helm_release.calico_operator]
  manifest = {
    apiVersion = "operator.tigera.io/v1"
    kind       = "APIServer"
    metadata   = { name = "default" }
    spec       = {}
  }
}
