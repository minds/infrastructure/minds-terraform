/*
  Temporary file containing Sandbox-only resources.
*/

locals {
  is_development         = terraform.workspace == "sandbox" ? true : false
  nodegroup_cluster_role = "arn:aws:iam::${local.this_account}:role/eksctl-sandbox-nodegroup-ng-1-wor-NodeInstanceRole-1JUC48MCVFT8K"
}

/*****************************************************************************/

##########################
## Service role for EKS ##
##########################

resource "aws_iam_role" "eks_cluster_service_role" {
  count = local.is_development ? 1 : 0

  assume_role_policy = <<EOF
{
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Effect":  "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      }
    }
  ],
  "Version": "2012-10-17"
}
EOF
  ## EKSCTL compatability
  tags = {
    "alpha.eksctl.io/cluster-name"                = local.cluster_name
    "eksctl.cluster.k8s.io/v1alpha1/cluster-name" = local.cluster_name
  }
}

# Role to allow gitlab to control eks
resource "aws_iam_role" "gitlab_deploy" {
  count = local.is_development ? 1 : 0

  name = "gitlab_deploy"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${local.this_account}:root"
        }
        Action = "sts:AssumeRole"
      },
    ]
  })
}

resource "aws_iam_user_policy" "gitlab_deploy" {
  count = local.is_development ? 1 : 0

  name = "gitlab_sandbox_deploy"
  user = "gitlab-sandbox"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Sid      = "AllowAssumeOrganizationAccountRole",
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.gitlab_deploy[0].arn
      }
    ]
  })
}

/*****************************************************************************/

#####################################
## Security groups for eks cluster ##
#####################################

resource "aws_security_group" "eks_cluster_security_group" {
  count = local.is_development ? 1 : 0

  vpc_id      = module.vpc.vpc_id
  description = "Communication between the control plane and worker nodegroups"
  egress = [

  ]
  ingress = [

  ]
  revoke_rules_on_delete = false

  ## EKSCTL still required until terraform fully migrated
  tags = {
    "Name"                                        = "eksctl-sandbox-cluster/ControlPlaneSecurityGroup"
    "alpha.eksctl.io/cluster-name"                = "sandbox"
    "eksctl.cluster.k8s.io/v1alpha1/cluster-name" = "sandbox"
  }

}
