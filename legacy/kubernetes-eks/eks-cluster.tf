/*
  Modules:
  - https://github.com/terraform-aws-modules/terraform-aws-eks/tree/v17.10.0

  Data Sources:
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster
  - https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth

  TODO:
  - If possible, restrict node groups for Sandbox to private subnets only.
  - Allow the module to take ownership of IAM resources.
    - The applied policies for these rules are different, so we'll need to recreate them.
  - Allow the module to take ownership of security groups
    - As with IAM, the rules are slightly different than the groups we've created separately for Sandbox.
  - Create new node group(s) for cluster autoscaler to be managed by the module, removing the managed node group in Sandbox.
*/

locals {
  // TODO Do we need to enable public subnets for the Sandbox cluster?
  cluster_subnets = (
    local.is_development
    ? concat(module.vpc.private_subnets, module.vpc.public_subnets)
    : slice(module.vpc.private_subnets, 0, 3)
  )

  stateless_nodegroup_config = (
    local.is_development
    ? {
      instance_types            = ["c5.xlarge"]
      desired_capacity          = 1
      min_capacity              = 0
      max_capacity              = 6
      disk_size                 = 128
      source_security_group_ids = []
    }
    : {
      instance_types   = ["m5.2xlarge"]
      desired_capacity = 2
      min_capacity     = 0
      max_capacity     = 6
      disk_size        = 100 // TODO reduce size
      source_security_group_ids = [
        aws_security_group.node_group_one[0].id,
        aws_security_group.all_node_groups[0].id,
      ]
    }
  )

  stateful_nodegroup_config = (
    local.is_development
    ? {
      instance_types            = ["t3a.medium"],
      desired_capacity          = 1,
      min_capacity              = 1,
      max_capacity              = 4,
      disk_size                 = 128,
      source_security_group_ids = []
    }
    : {
      instance_types   = ["m5.4xlarge"],
      desired_capacity = 3,
      min_capacity     = 1,
      max_capacity     = 4,
      disk_size        = 100,
      source_security_group_ids = [
        aws_security_group.node_group_one[0].id,
        aws_security_group.all_node_groups[0].id,
      ]
    }
  )

  stateless_subnets = local.is_development ? module.vpc.private_subnets : slice(module.vpc.private_subnets, 6, 9)
  stateless_nodegroups = { for i, subnet in local.stateless_subnets : local.is_development ? "stateless-${i}" : "stateless-ng-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = "stateless-${i}"
      subnets                   = [subnet]
      instance_types            = local.stateless_nodegroup_config.instance_types
      desired_capacity          = local.stateless_nodegroup_config.desired_capacity
      min_capacity              = local.stateless_nodegroup_config.min_capacity
      max_capacity              = local.stateless_nodegroup_config.max_capacity
      disk_size                 = local.stateless_nodegroup_config.disk_size
      source_security_group_ids = local.stateless_nodegroup_config.source_security_group_ids
      k8s_labels                = { stateless = "true" }
      // TODO remove ASG tags
      additional_tags = {
        "k8s.io/cluster-autoscaler/enabled"               = true,
        "k8s.io/cluster-autoscaler/${local.cluster_name}" = "owned"
      }
  }) }

  // Stateful Node Groups

  stateful_subnets = local.is_development ? slice(module.vpc.private_subnets, 0, 3) : slice(module.vpc.private_subnets, 3, 6)

  stateful_nodegroups = { for i, subnet in local.is_development ? slice(module.vpc.private_subnets, 0, 3) : [] : "stateful-ng-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = local.is_development ? "stateful-ng-${i}" : "stateful-${i}" // TODO fix
      subnets                   = [subnet]
      instance_types            = local.stateful_nodegroup_config.instance_types
      desired_capacity          = local.stateful_nodegroup_config.desired_capacity
      min_capacity              = local.stateful_nodegroup_config.min_capacity
      max_capacity              = local.stateful_nodegroup_config.max_capacity
      disk_size                 = local.stateful_nodegroup_config.disk_size
      source_security_group_ids = local.stateful_nodegroup_config.source_security_group_ids
      additional_tags           = {}
      k8s_labels                = { stateful = "true" }
    }
    )
  }

  generic_node_groups = { for i, subnet in local.is_development ? [] : local.stateful_subnets : "stateful-generic-ng-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = "stateful-generic-ng-${i}"
      subnets                   = [subnet]
      instance_types            = ["m6a.xlarge"]
      desired_capacity          = 0
      min_capacity              = 0
      max_capacity              = 4
      disk_size                 = 25
      source_security_group_ids = local.stateful_nodegroup_config.source_security_group_ids
      additional_tags           = {}
      k8s_labels                = { stateful = "true" }
      taints = {
        stateful = {
          key    = "stateful"
          value  = "true"
          effect = "NO_SCHEDULE"
        }
      }
    }
  ) }

  elasticsearch_node_groups = { for i, subnet in local.is_development ? [] : local.stateful_subnets : "elasticsearch-ng-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = "elasticsearch-ng-${i}"
      subnets                   = [subnet]
      instance_types            = ["r6a.2xlarge"]
      desired_capacity          = 0
      min_capacity              = 0
      max_capacity              = 4
      disk_size                 = 25
      source_security_group_ids = local.stateful_nodegroup_config.source_security_group_ids
      additional_tags           = {}
      k8s_labels                = { elasticsearch = "true" }
      taints = {
        stateful = {
          key    = "elasticsearch"
          value  = "true"
          effect = "NO_SCHEDULE"
        }
      }
    }
  ) }

  k8ssandra_node_groups = { for i, subnet in local.stateful_subnets : local.is_development ? "k8ssandra-${i}" : "k8ssandra-ng-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = local.is_development ? "k8ssandra-${i}" : "k8ssandra-ng-${i}"
      subnets                   = [subnet]
      instance_types            = local.is_development ? ["r6a.xlarge"] : ["m6a.2xlarge"]
      desired_capacity          = 0
      min_capacity              = 0
      max_capacity              = 4
      disk_size                 = 25
      source_security_group_ids = local.stateful_nodegroup_config.source_security_group_ids
      additional_tags           = {}
      k8s_labels                = { k8ssandra = "true" }
      taints = {
        stateful = {
          key    = "k8ssandra"
          value  = "true"
          effect = "NO_SCHEDULE"
        }
      }
    }
  ) }

  caching_node_groups = { for i, subnet in local.is_development ? [] : slice(module.vpc.private_subnets, 0, 3) : "caching-ng-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = "caching-ng-${i}"
      subnets                   = [subnet]
      instance_types            = ["r6a.large"]
      desired_capacity          = 0
      min_capacity              = 0
      max_capacity              = 4
      disk_size                 = 25
      source_security_group_ids = local.stateful_nodegroup_config.source_security_group_ids
      additional_tags           = {}
      k8s_labels                = { caching = "true" }
      taints = {
        stateful = {
          key    = "caching"
          value  = "true"
          effect = "NO_SCHEDULE"
        }
      }
    }
  ) }

  monitoring_node_groups = { for i, subnet in local.is_development ? [] : [module.vpc.private_subnets[0]] : "monitoring-ng-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = "monitoring-ng-${i}"
      subnets                   = [subnet]
      instance_types            = ["r6a.xlarge"]
      desired_capacity          = 1
      min_capacity              = 1
      max_capacity              = 4
      disk_size                 = 25
      source_security_group_ids = local.stateful_nodegroup_config.source_security_group_ids
      additional_tags           = {}
      k8s_labels                = { monitoring = "true" }
      taints = {
        stateful = {
          key    = "monitoring"
          value  = "true"
          effect = "NO_SCHEDULE"
        }
      }
    }
  ) }

  gpu_node_groups = { for i, subnet in local.is_development ? [] : [module.vpc.private_subnets[0]] : "gpu-${i}" => merge(
    local.is_development ? { iam_role_arn = local.nodegroup_cluster_role } : {},
    {
      name                      = "gpu-${i}"
      subnets                   = [subnet]
      instance_types            = ["g4dn.xlarge"]
      ami_type                  = "AL2_x86_64_GPU"
      desired_capacity          = 1
      min_capacity              = 1
      max_capacity              = 1
      disk_size                 = 25
      source_security_group_ids = local.stateful_nodegroup_config.source_security_group_ids
      additional_tags           = {}
      k8s_labels                = { gpu = "true" }
      taints = {
        stateful = {
          key    = "gpu"
          value  = "true"
          effect = "NO_SCHEDULE"
        }
      }
    }
  ) }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.1.0"
  tags    = {}

  cluster_name    = local.cluster_name
  cluster_version = var.eks_version[terraform.workspace]
  node_groups = merge(
    local.stateless_nodegroups,
    local.stateful_nodegroups,
    local.k8ssandra_node_groups,
    local.elasticsearch_node_groups,
    local.generic_node_groups,
    local.caching_node_groups,
    local.gpu_node_groups
  )

  // Network
  vpc_id  = module.vpc.vpc_id
  subnets = local.cluster_subnets

  // Do not create local Kubeconfig
  write_kubeconfig = false

  // Do not manage aws-auth ConfigMap
  manage_aws_auth = false

  // Temporary Sandbox-only attributes
  manage_cluster_iam_resources = !local.is_development
  cluster_iam_role_name        = local.is_development ? "eksctl-sandbox-cluster-ServiceRole-1TXAVBPU5LW7Q" : ""

  manage_worker_iam_resources = !local.is_development

  cluster_create_security_group = !local.is_development
  cluster_security_group_id     = local.is_development ? "sg-0d23588f13dd9715a" : ""

  worker_create_security_group = !local.is_development
  worker_security_group_id     = local.is_development ? "sg-0083deb15e6d0ed52" : ""
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  provider = aws.eks
  name     = module.eks.cluster_id
}


// TODO upgrade EKS module, it will simplify this and the other addons we use.
resource "aws_eks_addon" "eks_ebs_csi" {
  cluster_name  = module.eks.cluster_id
  addon_name    = "aws-ebs-csi-driver"
  addon_version = var.eks_ebs_csi_addon_version[terraform.workspace]
}
