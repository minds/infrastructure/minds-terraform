variable "gcp_project_id" {
  type        = string
  description = "The name of gcp project"
}

variable "gcp_region" {
  type        = string
  description = "The region of the gcp project"
}

variable "gke_cluster_name" {
  type        = string
  description = "The name of the GKE cluster"
}

variable "gke_cluster_location" {
  type        = string
  description = "The zone of the GKE cluster"
}