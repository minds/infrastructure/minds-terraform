provider "google" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

provider "google-beta" {
  project = var.gcp_project_id
  region  = var.gcp_region
}

data "google_container_cluster" "primary" {
  name     = var.gke_cluster_name
  location = var.gke_cluster_location
}

data "google_client_config" "provider" {
}


provider "kubernetes" {
  host                   = data.google_container_cluster.primary.endpoint
  token                  = data.google_client_config.provider.access_token
  cluster_ca_certificate = base64decode(data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate)
  #alias = "k8s_gke"
}

provider "helm" {
  kubernetes {
    host                   = data.google_container_cluster.primary.endpoint
    token                  = data.google_client_config.provider.access_token
    cluster_ca_certificate = base64decode(data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate)
  }
}


output "k8s_host" {
  value = data.google_container_cluster.primary.endpoint
}

output "k8s_token" {
  value = data.google_client_config.provider.access_token
}

output "k8s_cert" {
  value = base64decode(data.google_container_cluster.primary.master_auth[0].cluster_ca_certificate)
}