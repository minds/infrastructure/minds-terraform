variable "eks_cluster_name" {
  type = string
}

variable "kubernetes_namespace" {
  type = string
}

#

variable "data_replicas" {
  type = number
}

variable "master_replicas" {
  type = number
}

variable "client_replicas" {
  type = number
}

#

variable "data_jvm_memory_gb" {
  type = number
}

variable "master_jvm_memory_gb" {
  type = number
}

#

variable "security_nodes_dn" {
  type = string
}

variable "security_admin_dn" {
  type = string
}

variable "reindex_host" {
  type    = string
  default = ""
}

#

variable "values" {
  type = list(string)
}
