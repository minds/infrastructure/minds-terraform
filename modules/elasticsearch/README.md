# Minds ElasticSearch

This module configures an ElasticSearch cluster on Kubernetes.

## Download the opendistro repository

`git clone https://github.com/opendistro-for-elasticsearch/opendistro-build`


## Inputs

| Variable         | Description                                                | Default |
| ---------------- | ---------------------------------------------------------- | ------- |
| eks_cluster_name | The name/id of the EKS cluster to install elasticsearch to |         |
