provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "elasticsearch/terraform.tfstate"
  }
}

data "aws_eks_cluster" "cluster" {
  name = var.eks_cluster_name
}

data "aws_eks_cluster_auth" "cluster" {
  name = var.eks_cluster_name
}

resource "helm_release" "elasticsearch" {
  name      = "elasticsearch"
  chart     = "${path.module}/opendistro-es-1.13.3.tgz"
  namespace = var.kubernetes_namespace

  timeout = 1800

  set {
    name  = "global.clusterName"
    value = "minds-k8s"
  }

  set {
    name  = "global.imageRegistry"
    value = "public.ecr.aws/t7r8r4d6"
  }


  ##
  # Master
  ##
  set {
    name  = "elasticsearch.master.replicas"
    value = var.master_replicas
  }

  set {
    name  = "elasticsearch.master.persistence.size"
    value = "256Gi"
  }


  set {
    name  = "elasticsearch.master.javaOpts"
    value = "-Xms${var.master_jvm_memory_gb}G -Xmx${var.master_jvm_memory_gb}G"
  }

  set {
    name  = "elasticsearch.master.resources.requests.memory"
    value = "${var.master_jvm_memory_gb * 2}Gi"
  }

  set {
    name  = "elasticsearch.master.resources.limits.memory"
    value = "${var.master_jvm_memory_gb * 2}Gi"
  }

  set {
    name  = "elasticsearch.master.resources.requests.cpu"
    value = "1"
  }

  set {
    name  = "elasticsearch.master.resources.limits.cpu"
    value = "2"
  }

  ##
  # Client
  ##
  set {
    name  = "elasticsearch.client.replicas"
    value = var.client_replicas
  }


  set {
    name  = "elasticsearch.client.javaOpts"
    value = "-Xms2G -Xmx2G"
  }

  set {
    name  = "elasticsearch.client.resources.requests.memory"
    value = "2Gi"
  }

  set {
    name  = "elasticsearch.client.resources.limits.memory"
    value = "4Gi"
  }

  set {
    name  = "elasticsearch.client.resources.requests.cpu"
    value = "600m"
  }

  set {
    name  = "elasticsearch.client.resources.limits.cpu"
    value = "1"
  }

  set {
    name  = "elasticsearch.client.service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "elasticsearch.client.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-internal"
    value = "true"
    type  = "string"
  }

  set {
    name  = "elasticsearch.client.service.annotations.service\\.beta\\.kubernetes\\.io/aws-load-balancer-type"
    value = "nlb"
  }

  ##
  # Data
  ##
  set {
    name  = "elasticsearch.data.replicas"
    value = var.data_replicas
  }

  set {
    name  = "elasticsearch.data.persistence.size"
    value = "256Gi"
  }

  set {
    name  = "elasticsearch.data.javaOpts"
    value = "-Xms${var.data_jvm_memory_gb}G -Xmx${var.data_jvm_memory_gb}G"
  }

  set {
    name  = "elasticsearch.data.resources.requests.memory"
    value = "${var.data_jvm_memory_gb * 2}Gi"
  }

  set {
    name  = "elasticsearch.data.resources.limits.memory"
    value = "${var.data_jvm_memory_gb * 2}Gi"
  }

  set {
    name = "elasticsearch.data.resources.requests.cpu"
    #value = "${var.data_jvm_memory_gb / 5}" # 0.2 cpus per GB of RAM
    value = "4"
  }

  set {
    name = "elasticsearch.data.resources.limits.cpu"
    #value = "${var.data_jvm_memory_gb / 3}"
    value = "6"
  }

  ##

  values = concat(
    var.values,
    [
      <<EOT
elasticsearch:
  ssl:
    transport:
      existingCertSecret: elasticsearch-transport-certs
      existingCertSecretCertSubPath: tls.crt
      existingCertSecretKeySubPath: tls.key
      existingCertSecretRootCASubPath: ca.crt
    rest:
      enabled: true
      existingCertSecret: elasticsearch-rest-certs
      existingCertSecretCertSubPath: tls.crt
      existingCertSecretKeySubPath: tls.key
      existingCertSecretRootCASubPath: ca.crt
    admin:
      enabled: true
      existingCertSecret: elasticsearch-admin-certs
      existingCertSecretCertSubPath: tls.crt
      existingCertSecretKeySubPath: tls.key
      existingCertSecretRootCASubPath: ca.crt

  securityConfig:
    enabled: true
    path: "/usr/share/elasticsearch/plugins/opendistro_security/securityconfig"
    config:
      data:
        internal_users.yml: |-
          _meta:
            type: "internalusers"
            config_version: 2

          admin:
            hash: "$2y$12$TKEIHWwDfgfxQE2LxByvbeDMhJ3357YCV4R0TGTusFtUqQIA7QRKa"
            reserved: false
            backend_roles:
            - "admin"
            description: "Minds admin user"

  config:
    opendistro_security.audit.ignore_users: ["kibanaserver"]
    opendistro_security.allow_unsafe_democertificates: false
    opendistro_security.allow_default_init_securityindex: true
    opendistro_security.audit.type: internal_elasticsearch
    opendistro_security.roles_mapping_resolution: BOTH
    opendistro_security.restapi.roles_enabled: ["all_access", "security_rest_api_access"]

    opendistro_security.nodes_dn:
      - '${var.security_nodes_dn}'

    thread_pool.search.queue_size: 5500
    thread_pool.search.min_queue_size: 5000
    thread_pool.search.max_queue_size: 6000

    thread_pool.write.queue_size: 2000

    indices.queries.cache.size: 20%

    # TLS Configuration Transport Layer
    opendistro_security.ssl.transport.pemcert_filepath: elk-transport-crt.pem
    opendistro_security.ssl.transport.pemkey_filepath: elk-transport-key.pem
    opendistro_security.ssl.transport.pemtrustedcas_filepath: elk-transport-root-ca.pem
    opendistro_security.ssl.transport.enforce_hostname_verification: false

    # TLS Configuration REST Layer
    opendistro_security.ssl.http.enabled: true
    opendistro_security.ssl.http.pemcert_filepath: elk-rest-crt.pem
    opendistro_security.ssl.http.pemkey_filepath: elk-rest-key.pem
    opendistro_security.ssl.http.pemtrustedcas_filepath: elk-rest-root-ca.pem
    opendistro_security.ssl.transport.truststore_filepath: opendistro-es.truststore

    opendistro_security.authcz.admin_dn:
      - ${var.security_admin_dn}

    reindex.remote.whitelist: ${var.reindex_host}
EOT
  ])

}

