terraform {
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = "4.93.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "4.50.0"
    }
    local = {
      source = "hashicorp/local"
    }
  }
}

// TODO this is wrong, we need to auth with OKE within the OCI provider
provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = var.k8s_context[var.workspace]
}
