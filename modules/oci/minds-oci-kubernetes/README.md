## Taint Nodes

```
k taint nodes `k get no -l minds.com/node-type=stateful-vitess -o name` minds.com/node-type=stateful-vitess:NoSchedule
k taint nodes `k get no -l minds.com/node-type=stateful-pulsar -o name` minds.com/node-type=stateful-pulsar:NoSchedule
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.50.0 |
| <a name="requirement_oci"></a> [oci](#requirement\_oci) | 4.93.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.50.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.18.1 |
| <a name="provider_oci"></a> [oci](#provider\_oci) | 4.93.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.4.3 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_oke_main"></a> [oke\_main](#module\_oke\_main) | oracle-terraform-modules/oke/oci | 4.5.2 |

## Resources

| Name | Type |
|------|------|
| [kubernetes_storage_class.high_performance](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/storage_class) | resource |
| [oci_core_network_security_group_security_rule.oke_main_control_plane_ingress](https://registry.terraform.io/providers/oracle/oci/4.93.0/docs/resources/core_network_security_group_security_rule) | resource |
| [oci_core_network_security_group_security_rule.oke_main_pod_ingress](https://registry.terraform.io/providers/oracle/oci/4.93.0/docs/resources/core_network_security_group_security_rule) | resource |
| [random_string.vcn_oke_main_suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [aws_nat_gateway.eks_ops_nat](https://registry.terraform.io/providers/hashicorp/aws/4.50.0/docs/data-sources/nat_gateway) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_autoscaler_pools"></a> [autoscaler\_pools](#input\_autoscaler\_pools) | Object containing autoscaler node pool configuration for OKE Main. | `any` | `{}` | no |
| <a name="input_enable_calico"></a> [enable\_calico](#input\_enable\_calico) | Enable Calico for NetworkPolicy usage. | `bool` | `false` | no |
| <a name="input_enable_cluster_autoscaler"></a> [enable\_cluster\_autoscaler](#input\_enable\_cluster\_autoscaler) | Enable cluster autoscaler. | `bool` | `false` | no |
| <a name="input_enable_hp_storage_class"></a> [enable\_hp\_storage\_class](#input\_enable\_hp\_storage\_class) | Enable High Performance storage class. | `bool` | `true` | no |
| <a name="input_k8s_context"></a> [k8s\_context](#input\_k8s\_context) | Kubernetes context to use | `map(string)` | <pre>{<br>  "default": "context-c2nnvhcgaha",<br>  "ops": "context-cyiarjo4tsa",<br>  "sandbox": "context-cyiarjo4tsa"<br>}</pre> | no |
| <a name="input_oci_compartment_ocid"></a> [oci\_compartment\_ocid](#input\_oci\_compartment\_ocid) | Compartment ID for the target Oracle cloud compartment. | `map(string)` | <pre>{<br>  "default": "ocid1.compartment.oc1..aaaaaaaazlkrijkglvvtye3vxhqsfwdn6gazaj26hjsgwref6gdsgdwjjcza",<br>  "ops": "ocid1.compartment.oc1..aaaaaaaazlkrijkglvvtye3vxhqsfwdn6gazaj26hjsgwref6gdsgdwjjcza",<br>  "sandbox": "ocid1.compartment.oc1..aaaaaaaaeveizn2oeenmh264tdv7hpx63r45lmzas77alwhibf3ryof2ujkq"<br>}</pre> | no |
| <a name="input_oci_region"></a> [oci\_region](#input\_oci\_region) | Target OCI region. | `string` | `"us-ashburn-1"` | no |
| <a name="input_oci_tenancy_ocid"></a> [oci\_tenancy\_ocid](#input\_oci\_tenancy\_ocid) | Tenancy ID for the target Oracle cloud account. | `string` | `"ocid1.tenancy.oc1..aaaaaaaa24wrydmie6u35gxqyeqnt2g4swp2kge3jomukit34tjy6kokqknq"` | no |
| <a name="input_oke_main_nat_gateway_route_rules"></a> [oke\_main\_nat\_gateway\_route\_rules](#input\_oke\_main\_nat\_gateway\_route\_rules) | List of objects containing NAT Gateway route rules for OKE Main. | `any` | `[]` | no |
| <a name="input_oke_main_node_pools"></a> [oke\_main\_node\_pools](#input\_oke\_main\_node\_pools) | Object containing node pool configuration for OKE Main. | `any` | `{}` | no |
| <a name="input_oke_main_vcn_cidrs"></a> [oke\_main\_vcn\_cidrs](#input\_oke\_main\_vcn\_cidrs) | CIDR ranges to create for OKE Main VCN in OCI. | `map(list(string))` | <pre>{<br>  "default": [<br>    "10.7.0.0/16"<br>  ],<br>  "ops": [<br>    "10.12.0.0/16"<br>  ],<br>  "sandbox": [<br>    "10.7.0.0/16"<br>  ]<br>}</pre> | no |
| <a name="input_oke_main_vcn_dns_label"></a> [oke\_main\_vcn\_dns\_label](#input\_oke\_main\_vcn\_dns\_label) | DNS label for OKE Main VCN. | `map(string)` | <pre>{<br>  "default": "mindscom",<br>  "ops": "opsmindscom",<br>  "sandbox": "mindsio"<br>}</pre> | no |
| <a name="input_oke_main_vcn_pod_cidr"></a> [oke\_main\_vcn\_pod\_cidr](#input\_oke\_main\_vcn\_pod\_cidr) | CIDR range for K8s Pod IP's. | `map(string)` | <pre>{<br>  "default": "10.7.128.0/18",<br>  "ops": "10.12.128.0/18",<br>  "sandbox": "10.7.128.0/18"<br>}</pre> | no |
| <a name="input_oke_main_vpn_route_cidr"></a> [oke\_main\_vpn\_route\_cidr](#input\_oke\_main\_vpn\_route\_cidr) | CIDR range for route table to map to VPN | `map(string)` | <pre>{<br>  "default": "10.2.0.0/16",<br>  "ops": "10.2.0.0/16",<br>  "sandbox": "192.168.0.0/16"<br>}</pre> | no |
| <a name="input_oke_main_vpn_route_drg"></a> [oke\_main\_vpn\_route\_drg](#input\_oke\_main\_vpn\_route\_drg) | The DRG to point to | `map(string)` | <pre>{<br>  "default": "ocid1.drg.oc1.iad.aaaaaaaa4vg7zgi4r5o5vofek5tygasxa67qfldkiofawqud745nsgzocsfq",<br>  "ops": "ocid1.drg.oc1.iad.aaaaaaaa4vg7zgi4r5o5vofek5tygasxa67qfldkiofawqud745nsgzocsfq",<br>  "sandbox": "ocid1.drg.oc1.iad.aaaaaaaa4o56i2comexo5ovmqjtkpzxx4gjbsloc35uhemi3s2ir4x7l3pra"<br>}</pre> | no |
| <a name="input_ssh_private_key_path"></a> [ssh\_private\_key\_path](#input\_ssh\_private\_key\_path) | Path to local SSH private key for connecting to bastion and operator instances. | `string` | `"~/.ssh/oke"` | no |
| <a name="input_ssh_public_key_path"></a> [ssh\_public\_key\_path](#input\_ssh\_public\_key\_path) | Path to local SSH public key for connecting to bastion and operator instances. | `string` | `"~/.ssh/oke.pub"` | no |
| <a name="input_workspace"></a> [workspace](#input\_workspace) | n/a | `string` | `"sandbox"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_oke_main_vcn_id"></a> [oke\_main\_vcn\_id](#output\_oke\_main\_vcn\_id) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
