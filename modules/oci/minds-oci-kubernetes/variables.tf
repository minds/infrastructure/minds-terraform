###########
### OCI ###
###########

variable "workspace" {
  default = "sandbox"
}

variable "oci_region" {
  type        = string
  description = "Target OCI region."

  default = "us-ashburn-1"
}

variable "oci_tenancy_ocid" {
  type        = string
  description = "Tenancy ID for the target Oracle cloud account."

  default = "ocid1.tenancy.oc1..aaaaaaaa24wrydmie6u35gxqyeqnt2g4swp2kge3jomukit34tjy6kokqknq" # mindsoci
}

variable "oci_compartment_ocid" {
  type        = map(string)
  description = "Compartment ID for the target Oracle cloud compartment."

  default = {
    ops     = "ocid1.compartment.oc1..aaaaaaaazlkrijkglvvtye3vxhqsfwdn6gazaj26hjsgwref6gdsgdwjjcza" # minds-com
    default = "ocid1.compartment.oc1..aaaaaaaazlkrijkglvvtye3vxhqsfwdn6gazaj26hjsgwref6gdsgdwjjcza" # minds-com
    sandbox = "ocid1.compartment.oc1..aaaaaaaaeveizn2oeenmh264tdv7hpx63r45lmzas77alwhibf3ryof2ujkq" # minds-sandbox
  }
}

################
### OKE Main ###
################

variable "oke_main_vcn_dns_label" {
  type        = map(string)
  description = "DNS label for OKE Main VCN."

  default = {
    ops     = "opsmindscom"
    default = "mindscom"
    sandbox = "mindsio"
  }
}

variable "oke_main_vcn_cidrs" {
  type        = map(list(string))
  description = "CIDR ranges to create for OKE Main VCN in OCI."

  default = {
    ops     = ["10.12.0.0/16"]
    default = ["10.7.0.0/16"]
    sandbox = ["10.7.0.0/16"]
  }
}

variable "oke_main_vcn_pod_cidr" {
  type        = map(string)
  description = "CIDR range for K8s Pod IP's."

  default = {
    ops     = "10.12.128.0/18"
    default = "10.7.128.0/18"
    sandbox = "10.7.128.0/18"
  }
}

variable "oke_main_vpn_route_cidr" {
  type        = map(string)
  description = "CIDR range for route table to map to VPN"

  default = {
    ops     = "10.2.0.0/16" // TODO - this is a hack, need to make the route optional
    default = "10.2.0.0/16"
    sandbox = "192.168.0.0/16"
  }
}

variable "oke_main_vpn_route_drg" {
  type        = map(string)
  description = "The DRG to point to"

  default = {
    ops     = "ocid1.drg.oc1.iad.aaaaaaaa4vg7zgi4r5o5vofek5tygasxa67qfldkiofawqud745nsgzocsfq" // TODO - this is a hack, need to make the route optional
    default = "ocid1.drg.oc1.iad.aaaaaaaa4vg7zgi4r5o5vofek5tygasxa67qfldkiofawqud745nsgzocsfq"
    sandbox = "ocid1.drg.oc1.iad.aaaaaaaa4o56i2comexo5ovmqjtkpzxx4gjbsloc35uhemi3s2ir4x7l3pra"
  }
}

variable "oke_main_node_pools" {
  type        = any
  description = "Object containing node pool configuration for OKE Main."

  default = {}
}

variable "oke_main_nat_gateway_route_rules" {
  type        = any
  description = "List of objects containing NAT Gateway route rules for OKE Main."

  default = []
}

variable "autoscaler_pools" {
  type        = any
  description = "Object containing autoscaler node pool configuration for OKE Main."

  default = {}
}

variable "k8s_context" {
  type        = map(string)
  description = "Kubernetes context to use"

  default = {
    ops     = "context-cyiarjo4tsa" // TODO fix
    default = "oke-prod"
    sandbox = "oke-sandbox"
  }
}

variable "enable_hp_storage_class" {
  type        = bool
  description = "Enable High Performance storage class."

  default = true
}

// TODO, better manage SSH keys
variable "ssh_private_key_path" {
  type        = string
  description = "Path to local SSH private key for connecting to bastion and operator instances."

  default = "~/.ssh/oke"
}

variable "ssh_public_key_path" {
  type        = string
  description = "Path to local SSH public key for connecting to bastion and operator instances."

  default = "~/.ssh/oke.pub"
}

variable "enable_calico" {
  description = "Enable Calico for NetworkPolicy usage."
  type        = bool
  default     = false
}

variable "enable_cluster_autoscaler" {
  description = "Enable cluster autoscaler."
  type        = bool
  default     = false
}

variable "create_fss" {
  description = "Create File Storage Service."
  type        = bool
  default     = false
}