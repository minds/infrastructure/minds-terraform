resource "random_string" "vcn_oke_main_suffix" {
  length  = 6
  special = false
}

locals {
  vcn_oke_main_name = "${var.oke_main_vcn_dns_label[var.workspace]}-oke-${random_string.vcn_oke_main_suffix.result}"
}

################
### OKE Main ###
################

module "oke_main" {
  source  = "oracle-terraform-modules/oke/oci"
  version = "4.5.2"

  providers = { oci.home = oci.home }

  home_region = var.oci_region
  region      = var.oci_region

  compartment_id = var.oci_compartment_ocid[var.workspace]
  tenancy_id     = var.oci_tenancy_ocid

  // Networking
  vcn_name      = local.vcn_oke_main_name
  vcn_dns_label = var.oke_main_vcn_dns_label[var.workspace]
  vcn_cidrs     = var.oke_main_vcn_cidrs[var.workspace]
  cni_type      = "npn" # Native Pod Networking

  # pods_cidr = var.oke_main_vcn_pod_cidr[var.workspace]
  control_plane_allowed_cidrs = [var.oke_main_vcn_pod_cidr[var.workspace]] # Allow Ingress from Pods

  // Control Plane
  label_prefix       = local.vcn_oke_main_name
  kubernetes_version = "v1.29.1"

  // Data Plane
  node_pools           = var.oke_main_node_pools
  node_pool_os_version = "7.9"

  // Fixes DNS issues on sandboxes
  kubeproxy_mode = "ipvs"

  // Routing rules
  nat_gateway_route_rules = concat(
    [
      {
        destination       = var.oke_main_vpn_route_cidr[var.workspace]
        destination_type  = "CIDR_BLOCK",
        network_entity_id = var.oke_main_vpn_route_drg[var.workspace],
        description       = "EKS"
      }
    ],
    var.oke_main_nat_gateway_route_rules
  )

  // Admin
  # SSH keys for use with bastion and operator
  ssh_private_key_path = pathexpand(var.ssh_private_key_path)
  ssh_public_key_path  = pathexpand(var.ssh_public_key_path)

  allow_worker_ssh_access = true

  # Bastion host, used for kubectl and SSH access to worker nodes
  create_bastion_host = true

  # Operator, used by the Terraform module for deploying addons and other admin tasks
  create_operator = true

  // Other options
  enable_calico             = var.enable_calico
  enable_cluster_autoscaler = var.enable_cluster_autoscaler
  create_fss = var.create_fss

  // Override default tags
  freeform_tags = {
    vcn = {
      environment = "dev"
    }
    bastion = {
      environment = "dev"
      role        = "bastion"
    }
    operator = {
      environment = "dev"
      role        = "operator"
    }
    oke = {
      cluster = {
        environment = "dev"
      }
      persistent_volume = {
        environment = "dev"
      }
      service_lb = {
        environment = "dev"
        role        = "load balancer"
      }
      node_pool = {}
      node      = {} // Needed for autoscaler
    }
  }
}

####################################
### Network Security Group Rules ###
####################################

// Allow Ingress from other Pods

resource "oci_core_network_security_group_security_rule" "oke_main_pod_ingress" {
  network_security_group_id = module.oke_main.nsg_ids.pods
  direction                 = "INGRESS"
  protocol                  = "all"

  description = "Allow ingress from other pods."

  stateless = false
  source    = var.oke_main_vcn_pod_cidr[var.workspace]
}

// Allow from EKS Ops NAT gateway

# data "aws_nat_gateway" "eks_ops_nat" {
#   tags = { Application = "ops" }
# }

# resource "oci_core_network_security_group_security_rule" "oke_main_control_plane_ingress" {
#   network_security_group_id = module.oke_main.nsg_ids.cp
#   direction                 = "INGRESS"
#   protocol                  = "all"

#   description = "Allow Ingress from EKS Ops NAT"

#   stateless = false
#   source    = "${data.aws_nat_gateway.eks_ops_nat.public_ip}/32"
# }

// StorageClass for high performance volumes

resource "kubernetes_storage_class" "high_performance" {

  count = var.enable_hp_storage_class ? 1 : 0

  metadata {
    name = "high-performance"
  }

  storage_provisioner    = "blockvolume.csi.oraclecloud.com"
  reclaim_policy         = "Delete"
  volume_binding_mode    = "WaitForFirstConsumer"
  allow_volume_expansion = true

  parameters = {
    vpusPerGB = "20"
  }
}
