output "aws_customer_gateway" {
  value = var.use_aws_main_minds ? aws_customer_gateway.customer_gateway[0].id : ""
}

output "drg_eks" {
  value = oci_core_drg.oci_drg_eks.id
}
