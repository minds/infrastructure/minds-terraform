######################################
### MainMinds VPC --> OKE Main VCN ###
######################################

// Temporary Customer Gateway

# "OCI requires the public IP of the remote VPN peer before creating a IPSec connection. 
# After this process has been completed, a new customer gateway is configured representing 
# the actual OCI VPN endpoint public IP."

resource "aws_customer_gateway" "tmp_customer_gateway" {
  bgp_asn    = "31898"   # This ASN is used in all OCI regions
  ip_address = "1.1.1.1" # Any valid ip address

  # We want it to be the below, but we get circle issue....
  #ip_address = oci_core_ipsec_connection_tunnel_management.tunnel_1.vpn_ip
  type = "ipsec.1"

  tags = {
    Name = "tmp-aws-cgw"
  }
}

resource "aws_vpn_connection" "vpn_connection" {
  count = var.use_aws_main_minds ? 1 : 0

  vpn_gateway_id      = "vgw-0c397ed3f16c4c4dc"
  customer_gateway_id = aws_customer_gateway.tmp_customer_gateway.id
  type                = "ipsec.1"
  static_routes_only  = false

  tags = {
    Name = "${var.aws_main_minds_vpc_id}-oci-vpn"
  }

  lifecycle {
    ignore_changes = [
      customer_gateway_id # must be changed to aws_customer_gateway.customer_gateway.id
    ]
  }
}

resource "aws_customer_gateway" "customer_gateway" {
  count = var.use_aws_main_minds ? 1 : 0

  bgp_asn    = "31898"
  ip_address = oci_core_ipsec_connection_tunnel_management.tunnel_1[0].vpn_ip
  type       = "ipsec.1"

  tags = {
    Name = "aws-oci-customer-gateway"
  }
}

################################
### OKE Main VCN --> EKS VPC ###
################################

resource "aws_customer_gateway" "customer_gateway_eks" {
  bgp_asn    = "31898"
  ip_address = oci_core_ipsec_connection_tunnel_management.tunnel_1_eks.vpn_ip
  type       = "ipsec.1"
  tags = {
    Name = "${var.aws_eks_vpc_id}-oci-customer-gateway-eks"
  }
}

resource "aws_vpn_connection" "vpn_connection_eks" {
  vpn_gateway_id      = var.deploy_aws_eks_vpn_gateway ? aws_vpn_gateway.eks_vpc[0].id : data.aws_vpn_gateway.eks_vpc.id
  customer_gateway_id = aws_customer_gateway.tmp_customer_gateway.id
  type                = "ipsec.1"
  static_routes_only  = false

  /*
    https://docs.oracle.com/en-us/iaas/Content/Network/Tasks/settingupIPsec.htm#Before
  */
  tunnel1_inside_cidr = "169.254.8.0/30"
  tunnel2_inside_cidr = "169.254.9.0/30"

  tags = {
    Name = "${var.aws_eks_vpc_id}-oci-vpn"
  }

  lifecycle {
    ignore_changes = [
      customer_gateway_id # must be changed to aws_customer_gateway.customer_gateway.id
    ]
  }
}
