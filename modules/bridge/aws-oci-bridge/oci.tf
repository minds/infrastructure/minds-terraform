#############################
### minds-com Compartment ###
#############################

// Customer Premises Equipment (CPE)

# Equivalent of the Customer Gateway in AWS

resource "oci_core_cpe" "oci_cpe" {
  count = var.use_aws_main_minds ? 1 : 0

  compartment_id = var.oci_compartment_ocid
  ip_address     = aws_vpn_connection.vpn_connection[0].tunnel1_address # this could be wrong
  display_name   = "to_aws"
}

// Dynamic Routing Gateway (DRG)

# Equivalent of the Virtual Private Gateway in AWS

resource "oci_core_drg" "oci_drg" {
  count = var.use_aws_main_minds ? 1 : 0

  compartment_id = var.oci_compartment_ocid
}

// *** EKS *** //

resource "oci_core_cpe" "oci_cpe_eks" {
  compartment_id = var.oci_compartment_ocid
  ip_address     = aws_vpn_connection.vpn_connection_eks.tunnel1_address
  display_name   = "to_aws_eks"
}

resource "oci_core_drg" "oci_drg_eks" {
  compartment_id = var.oci_compartment_ocid
}
