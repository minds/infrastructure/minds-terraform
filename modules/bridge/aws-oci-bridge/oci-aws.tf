######################################
### OKE Main VCN --> MainMinds VPC ###
######################################

// IPSec Connection

resource "oci_core_ipsec" "oci_ip_sec_connection" {
  count = var.use_aws_main_minds ? 1 : 0

  compartment_id = var.oci_compartment_ocid
  cpe_id         = oci_core_cpe.oci_cpe[0].id
  drg_id         = oci_core_drg.oci_drg[0].id
  static_routes  = ["0.0.0.0/0"]
  display_name   = "oci-to-aws-vpn"
}

// Tunnels

/* 
  Both tunnels are created alongside the IPSec connection. 
  We need to first create the connection with static routes, and then modify the tunnels with the AWS VPN details.
*/

data "oci_core_ipsec_connection_tunnels" "oci_tunnels" {
  count = var.use_aws_main_minds ? 1 : 0

  ipsec_id = oci_core_ipsec.oci_ip_sec_connection[0].id
}

// Tunnel 1

resource "oci_core_ipsec_connection_tunnel_management" "tunnel_1" {
  count = var.use_aws_main_minds ? 1 : 0

  ipsec_id  = oci_core_ipsec.oci_ip_sec_connection[0].id
  tunnel_id = data.oci_core_ipsec_connection_tunnels.oci_tunnels[0].ip_sec_connection_tunnels[0].id
  routing   = "BGP"
  bgp_session_info {
    customer_bgp_asn      = aws_vpn_connection.vpn_connection[0].tunnel1_bgp_asn
    customer_interface_ip = "${aws_vpn_connection.vpn_connection[0].tunnel1_vgw_inside_address}/30"
    oracle_interface_ip   = "${aws_vpn_connection.vpn_connection[0].tunnel1_cgw_inside_address}/30"
  }
  shared_secret = aws_vpn_connection.vpn_connection[0].tunnel1_preshared_key
  display_name  = "tunnel-1"
}

// Tunnel 2

resource "oci_core_ipsec_connection_tunnel_management" "tunnel_2" {
  count = var.use_aws_main_minds ? 1 : 0

  ipsec_id  = oci_core_ipsec.oci_ip_sec_connection[0].id
  tunnel_id = data.oci_core_ipsec_connection_tunnels.oci_tunnels[0].ip_sec_connection_tunnels[1].id
  routing   = "BGP"
  bgp_session_info {
    customer_bgp_asn      = aws_vpn_connection.vpn_connection[0].tunnel2_bgp_asn
    customer_interface_ip = "${aws_vpn_connection.vpn_connection[0].tunnel2_vgw_inside_address}/30"
    oracle_interface_ip   = "${aws_vpn_connection.vpn_connection[0].tunnel2_cgw_inside_address}/30"
  }
  shared_secret = aws_vpn_connection.vpn_connection[0].tunnel2_preshared_key
  display_name  = "tunnel-2"
}

// DRG attachment

resource "oci_core_drg_attachment" "oci_drg_attachment" {
  count = var.use_aws_main_minds ? 1 : 0

  drg_id = oci_core_drg.oci_drg[0].id
  network_details {
    id   = var.oke_main_vcn_id
    type = "VCN"
  }
}

################################
### OKE Main VCN --> EKS VPC ###
################################

// IPSec Connection

resource "oci_core_ipsec" "oci_ip_sec_connection_eks" {
  compartment_id = var.oci_compartment_ocid
  cpe_id         = oci_core_cpe.oci_cpe_eks.id
  drg_id         = oci_core_drg.oci_drg_eks.id
  static_routes  = ["0.0.0.0/0"]
  display_name   = "oci-to-eks-vpn"
}

data "oci_core_ipsec_connection_tunnels" "oci_tunnels_eks" {
  ipsec_id = oci_core_ipsec.oci_ip_sec_connection_eks.id
}

// Tunnel 1

resource "oci_core_ipsec_connection_tunnel_management" "tunnel_1_eks" {
  ipsec_id  = oci_core_ipsec.oci_ip_sec_connection_eks.id
  tunnel_id = data.oci_core_ipsec_connection_tunnels.oci_tunnels_eks.ip_sec_connection_tunnels[0].id
  routing   = "BGP"
  bgp_session_info {
    customer_bgp_asn      = aws_vpn_connection.vpn_connection_eks.tunnel1_bgp_asn
    customer_interface_ip = "${aws_vpn_connection.vpn_connection_eks.tunnel1_vgw_inside_address}/30"
    oracle_interface_ip   = "${aws_vpn_connection.vpn_connection_eks.tunnel1_cgw_inside_address}/30"
  }
  shared_secret = aws_vpn_connection.vpn_connection_eks.tunnel1_preshared_key
  display_name  = "tunnel-1"
}

// Tunnel 2

resource "oci_core_ipsec_connection_tunnel_management" "tunnel_2_eks" {
  ipsec_id  = oci_core_ipsec.oci_ip_sec_connection_eks.id
  tunnel_id = data.oci_core_ipsec_connection_tunnels.oci_tunnels_eks.ip_sec_connection_tunnels[1].id
  routing   = "BGP"
  bgp_session_info {
    customer_bgp_asn      = aws_vpn_connection.vpn_connection_eks.tunnel2_bgp_asn
    customer_interface_ip = "${aws_vpn_connection.vpn_connection_eks.tunnel2_vgw_inside_address}/30"
    oracle_interface_ip   = "${aws_vpn_connection.vpn_connection_eks.tunnel2_cgw_inside_address}/30"
  }
  shared_secret = aws_vpn_connection.vpn_connection_eks.tunnel2_preshared_key
  display_name  = "tunnel-2"
}

// DRG attachment

resource "oci_core_drg_attachment" "oci_drg_attachment_eks" {
  drg_id = oci_core_drg.oci_drg_eks.id
  network_details {
    id   = var.oke_main_vcn_id
    type = "VCN"
  }
}
