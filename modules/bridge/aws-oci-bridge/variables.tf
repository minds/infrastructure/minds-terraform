###########
### OCI ###
###########

variable "oci_region" {
  type        = string
  description = "Target OCI region."

  default = "us-ashburn-1"
}

variable "oci_tenancy_ocid" {
  type        = string
  description = "Tenancy ID for the target Oracle cloud account."

  default = "ocid1.tenancy.oc1..aaaaaaaa24wrydmie6u35gxqyeqnt2g4swp2kge3jomukit34tjy6kokqknq" # mindsoci
}

variable "oci_compartment_ocid" {
  type        = string
  description = "Compartment ID for the target Oracle cloud compartment."

  default = "ocid1.compartment.oc1..aaaaaaaazlkrijkglvvtye3vxhqsfwdn6gazaj26hjsgwref6gdsgdwjjcza" # minds-com
}

##################### 
### MainMinds VPC ###
#####################

variable "use_aws_main_minds" {
  type    = bool
  default = true
}

variable "aws_main_minds_vpc_id" {
  type        = string
  description = "Unique ID for the MainMind VPC in AWS."

  // TODO import other VPCs and wire up appropriately
  default = "vpc-e8ce7583"
}

variable "aws_main_minds_route_table_id" {
  type        = string
  description = "Route table for MainMinds VPC."

  default = "rtb-ce4269a5"
}

###############
### EKS VPC ###
###############

variable "aws_eks_vpc_id" {
  type        = string
  description = "Unique ID for the EKS VPC in AWS."

  default = "vpc-0439a28907e3db7a3"
}

variable "aws_eks_route_table_id" {
  type        = string
  description = "Route table for EKS VPC."

  default = "rtb-0a5bc882e71ab4450"
}

variable "deploy_aws_eks_vpn_gateway" {
  type    = bool
  default = false
}

####################
### OKE Main VCN ###
####################

variable "oke_main_vcn_id" {
  type        = string
  description = "Unique ID for the OKE Main VCN."
}
