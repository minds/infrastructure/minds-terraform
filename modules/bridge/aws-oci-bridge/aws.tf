#####################
### MainMinds VPC ###
#####################

// VPN Gateway //

# Note that only one VPN gateway can be created per VPC

resource "aws_vpn_gateway" "main_minds" {
  count = var.use_aws_main_minds ? 1 : 0

  vpc_id = var.aws_main_minds_vpc_id

  tags = {
    Name = "${var.aws_main_minds_vpc_id}-gateway"
  }
}

// Route Table Propogation

# This is needed to dynamically add advertised routes (from other clouds) to the route table for the VPC

resource "aws_vpn_gateway_route_propagation" "main_minds" {
  count = var.use_aws_main_minds ? 1 : 0

  vpn_gateway_id = aws_vpn_gateway.main_minds[0].id
  route_table_id = var.aws_main_minds_route_table_id
}

###############
### EKS VPC ###
###############

data "aws_vpn_gateway" "eks_vpc" {
  filter {
    name   = "tag:Name"
    values = ["aws-gcp-vpn-gateway"]
  }
}

resource "aws_vpn_gateway" "eks_vpc" {
  count = var.deploy_aws_eks_vpn_gateway ? 1 : 0

  vpc_id = var.aws_eks_vpc_id

  tags = {
    Name = "${var.aws_eks_vpc_id}-gateway"
  }
}

resource "aws_vpn_gateway_route_propagation" "eks_vpc" {
  vpn_gateway_id = var.deploy_aws_eks_vpn_gateway ? aws_vpn_gateway.eks_vpc[0].id : data.aws_vpn_gateway.eks_vpc.id
  route_table_id = var.aws_eks_route_table_id
}
