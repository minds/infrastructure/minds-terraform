module "s3_cassandra_backups" {
  source = "terraform-aws-modules/s3-bucket/aws"

  version = "3.5.0"

  bucket = "minds-k8ssandra-backups-${local.environment}"
  acl    = "private"

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
