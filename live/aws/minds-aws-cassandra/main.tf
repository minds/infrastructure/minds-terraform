terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "environments/aws/minds-aws-cassandra/terraform.tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.30.0"
    }
  }
}

provider "aws" { region = "us-east-1" }

locals {
  environment = terraform.workspace == "default" ? "production" : terraform.workspace
}

# data "aws_eks_cluster_auth" "cluster" {
#   name = module.eks.cluster_id
# }

# provider "kubernetes" {
#   host                   = module.eks.cluster_endpoint
#   token                  = data.aws_eks_cluster_auth.cluster.token
#   cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
# }

# provider "helm" {
#   kubernetes {
#     host                   = module.eks.cluster_endpoint
#     token                  = data.aws_eks_cluster_auth.cluster.token
#     cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
#   }
# }
