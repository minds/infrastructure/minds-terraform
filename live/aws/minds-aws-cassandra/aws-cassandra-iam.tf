// Full access

data "aws_iam_policy_document" "k8ssandra_s3" {
  statement {
    actions = ["s3:*"]
    resources = [
      "arn:aws:s3:::${module.s3_cassandra_backups.s3_bucket_id}",
      "arn:aws:s3:::${module.s3_cassandra_backups.s3_bucket_id}/*",
    ]
  }
  statement {
    actions   = ["s3:GetAccountPublicAccessBlock", "s3:HeadBucket"]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "k8ssandra_s3" {
  name   = "k8ssandra-${local.environment}-s3"
  policy = data.aws_iam_policy_document.k8ssandra_s3.json
}

resource "aws_iam_user" "k8ssandra_s3" {
  name = "k8ssandra-${local.environment}-backups"
}

resource "aws_iam_user_policy_attachment" "k8ssandra_s3" {
  user       = aws_iam_user.k8ssandra_s3.name
  policy_arn = aws_iam_policy.k8ssandra_s3.arn
}