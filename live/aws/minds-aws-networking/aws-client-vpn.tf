######################
### AWS Client VPN ###
######################

// *** Production EKS VPC *** //

# Logs

resource "aws_cloudwatch_log_group" "eks_prod_client_vpn" {
  name = "ClientVPN-EKS-Prod"
}

resource "aws_cloudwatch_log_stream" "eks_prod_client_vpn" {
  name           = "ClientVPN-EKS-Prod"
  log_group_name = aws_cloudwatch_log_group.eks_prod_client_vpn.name
}

# Endpoint

resource "aws_ec2_client_vpn_endpoint" "eks_prod" {
  description = "Client VPN Endpoint for EKS Prod VPC"
  # TODO move to Vault
  server_certificate_arn = "arn:aws:acm:us-east-1:${data.aws_caller_identity.current.account_id}:certificate/ac1b5b6b-2eeb-4ca1-ab8d-86b9a47623b2"
  client_cidr_block      = "10.0.0.0/16"

  authentication_options {
    type              = "federated-authentication"
    saml_provider_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:saml-provider/Keycloak"
  }

  connection_log_options {
    enabled               = true
    cloudwatch_log_group  = aws_cloudwatch_log_group.eks_prod_client_vpn.name
    cloudwatch_log_stream = aws_cloudwatch_log_stream.eks_prod_client_vpn.name
  }
}

# Target Associations

resource "aws_ec2_client_vpn_network_association" "eks_prod_client_vpn" {
  // TODO remove hard coded subnets when we migrate prod vpc to this module.
  for_each = toset(["subnet-08e09bdd64528801a", "subnet-0946caee63f08f371", "subnet-03c639f65fa2e175b"])

  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.eks_prod.id
  subnet_id              = each.value
}
