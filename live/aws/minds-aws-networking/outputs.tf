output "eks_vpc_ops" {
  value = {
    name               = module.vpc_eks_ops.name
    id                 = module.vpc_eks_ops.vpc_id
    public_subnet_ids  = module.vpc_eks_ops.public_subnets
    private_subnet_ids = module.vpc_eks_ops.private_subnets
  }
}
