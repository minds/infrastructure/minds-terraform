terraform {
  backend "s3" {
    region = "us-east-1"
    bucket = "minds-terraform"
    key    = "environments/aws/minds-aws-networking/terraform.tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.30.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~>3.4.3"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~>4.0.4"
    }
  }
}

provider "aws" { region = "us-east-1" }
data "aws_caller_identity" "current" {}
