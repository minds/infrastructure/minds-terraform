/*** VPC ***/

// OPS

variable "vpc_name_prefix_ops" {
  type    = string
  default = "minds-eks-ops"
}

variable "vpc_cidr_ops" {
  type    = string
  default = "10.4.0.0/16"
}

variable "vpc_azs_ops" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b", "us-east-1d", "us-east-1e"]
}

variable "vpc_public_subnets_ops" {
  type    = list(string)
  default = ["10.4.0.0/19", "10.4.32.0/19", "10.4.64.0/19", "10.4.96.0/19"]
}

variable "vpc_private_subnets_ops" {
  type    = list(string)
  default = ["10.4.128.0/19", "10.4.160.0/19", "10.4.192.0/19", "10.4.224.0/19"]
}
