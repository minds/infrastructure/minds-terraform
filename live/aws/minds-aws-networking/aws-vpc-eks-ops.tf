resource "random_string" "ops_suffix" {
  length  = 8
  special = false
}

locals {
  vpc_name_ops = "${var.vpc_name_prefix_ops}-${random_string.ops_suffix.result}"
}

module "vpc_eks_ops" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.4"

  name = local.vpc_name_ops

  // Network
  cidr = var.vpc_cidr_ops
  azs  = var.vpc_azs_ops

  public_subnets  = var.vpc_public_subnets_ops
  private_subnets = var.vpc_private_subnets_ops

  enable_nat_gateway   = true
  single_nat_gateway   = true // TODO revisit this, single NAT gateway contributes to cross-az data transfer costs
  enable_dns_hostnames = true

  // Tagging
  tags = {
    "kubernetes.io/cluster/${local.vpc_name_ops}" = "shared"
    "Module"                                      = "environments/aws/minds-aws-networking"
    "Automation"                                  = "terraform"
    "Application"                                 = "ops"
  }
  public_subnet_tags = {
    "kubernetes.io/cluster/${local.vpc_name_ops}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${local.vpc_name_ops}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}
