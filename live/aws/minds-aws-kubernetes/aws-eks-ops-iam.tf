locals {
  issuer_substring = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
}

resource "aws_iam_role" "eks_ops_ebs_csi" {
  name               = "eks-ops-ebs-csi"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.issuer_substring}"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "${local.issuer_substring}:aud": "sts.amazonaws.com",
          "${local.issuer_substring}:sub": "system:serviceaccount:kube-system:ebs-csi-controller-sa"
        }
      }
    }
  ]
}

EOF

  tags = {
    Module      = "environments/aws/minds-aws-kubernetes"
    Automation  = "terraform"
    Application = "ops"
  }
}

resource "aws_iam_role_policy_attachment" "eks_ops_ebs_csi" {
  role       = aws_iam_role.eks_ops_ebs_csi.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
}

resource "kubernetes_storage_class" "eks_ops_ebs_csi" {
  metadata {
    name = "ebs-sc"
  }
  storage_provisioner = "ebs.csi.aws.com"
  reclaim_policy      = "Retain"
  volume_binding_mode = "WaitForFirstConsumer"
}
