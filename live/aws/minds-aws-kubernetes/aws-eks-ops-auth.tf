// TODO build this dynamically
resource "kubernetes_config_map" "aws_auth" {
  metadata {
    namespace = "kube-system"
    name      = "aws-auth"
  }

  data = {
    mapAccounts = <<YAML
[]
YAML
    mapUsers    = <<YAML
[]
YAML
    mapRoles    = <<YAML
- groups:
  - system:bootstrappers
  - system:nodes
  rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/worker-1-eks-node-group-2022090904445502210000000a
  username: system:node:{{EC2PrivateDNSName}}
- groups:
  - system:bootstrappers
  - system:nodes
  rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/worker-0-eks-node-group-20220909044454321200000007
  username: system:node:{{EC2PrivateDNSName}}
- groups:
  - system:bootstrappers
  - system:nodes
  rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/worker-2-eks-node-group-20220909044454321200000006
  username: system:node:{{EC2PrivateDNSName}}
- groups:
  - system:bootstrappers
  - system:nodes
  rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/worker-3-eks-node-group-20220909044454239700000001
  username: system:node:{{EC2PrivateDNSName}}
- groups:
  - system:masters
  rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/minds_eks_k8s_superheroes
  username: superhero-user
- groups:
  - system:masters
  rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/minds_eks_k8s_admins
  username: admin-user
- rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/minds_eks_k8s_developers
  username: dev-user
- rolearn: arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/minds_eks_k8s_devops
  username: devops-user
YAML 
  }
}