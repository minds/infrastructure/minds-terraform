### ALB Controller

resource "aws_iam_policy" "alb_controller_policy" {
  name        = var.alb_controller_policy_name
  path        = "/"
  description = "Minds EKS Controller Policy"
  policy      = file("${path.module}/policies/alb-controller-policy.json")
}

resource "aws_iam_role" "alb_controller_role" {
  name = var.alb_controller_role_name
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.issuer_substring}"
        }
        Condition = {
          StringEquals = {
            "${local.issuer_substring}:sub" : "system:serviceaccount:kube-system:aws-load-balancer-controller"
          }
        }
      },
    ]
  })

  managed_policy_arns = [aws_iam_policy.alb_controller_policy.arn]
}

resource "kubernetes_service_account" "alb_controller_k8s_service_account" {
  metadata {
    name      = "aws-load-balancer-controller"
    namespace = "kube-system"

    labels = {
      "app.kubernetes.io/component" = "controller"
      "app.kubernetes.io/name"      = "aws-load-balancer-controller"
    }

    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.alb_controller_role.arn
    }
  }
}

resource "helm_release" "alb_controller" {
  name = "aws-load-balancer-controller"

  chart      = "aws-load-balancer-controller"
  repository = "https://aws.github.io/eks-charts"
  namespace  = "kube-system"

  set {
    name  = "clusterName"
    value = module.eks.cluster_id
  }

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller"
  }
}
