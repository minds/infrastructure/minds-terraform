/*** EKS ***/

// Ops

variable "cluster_name_ops" {
  description = "Name for the deployed EKS cluster."
  type        = string
}

variable "vpc_id_ops" {
  description = "VPC id in which to deploy the EKS cluster."
  type        = string
}

variable "cluster_subnets_ops" {
  description = "VPC subnets in which to deploy the EKS cluster and node groups."
  type        = list(string)
}

// ALB controller

variable "alb_controller_policy_name" {
  description = "Name for ALB controller IAM policy."
  type        = string

  default = "minds_eks_alb_controller_policy_ops"
}

variable "alb_controller_role_name" {
  description = "Name for ALB controller IAM role."
  type        = string

  default = "minds_eks_alb_controller_role_ops"
}
