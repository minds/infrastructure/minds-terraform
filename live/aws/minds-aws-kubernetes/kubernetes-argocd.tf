resource "kubernetes_namespace" "this" {
  metadata {
    name   = "argo-cd"
    labels = { name = "argo-cd", istio-injection = "enabled" }
  }
}

resource "helm_release" "this" {
  name      = "argo-cd"
  namespace = kubernetes_namespace.this.metadata[0].name

  chart      = "argo-cd"
  repository = "https://argoproj.github.io/argo-helm"
  version    = "5.27.0"

  set {
    name  = "configs.cm.admin\\.enabled"
    value = "false"
    type  = "string"
  }

  set {
    name  = "configs.cm.url"
    value = "https://argo.minds.com"
    type  = "string"
  }

  values = [yamlencode({
    rbac = { create = false }, # Managed in IAM repo
    applicationSet = {
      resources = {
        requests = {
          cpu    = "100m"
          memory = "128Mi"
        }
        limits = {
          cpu    = "100m"
          memory = "128Mi"
        }
      }
    }
    controller = {
      enableStatefulSet = true
      resources = {
        requests = {
          cpu    = "500m"
          memory = "1024Mi"
        }
        limits = {
          cpu    = "1"
          memory = "2048Mi"
        }
      }
    }
    dex = {
      resources = {
        requests = {
          cpu    = "10m"
          memory = "32Mi"
        }
        limits = {
          cpu    = "50m"
          memory = "64Mi"
        }
      }
    }
    notifications = {
      resources = {
        requests = {
          cpu    = "100m"
          memory = "128Mi"
        }
        limits = {
          cpu    = "100m"
          memory = "128Mi"
        }
      }
    }
    repoServer = {
      replicas = 1
      resources = {
        requests = {
          cpu    = "100m"
          memory = "256Mi"
        }
        limits = {
          cpu    = "250m"
          memory = "512Mi"
        }
      }
    }
    server = {
      replicas = 2
      env      = [{ name = "ARGOCD_API_SERVER_REPLICAS", value = "2" }]
      resources = {
        requests = {
          cpu    = "50m"
          memory = "64Mi"
        }
        limits = {
          cpu    = "100m"
          memory = "128Mi"
        }
      }
    }
    redis-ha = {
      enabled = true
      redis = {
        resources = {
          requests = {
            cpu    = "100m"
            memory = "200Mi"
          }
          limits = {
            cpu    = "200m"
            memory = "700Mi"
          }
        }
      }
      haproxy = {
        resources = {
          requests = {
            cpu    = "100m"
            memory = "200Mi"
          }
          limits = {
            cpu    = "250m"
            memory = "700Mi"
          }
        }
      }
    }
  })]
}

resource "kubernetes_manifest" "argocd_appproject_sandbox" {
  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "AppProject"
    metadata = {
      name      = "sandbox"
      namespace = "argo-cd"
    }
    spec = {
      clusterResourceWhitelist = [
        {
          group = "*"
          kind  = "*"
        }
      ]
      destinations = [
        {
          name      = "sandbox"
          namespace = "*"
          server    = "https://DCFFE468F168C0F75BCA2D65CA301B72.gr7.us-east-1.eks.amazonaws.com"
        },
      ]
      sourceRepos = [
        "*",
      ]
    }
  }
}

resource "kubernetes_manifest" "argocd_appproject_production" {
  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "AppProject"
    metadata = {
      name      = "aws-production"
      namespace = "argo-cd"
    }
    spec = {
      clusterResourceWhitelist = [
        {
          group = "*"
          kind  = "*"
        }
      ]
      destinations = [
        {
          name      = "aws-production"
          namespace = "*"
          server    = "https://FD41EC693A70483DD05CE94CB241A56A.gr7.us-east-1.eks.amazonaws.com"
        },
      ]
      sourceRepos = [
        "*",
      ]
    }
  }
}

resource "kubernetes_manifest" "argocd_base_application" {
  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "Application"
    metadata = {
      name      = "apps"
      namespace = "argo-cd"
    }
    spec = {
      destination = {
        name      = "in-cluster"
        namespace = "argo-cd"
      }
      project = "default"
      source = {
        directory = {
          recurse = true
        }
        path    = "apps"
        repoURL = "https://gitlab.com/minds/infrastructure/minds-argocd-applications.git"
      }
    }
  }
}

resource "kubernetes_manifest" "istio_route" {
  manifest = {
    apiVersion = "networking.istio.io/v1beta1"
    kind       = "VirtualService"
    metadata = {
      name      = "argo-cd"
      namespace = kubernetes_namespace.this.metadata[0].name
    }
    spec = {
      gateways = ["istio-system/ingress-gateway"]
      hosts    = ["argo.minds.com"]
      http = [
        {
          name  = "argo-cd"
          match = [{ uri = { prefix = "/" } }]
          route = [
            {
              destination = {
                host = "argo-cd-argocd-server.argo-cd.svc.cluster.local",
                port = { number = 80 } // Istio will encrypt east-west traffic
              }
            }
          ]
        }
      ]
    }
  }
}
