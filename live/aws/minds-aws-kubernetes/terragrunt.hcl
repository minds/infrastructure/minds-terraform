dependency "network" {
  config_path = "../minds-aws-networking"

  mock_outputs = {
    eks_vpc_ops = {
      name               = "temporary-dummy-name"
      id                 = "temporary-dummy-id"
      public_subnet_ids  = ["one", "two", "three"]
      private_subnet_ids = ["four", "five", "six"]
    }
  }
}

inputs = {
  cluster_name_ops    = dependency.network.outputs.eks_vpc_ops.name
  vpc_id_ops          = dependency.network.outputs.eks_vpc_ops.id
  cluster_subnets_ops = dependency.network.outputs.eks_vpc_ops.private_subnet_ids
}
