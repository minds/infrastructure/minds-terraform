resource "aws_kms_key" "eks_ops" {
  description = "KMS key for EKS Ops cluster"
  tags = {
    Module      = "environments/aws/minds-aws-networking"
    Automation  = "terraform"
    Application = "ops"
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.29.0"

  // Network
  vpc_id     = var.vpc_id_ops
  subnet_ids = var.cluster_subnets_ops

  // Control Plane
  cluster_name    = var.cluster_name_ops
  cluster_version = "1.23"

  // Encrypt K8s secrets at rest
  cluster_encryption_config = [{
    provider_key_arn = aws_kms_key.eks_ops.arn
    resources        = ["secrets"]
  }]

  // Addons
  cluster_addons = {
    vpc-cni            = { addon_version = "v1.11.3-eksbuild.1", resolve_conflicts = "OVERWRITE" },
    kube-proxy         = { addon_version = "v1.23.7-eksbuild.1" },
    coredns            = { addon_version = "v1.8.7-eksbuild.2" },
    aws-ebs-csi-driver = { addon_version = "v1.11.2-eksbuild.1" }
  }

  // We'll restrict East/West traffic with NetworkPolicy, so we can allow connections between nodes here
  node_security_group_additional_rules = {
    node_egress = {
      description = "Node to node egress"
      protocol    = "all"
      from_port   = 0
      to_port     = 65535
      type        = "egress"
      self        = true
    },
    node_ingress = {
      description = "Node to node ingress"
      protocol    = "all"
      from_port   = 0
      to_port     = 65535
      type        = "ingress"
      self        = true
    },
    control_plane_webhook = {
      description                   = "Allow ingress from control plane to ALB controller webhook."
      protocol                      = "tcp"
      from_port                     = 9443
      to_port                       = 9443
      type                          = "ingress"
      source_cluster_security_group = true
    },
    istio_webhook = {
      description                   = "Allow ingress from control plane to Istio webhook."
      protocol                      = "tcp"
      from_port                     = 15017
      to_port                       = 15017
      type                          = "ingress"
      source_cluster_security_group = true
    },
    metrics_server = {
      description                   = "Allow ingress from control plane to metrics server."
      protocol                      = "tcp"
      from_port                     = 4443
      to_port                       = 4443
      type                          = "ingress"
      source_cluster_security_group = true
    }
  }

  // Data Plane
  eks_managed_node_groups = { for i, subnet in var.cluster_subnets_ops : "worker-${i}" => {
    name                      = "worker-${i}"
    subnet_ids                = [subnet]
    instance_types            = ["m5.xlarge"]
    desired_capacity          = 1
    min_size                  = i == 0 ? 0 : 1 // Scaling down 1st Node Group
    max_capacity              = 3
    disk_size                 = 50
    source_security_group_ids = []
  } }

  // Tagging
  tags = {
    Module      = "environments/aws/minds-aws-kubernetes"
    Automation  = "terraform"
    Application = "ops"
  }
}
