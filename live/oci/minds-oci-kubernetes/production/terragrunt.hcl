terraform {
  source = "${get_path_to_repo_root()}/modules/oci/minds-oci-kubernetes"
}

inputs = {
  workspace                 = "default",
  enable_calico             = true,
  enable_cluster_autoscaler = true,
  create_fss                = true,
  autoscaler_pools          = { asp_v123 = {} },
  oke_main_nat_gateway_route_rules = [
    {
      destination       = "192.168.0.0/16"
      destination_type  = "CIDR_BLOCK",
      network_entity_id = "ocid1.localpeeringgateway.oc1.iad.aaaaaaaavwrmtxwtn3rqps4u7mqfxpapghcxpiv4muck6cebsymz3p247mmq",
      description       = ""
    }
  ],
  oke_main_node_pools = {
    /* Stateless */
    stateless = {
      placement_ads    = ["1", "2", "3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 12,
      memory           = 16,
      node_pool_size   = 12,
      boot_volume_size = 50,
      autoscale        = true
      label = {
        "minds.com/node-type"     = "stateless"
        "minds.com/node-revision" = "2"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateless"
        "minds-com.module"            = "k8s"
        "minds-com.module-group"      = "k8s"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    /* Stateful */
    cassandra-1 = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 8,
      memory           = 34,
      node_pool_size   = 2,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-k8ssandra"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-k8ssandra"
        "minds-com.module"            = "k8ssandra"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    cassandra-2 = {
      placement_ads    = ["2"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 8,
      memory           = 34,
      node_pool_size   = 2,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-k8ssandra"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-k8ssandra"
        "minds-com.module"            = "k8ssandra"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    cassandra-3 = {
      placement_ads    = ["3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 8,
      memory           = 34,
      node_pool_size   = 2,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-k8ssandra"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-k8ssandra"
        "minds-com.module"            = "k8ssandra"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    /* Redis */
    caching--1 = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 3,
      memory           = 12,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-caching"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-caching"
        "minds-com.module"            = "redis"
        "minds-com.module-group"      = "caching"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    caching--2 = {
      placement_ads    = ["2"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 3,
      memory           = 12,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-caching"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-caching"
        "minds-com.module"            = "redis"
        "minds-com.module-group"      = "caching"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    caching--3 = {
      placement_ads    = ["3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 3,
      memory           = 12,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-caching"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-caching"
        "minds-com.module"            = "redis"
        "minds-com.module-group"      = "caching"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    /* Pulsar */
    pulsar-1 = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 4,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-pulsar"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-pulsar"
        "minds-com.module"            = "pulsar"
        "minds-com.module-group"      = "messaging"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    pulsar--2 = {
      placement_ads    = ["2"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 4,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-pulsar"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-pulsar"
        "minds-com.module"            = "pulsar"
        "minds-com.module-group"      = "messaging"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    pulsar-3 = {
      placement_ads    = ["3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 4,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-pulsar"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-pulsar"
        "minds-com.module"            = "pulsar"
        "minds-com.module-group"      = "messaging"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    /* Vault */
    vault = {
      placement_ads    = ["1", "2", "3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 1,
      memory           = 4,
      node_pool_size   = 3,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vault"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vault"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    /* Vitess */
    vt-mautic-2 = { // AD-2 - Mautic
      placement_ads    = ["2"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 4,
      memory           = 16,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vitess-mautic"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vitess-mautic"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    /* Vitess */
    vt--1-minds = { // AD-1
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 6,
      memory           = 16,
      node_pool_size   = 1,
      boot_volume_size = 75,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vitess-minds"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vitess-minds"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    vt--2-minds = { // AD-2
      placement_ads    = ["2"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 6,
      memory           = 16,
      node_pool_size   = 1,
      boot_volume_size = 75,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vitess-minds"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vitess-minds"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    vt--3-minds = { // AD-3
      placement_ads    = ["3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 6,
      memory           = 16,
      node_pool_size   = 1,
      boot_volume_size = 75,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vitess-minds"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vitess-minds"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    search--1 = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 6,
      memory           = 48,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-opensearch"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-opensearch"
        "minds-com.module"            = "opensearch"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    search--2 = {
      placement_ads    = ["2"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 6,
      memory           = 48,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-opensearch"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-opensearch"
        "minds-com.module"            = "opensearch"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    search--3 = {
      placement_ads    = ["3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 6,
      memory           = 48,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-opensearch"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-opensearch"
        "minds-com.module"            = "opensearch"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    monitoring-1 = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 3,
      memory           = 12,
      node_pool_size   = 1,
      boot_volume_size = 75,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-monitoring"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-monitoring"
        "minds-com.module"            = "monitoring"
        "minds-com.module-group"      = "monitoring"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
    keycloak-1 = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 2,
      memory           = 4,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-keycloak"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-keycloak"
        "minds-com.module"            = "keycloak"
        "minds-com.module-group"      = "keycloak"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "production"
      }
    },
  }
}
