terraform {
  source = "${get_path_to_repo_root()}/modules/oci/minds-oci-kubernetes"
}

inputs = {
  workspace               = "ops",
  enable_hp_storage_class = false
  oke_main_node_pools = {
    /* Stateless */
    stateless = {
      placement_ads    = ["1", "2", "3"],
      shape            = "VM.Standard.E4.Flex",
      ocpus            = 2,
      memory           = 4,
      node_pool_size   = 4,
      boot_volume_size = 50,
      autoscale        = true
      label = {
        "minds.com/node-type" = "stateless"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateless"
        "minds-com.module"            = "k8s"
        "minds-com.module-group"      = "k8s"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "ops"
      }
    }
  }
}