terraform {
  source = "${get_path_to_repo_root()}/modules/oci/minds-oci-kubernetes"
}

inputs = {
  enable_calico             = true,
  enable_cluster_autoscaler = true,
  autoscaler_pools          = { asp_v123 = {} },
  create_fss                = true,
  oke_main_node_pools = {
    /* Stateless */
    stateless = {
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 8,
      memory           = 16,
      node_pool_size   = 8,
      boot_volume_size = 50,
      autoscale        = true,
      label = {
        "minds.com/node-type" = "stateless"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateless"
        "minds-com.module"            = "k8s"
        "minds-com.module-group"      = "k8s"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    /* Stateful */
    generic = {
      placement_ads    = ["1", "2", "3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 8,
      memory           = 16,
      node_pool_size   = 3,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-generic"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-generic"
        "minds-com.module"            = "k8s"
        "minds-com.module-group"      = "k8s"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    /* K8ssandra */
    k8ssandra = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 2,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-k8ssandra"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-k8ssandra"
        "minds-com.module"            = "k8ssandra"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    /* Vault */
    vault = { # Vault (no highly availble in sandboxes)
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 4,
      memory           = 8,
      node_pool_size   = 3,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vault"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vault"
        "minds-com.module"            = "vault"
        "minds-com.module-group"      = "secret-management"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    /* OpenSearch */
    opensearch = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 2,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-opensearch"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-opensearch"
        "minds-com.module"            = "opensearch"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    /* Pulsar */
    pulsar = {
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 2,
      memory           = 6,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-pulsar"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-pulsar"
        "minds-com.module"            = "pulsar"
        "minds-com.module-group"      = "messaging"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    /* Vitess */
    vitess-1 = { // AD-1
      placement_ads    = ["1"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 2,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vitess"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vitess"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    vitess-2 = { // AD-2
      placement_ads    = ["2"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 2,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vitess"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vitess"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    },
    vitess-3 = { // AD-3
      placement_ads    = ["3"],
      shape            = "VM.Standard.E5.Flex",
      ocpus            = 2,
      memory           = 8,
      node_pool_size   = 1,
      boot_volume_size = 50,
      autoscale        = false,
      label = {
        "minds.com/node-type" = "stateful-vitess"
      },
      node_defined_tags = {
        "oke-node-pools.node-type"    = "stateful-vitess"
        "minds-com.module"            = "vitess"
        "minds-com.module-group"      = "databases"
        "minds-com.deployment-method" = "terraform"
        "minds-com.environment"       = "sandbox"
      }
    }
  }
}
