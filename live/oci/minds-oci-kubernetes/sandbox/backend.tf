terraform {
  backend "s3" {
    bucket = "minds-terraform"
    region = "us-east-1"
    key    = "live/oci/minds-oci-kubernetes/sandbox/terraform.tfstate"
  }
}

provider "oci" {
  tenancy_ocid        = var.oci_tenancy_ocid
  region              = var.oci_region
  config_file_profile = "DEFAULT"
}

provider "oci" {
  alias               = "home"
  tenancy_ocid        = var.oci_tenancy_ocid
  region              = var.oci_region
  config_file_profile = "DEFAULT"
}

provider "aws" {
  region = "us-east-1"
}