terraform {
  source = "${get_path_to_repo_root()}/modules/bridge/aws-oci-bridge"
}

dependency "oci_network" {
  config_path = "../../../oci/minds-oci-kubernetes/sandbox"

  mock_outputs = {
    oke_main_vcn_id = "vcn_id"
  }
}

inputs = {
  oke_main_vcn_id      = dependency.oci_network.outputs.oke_main_vcn_id
  oci_compartment_ocid = "ocid1.compartment.oc1..aaaaaaaaeveizn2oeenmh264tdv7hpx63r45lmzas77alwhibf3ryof2ujkq"

  aws_eks_vpc_id             = "vpc-0184d2a6041da2b48"
  aws_eks_route_table_id     = "rtb-0da78c5115077bf43"
  use_aws_main_minds         = false
  deploy_aws_eks_vpn_gateway = true
}
