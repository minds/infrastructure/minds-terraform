terraform {
  backend "s3" {
    bucket = "minds-terraform"
    key    = "live/bridge/aws-oci-bridge/sandbox/terraform.tfstate"
    region = "us-east-1"
  }
  required_providers {
    oci = {
      version = "4.93.0"
    }
  }
}

provider "aws" { region = "us-east-1" }

provider "oci" {
  tenancy_ocid = var.oci_tenancy_ocid
  region       = var.oci_region
}
