# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "4.31.0"
  hashes = [
    "h1:CgcuV/dhLsXSMFKMbQIHTiGlSBz2/knIJa0vNLQ5SgU=",
    "h1:Ib7NTl1AX+17AP1QHbCTl6G2lIjdrlCSSmu8itTTVmk=",
    "h1:ZJ73siqSB+m54xf60aNJKY+HiQkqFy1HGrRTFloQL6s=",
    "zh:14169119237a7ae174ea19f13edf34ed4de963ab97d937fee9d1f1ddd706f883",
    "zh:24276942da2b858c2dd9eb0428cfd82b46e758fa00ae38685f05380f75034a8f",
    "zh:378f1a9f603995bb76827b703ace8bf5212a9c7b96a106f4a87a9871249c885f",
    "zh:38e992e1137d2e8dc203edd659432c21c87c7cbf99439b68253afdb82a079ead",
    "zh:56ff847dd504d4098ff4ea7501c8d5ffae4e1ba0dacb85f658d992de474c8fec",
    "zh:5b2ab38ba7d04e0d3c5bf0a0ab1aafe806faaa9be228480b772606bd32d41620",
    "zh:6e6c6b10f05018691ef767efadc60fe3cd38adb2e66e65a74fa2be0ce3d7f49d",
    "zh:90e9fb656d7c19cf702834d7da275b65fc40fbef10224f6f8295a40ca2acb43b",
    "zh:92b5eaca47b3ab1829cf4cd96dc144f456bc023329f3e20cf3d5fdc913e6e5e3",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:ad782fb8ead528a0d8c7c77d1a417f4d8ceaaefbfc0a0c34993ba92e6dc5ff41",
    "zh:b121595f53c85bdbb59504b255628d219c42586cb258f0d4cf8a532a98f766e5",
  ]
}

provider "registry.terraform.io/hashicorp/oci" {
  version     = "4.93.0"
  constraints = "4.93.0"
  hashes = [
    "h1:7Rs7TBjFrgBXf0jgZql38NgrjKX5RkiPdMD+85/bQPY=",
    "h1:FxFhMDkp3bzhy5SKN9YPhcbSrWk/AAiIPDMAZXFsi84=",
    "h1:eGuy0CbOTPXdfVHZshNWDUIlK1CfA37GjqsmZmLm8oQ=",
    "zh:2a3e88a0f373b7ec9020d51103b19e897b72272dd28eb5268ac451cefcd0dde6",
    "zh:2ca9d92369d4f27b4ab4cfb9d7e5edcaeb97cd5aedc82b3c66ad7b0220407a76",
    "zh:328ca8916d9a60084825672fb59308ab81816bef36272afae71a17f067dcb7b0",
    "zh:430c3c11a96045ad13aaa9169357a9078162d0a461c9a5e6ab59e99367ee9890",
    "zh:513cee90abc403f3313c41c959f32a292dbb444825e63635d160f9657e403e55",
    "zh:5a22966eebb5d3a749eb793e704011a8524207e22b486f76a85ed27cf9f1ce53",
    "zh:609fd6318b77571870cd184c47eb5bf952dd64a4e43d07e65e40cd3c87cdf667",
    "zh:756952e1ee143b33417c4e9f69b4142df6e0d8c2317be0bbfc4716185300df8a",
    "zh:75f94fa55cea936a2164e5fb0a62ae738f89f9c5f343c7964ac658ac975ee5fb",
    "zh:8b5abce56cc86483887e7490203513c9f4e3da932639f04bc57dfe0d6b261b80",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9d381e41035c83a413729b6a266098f35176cc11ce739d128da5e451f1316a3a",
    "zh:bc89e8a233ec496039b134716c5af16beec21e00424e96d1624b3695ae6ef623",
    "zh:cb340b5ac2081b0d512a596455a98d939e5ef2e71caf0831ab163183106ad0f9",
    "zh:d4c2b7cc1296b0f32feaea5aa13e3fee70e19c7a34223c7bd7a3b9382e1af522",
  ]
}
