terraform {
  source = "${get_path_to_repo_root()}/modules/bridge/aws-oci-bridge"
}

dependency "oci_network" {
  config_path = "../../oci/minds-oci-kubernetes/production"

  mock_outputs = {
    oke_main_vcn_id = "vcn_id"
  }
}

inputs = {
  oke_main_vcn_id = dependency.oci_network.outputs.oke_main_vcn_id
}
